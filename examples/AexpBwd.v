(** * Backward slicing of arithmetic expressions examples  *)

Require Import Imp.
Require Import ImpPartial.
Require Import ImpSlicing.
Require Import ImpState.
Require Import ImpValues.
Require Import PrefixSets.

Require Import Coq.Lists.List.


(** ** Universal definitions *)
Definition X   := Id "X".
Definition Y   := Id "Y".
Definition a1 := ANum 1.
Definition a2 := ANum 2.
Definition a3 := ANum 3.

Definition v1 := 1.
Definition v2 := 2.
Definition v3 := 3.

Definition v1P := valuePartialize v1.
Definition v2P := valuePartialize v2.
Definition v3P := valuePartialize v3.

Hint Unfold a1 a2 a3 v1 v2 v3 v1P v2P v3P.

(** ** Example 1 *)

(** Assuming evaluation

      X + 2 / [X |-> 1, Y |-> 2] \\ 3

    backward slicing w.r.t. 3 yields:

      X + 2 / [X |-> 1, Y |-> _]
*)

Module aexpBwdExample1.

Definition st  := StateCons X 1 (StateCons Y 2 StateNil).
Definition stP := statePartialize st.
Definition a   := (AId X + a2)%aexp.
Definition aP  := aexpPartialize a.

Definition t2 := ANumT 2.
Definition t  := (AIdT X 1 + t2)%impt.

Hint Unfold st stP a aP t2 t.

(* Evidence that expression (X + 2) under state [X |-> 1, Y |-> 2] evaluates to
   3 *)
Definition ev : (a / st \\ v3 :: t)%ar :=
  E_Plus st (AId X) a2 v1 v2 _ _ (E_Id v1 X st eq_refl) (E_Num st v2).

(* Partial value 3, a prefix of 3 *)
Definition vBwdCriterion : prefix valuePO v3P :=
  Pfx valuePO v3P v3P (reflexive_valuePO v3P).

(* Partial expression: X + 2, a prefix of X + 2 *)
Definition aexpBwdSlice : prefix aexpPO aP :=
  Pfx aexpPO aP aP (APlusPO (AIdP X) (ANumP 2) (AIdP X) (ANumP 2)
      (reflexive_aexpPO (AIdP X))
      (value_anum_lt 2 2 (reflexive_valuePO (valuePartialize 2)))).

(* Partial state: [X |-> 1, Y |-> _ ], a prefix of [X |-> 1, Y |-> 2 ] *)
Definition stBwdSlice : prefix statePO stP :=
  Pfx statePO stP
      (StatePCons X v1P (StatePCons Y VHoleP StatePNil))
      ((StateConsPO X (valuePartialize 1) (VNatP 1) (StatePCons Y VHoleP StatePNil)
       (StatePCons Y (VNatP 2) StatePNil) (stateP_leq_V (StatePCons Y VHoleP StatePNil)
          (StatePCons Y (VNatP 2) StatePNil) X X
          (valuePartialize 1) (VNatP 1)
          (updateP_with_smaller (StatePCons X VHoleP (StatePCons Y VHoleP StatePNil))
             (StatePCons X (VNatP 1) (StatePCons Y (VNatP 2) StatePNil)) X
             (valuePartialize 1)
             (empty_state_leq (StateCons X 1 (StateCons Y 2 StateNil)))
             (eq_ind_r (fun v : valueP => (valuePartialize 1 <= v)%v)
                       (reflexive_valuePO (valuePartialize 1))
                       (lookup_Some_VNatP
                          (StateCons X 1 (StateCons Y 2 StateNil)) X 1 eq_refl))))
       (StateConsPO Y VHoleP (VNatP 2) StatePNil
          StatePNil
          (stateP_leq_V StatePNil StatePNil Y Y VHoleP (VNatP 2)
             (stateP_leq_St
                (StatePCons Y VHoleP StatePNil)
                (StatePCons Y (VNatP 2) StatePNil) X X
                VHoleP (VNatP 1)
                (eq_ind_r (fun l : list id =>
                  (empty_stateP l <=
                   StatePCons X (VNatP 1) (StatePCons Y (VNatP 2) StatePNil))%s)
                   (empty_stateP_leq
                      (StatePCons X (VNatP 1) (StatePCons Y (VNatP 2) StatePNil)))
                   (state_partialize_keys (StateCons X 1 (StateCons Y 2 StateNil))))))
          (stateP_leq_St StatePNil StatePNil Y Y
             VHoleP (VNatP 2)
             (stateP_leq_St
                (StatePCons Y VHoleP StatePNil)
                (StatePCons Y (VNatP 2) StatePNil) X X
                (valuePartialize 1)
                (VNatP 1)
                (updateP_with_smaller
                   (StatePCons X VHoleP (StatePCons Y VHoleP StatePNil))
                   (StatePCons X (VNatP 1) (StatePCons Y (VNatP 2) StatePNil)) X
                   (valuePartialize 1)
                   (empty_state_leq (StateCons X 1 (StateCons Y 2 StateNil)))
                   (eq_ind_r (fun v : valueP => (valuePartialize 1 <= v)%v)
                      (reflexive_valuePO (valuePartialize 1))
                      (lookup_Some_VNatP (StateCons X 1 (StateCons Y 2 StateNil))
                                         X 1 eq_refl)))))))).

Hint Unfold vBwdCriterion aexpBwdSlice stBwdSlice.

Example example : aexpBwd ev vBwdCriterion = (aexpBwdSlice, stBwdSlice).
Proof. reflexivity. Qed.

End aexpBwdExample1.

(** ** Example 2 *)

(** Assuming evaluation

      X + 2 / [X |-> 1, Y |-> 2] \\ 3

    backward slicing w.r.t. _ yields:

      _ / [X |-> _, Y |-> _]
*)

Module aexpBwdExample2.

Definition st  := StateCons X 1 (StateCons Y 2 StateNil).
Definition stP := statePartialize st.
Definition a   := (AId X + a2)%aexp.
Definition aP  := aexpPartialize a.

Definition t2 := ANumT 2.
Definition t  := (AIdT X 1 + t2)%impt.

Hint Unfold st stP a aP t2 t.

(* Evidence that expression (X + 2) and state [X |-> 1, Y |-> 2] evaluate to 3 *)
Definition ev : (a / st \\ v3 :: t)%ar :=
  E_Plus st (AId X) a2 v1 v2 _ _ (E_Id v1 X st eq_refl) (E_Num st v2).

(* Partial value _, a prefix of 3 *)
Definition vBwdCriterion : prefix valuePO v3P :=
  Pfx valuePO v3P VHoleP (VHolePO v3P).

(* partial expression: _, a prefix of X + 2 *)
Definition aexpBwdSlice : prefix aexpPO aP :=
  Pfx aexpPO aP AHoleP (AHolePO aP).

(* partial state: [X |-> _, Y |-> _ ], a prefix of [X |-> 1, Y |-> 2 ] *)
Definition stBwdSlice : prefix statePO stP :=
  Pfx statePO stP
      (StatePCons X VHoleP (StatePCons Y VHoleP StatePNil))
      (empty_stateP_leq
       (StatePCons X (VNatP 1)
          (StatePCons Y (VNatP 2) StatePNil))).

Hint Unfold vBwdCriterion aexpBwdSlice stBwdSlice.

Example example : aexpBwd ev vBwdCriterion = (aexpBwdSlice, stBwdSlice).
Proof. reflexivity. Qed.

End aexpBwdExample2.
