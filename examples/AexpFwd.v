(** * Forward slicing of arithmetic expressions examples  *)

Require Import Imp.
Require Import ImpPartial.
Require Import ImpSlicing.
Require Import ImpState.
Require Import ImpValues.
Require Import PrefixSets.

Require Import Coq.Lists.List.

(** ** Universal definitions *)
Definition X   := Id "X".
Definition a0  := ANum 0.
Definition a17 := ANum 17.
Definition a42 := ANum 42.
Definition a59 := ANum 59.

Definition t42 := ANumT 42.

(** ** Example 1 : (X + 42, [X |-> _]) forward slices to _ *)
Module aexpFwdExample1.

(* Example state: [X |-> 17] *)
Definition st  := StateCons X 17 StateNil.
Definition stP := statePartialize st.

Definition a  := (AId X + a42)%aexp.
Definition aP := aexpPartialize a.

Definition t := (AIdT X 17 + t42)%impt.

(* Evidence that expression (X + 42) and state [X |-> 17] evaluate to 59 *)
Definition ev : (a / st \\ 59 :: t)%ar :=
  E_Plus st (AId X) a42 17 42 _ _ (E_Id 17 X st eq_refl) (E_Num st 42).

(* Partial expression (X + 42), a prefix of (X + 42) *)
Definition aexpFwdCriterion : prefix aexpPO aP :=
  Pfx aexpPO aP aP (reflexive_aexpPO aP).

(* partial state: [X |-> _ ], a prefix of [X |- 17] *)
Definition stFwdCriterion : prefix statePO stP :=
  Pfx statePO stP (StatePCons X VHoleP StatePNil)
      (StateConsPO X VHoleP (VNatP 17) StatePNil
                   StatePNil (VHolePO (VNatP 17)) StateNilPO).

(* Expected result of forward slicing : a hole *)
Definition fwdSlice : prefix valuePO (VNatP 59) :=
  Pfx valuePO (VNatP 59) VHoleP (VHolePO (VNatP 59)).

Example example : aexpFwd ev (aexpFwdCriterion, stFwdCriterion) = fwdSlice.
Proof. reflexivity. Qed.

End aexpFwdExample1.


(** ** Example 2 : (X, [X |-> _]) forward slices to _ *)
Module aexpFwdExample2.

(* Example state: [X |-> 17] *)
Definition st  := StateCons X 17 StateNil.
Definition stP := statePartialize st.

Definition a  := AId X.
Definition aP := aexpPartialize a.

Definition t := (AIdT X 17)%impt.

(* Evidence that expression X and state [X |-> 17] evaluate to 17 *)
Definition ev : (a / st \\ 17 :: t)%ar :=
  E_Id 17 X st eq_refl.

(* Partial expression X, a prefix of X *)
Definition aexpFwdCriterion : prefix aexpPO aP :=
  Pfx aexpPO aP aP (reflexive_aexpPO aP).

(* partial state: [X |-> _ ], a prefix of [X |- 17] *)
Definition stFwdCriterion : prefix statePO stP :=
  Pfx statePO stP (StatePCons X VHoleP StatePNil)
      (StateConsPO X VHoleP (VNatP 17) StatePNil
                   StatePNil (VHolePO (VNatP 17)) StateNilPO).

(* Expected result of forward slicing : a hole *)
Definition fwdSlice : prefix valuePO (VNatP 17) :=
  Pfx valuePO (VNatP 17) VHoleP
    (transitive_valuePO VHoleP (VNatP 17)
       (VNatP 17)
       (ordered_state_lookup
          (StatePCons X VHoleP StatePNil)
          (StatePCons X (VNatP 17) StatePNil) X
          (StateConsPO X VHoleP
             (VNatP 17) StatePNil StatePNil
             (VHolePO (VNatP 17)) StateNilPO))
       (eq_ind_r
          (fun v' : valueP => (v' <= VNatP 17)%v)
          (reflexive_valuePO (VNatP 17))
          (lookup_Some_VNatP st X 17 eq_refl))).

Example example : aexpFwd ev (aexpFwdCriterion, stFwdCriterion) = fwdSlice.
Proof. reflexivity. Qed.

End aexpFwdExample2.

(** ** Example 3 : (17 + (0 + 42), []) forward slices to _ *)

Module aexpFwdExample3.

(* Example state: [] *)
Definition st  := StateNil.
Definition stP := statePartialize st.

Definition a  := APlus a17 (APlus a0 a42).
Definition aP := aexpPartialize a.

Definition t := (APlusT (ANumT 17) (APlusT (ANumT 0) (ANumT 42)))%impt.

(* Evidence that expression 17 + (0 + 42) evaluates to 59 *)
Definition ev : (a / st \\ 59 :: t)%ar :=
 E_Plus st a17 (APlus a0 a42) 17 42 (ANumT 17) (APlusT (ANumT 0) (ANumT 42))
        (E_Num st 17)
        (E_Plus st a0 a42 0 42 (ANumT 0) (ANumT 42) (E_Num st 0) (E_Num st 42)).

(* Partial expression (17 + (_ + 42), a prefix of (17 + (0 + 42)) *)
Definition aexpFwdCriterion : prefix aexpPO aP :=
  Pfx aexpPO aP (APlusP (ANumP 17) (APlusP AHoleP (ANumP 42)))
      (APlusPO (ANumP 17) (APlusP AHoleP (ANumP 42)) (ANumP 17)
               (APlusP (ANumP 0) (ANumP 42)) (reflexive_aexpPO (ANumP 17))
               (APlusPO AHoleP (ANumP 42) (ANumP 0) (ANumP 42)
                        (AHolePO (ANumP 0)) (reflexive_aexpPO (ANumP 42)))).

(* partial state: [], a prefix of [] *)
Definition stFwdCriterion : prefix statePO stP :=
  Pfx statePO stP stP (reflexive_statePO stP).

(* Expected result of forward slicing : a hole *)
Definition fwdSlice : prefix valuePO (VNatP 59) :=
  Pfx valuePO (VNatP 59) VHoleP (VHolePO (VNatP 59)).

Example example : aexpFwd ev (aexpFwdCriterion, stFwdCriterion) = fwdSlice.
Proof. reflexivity. Qed.

End aexpFwdExample3.
