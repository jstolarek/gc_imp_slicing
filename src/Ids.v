Require Import Coq.MSets.MSets.
Require Import Coq.Lists.List.

Require Import LSTactics.

Require Import Coq.Init.Nat.
Require Import Coq.Arith.PeanoNat.

(** Variable ids *)

Inductive id : Type :=
  | Id : nat -> id.

(* Equality of ids *)
Definition beq_id (x y : id) : bool :=
  match x,y with
    | Id n1, Id n2 => eqb n1 n2
  end.

Theorem beq_id_refl : forall i, beq_id i i = true.
Proof.
  intros [n]. simpl. apply Nat.eqb_refl.
Qed.

Theorem beq_id_true_iff : forall x y, beq_id x y = true <-> x = y.
Proof.
  intros [n1] [n2]. unfold beq_id.
  split; intro H.
  - apply Nat.eqb_eq in H. subst. reflexivity.
  - inversion H. subst. apply Nat.eqb_refl.
Qed.

Theorem beq_id_false_iff : forall x y, beq_id x y = false <-> x <> y.
Proof.
  intros x y.
  rewrite <- beq_id_true_iff.
  rewrite not_true_iff_false.
  reflexivity.
Qed.

Hint Resolve beq_id_refl beq_id_true_iff beq_id_false_iff.

Theorem beq_id_false : forall x y, beq_id x y = false -> x <> y.
Proof.
  intros x y H. rewrite beq_id_false_iff in H. assumption.
Qed.

Hint Resolve beq_id_false.

Lemma id_eq_dec : forall x y : id, {x = y} + {x <> y}.
Proof.
  intros [x] [y].
  destruct (Nat.eq_dec x y).
  - auto.
  - right. intro. inversion H. contradiction.
Qed.

Hint Resolve id_eq_dec.

(* Ordering of ids *)
Definition id_lt (id1 id2 : id) : Prop :=
  match id1, id2 with
  | Id i1, Id i2 => i1 < i2
  end.

Definition id_le (id1 id2 : id) : Prop :=
  match id1, id2 with
  | Id i1, Id i2 => i1 <= i2
  end.

Definition id_ltb (id1 id2 : id) : bool :=
  match id1, id2 with
  | Id i1, Id i2 => i1 <? i2
  end.

Definition id_leb (id1 id2 : id) : bool :=
  match id1, id2 with
  | Id i1, Id i2 => i1 <=? i2
  end.

Hint Unfold id_lt id_le id_ltb id_leb.

(** Properties of Id ordering **)
Lemma id_eqb_neq  : forall x y, x <> y <-> beq_id x y = false.
Proof.
  intros [x] [y]. split.
  - simpl. generalize dependent y. induction x; induction y; intro H.
    + elim H. trivial.
    + trivial.
    + trivial.
    + simpl. apply IHx. intro. elim H. inversion H0. reflexivity.

  - rewrite beq_id_false_iff. trivial.
Qed.

Hint Resolve id_eqb_neq.

Lemma id_le_refl : forall i, id_le i i.
Proof. intros [i]. auto. Qed.

Hint Resolve id_le_refl.

Lemma id_le_antisym : forall x y, id_le x y -> id_le y x -> x = y.
  intros [x] [y] H H0.
  f_equal. apply Nat.le_antisymm; assumption.
Qed.

Hint Resolve id_le_antisym.

Lemma id_le_trans : forall x y z, id_le x y -> id_le y z -> id_le x z.
Proof.
  intros [x] [y] [z] H H0.
  autounfold in *. eapply Nat.le_trans; eassumption.
Qed.

Hint Resolve id_le_trans.

Lemma id_lt_norefl : forall x, ~ id_lt x x.
Proof.
  autounfold.
  intros [x].
  apply Nat.lt_irrefl.
Qed.

Hint Resolve id_lt_norefl.

Lemma id_lt_neq : forall x y, id_lt x y -> x <> y.
Proof.
  autounfold.
  intros [x] [y] H H0.
  inversion H0.
  subst.
  eapply Nat.lt_irrefl; eassumption.
Qed.

Hint Resolve id_lt_neq.

Lemma id_lt_antisym : forall x y, id_lt x y -> id_lt y x -> False.
Proof.
  autounfold.
  intros [x] [y] H H0.
  eapply Nat.lt_asymm; eassumption.
Qed.

Hint Resolve id_lt_antisym.

Lemma id_lt_trans : forall x y z, id_lt x y -> id_lt y z -> id_lt x z.
Proof.
  autounfold.
  intros [x] [y] [z] H H0.
  eapply Nat.lt_trans; eassumption.
Qed.

Hint Resolve id_lt_trans.

Lemma id_not_le_to_lt : forall x y, ~ id_le x y -> id_lt y x.
Proof.
  intros [x] [y] H.
  unfold id_le, id_lt in *.
  apply Nat.nle_gt; assumption.
Qed.

Hint Resolve id_not_le_to_lt.

Lemma id_not_lt_to_le : forall x y, ~ id_lt x y -> id_le y x.
Proof.
  intros [x] [y] H.
  unfold id_le, id_lt in *.
  apply Nat.nlt_ge; assumption.
Qed.

Hint Resolve id_not_lt_to_le.

Lemma id_leb_le : forall x y, id_leb x y = true <-> id_le x y.
Proof.
  intros [x] [y].
  autounfold.
  split; intro H; [ rewrite <- Nat.leb_le | rewrite Nat.leb_le ]; assumption.
Qed.

Hint Resolve id_leb_le.

Lemma id_ltb_lt : forall x y, id_ltb x y = true <-> id_lt x y.
Proof.
  intros [x] [y].
  autounfold.
  split; intro H; [ rewrite <- Nat.ltb_lt | rewrite Nat.ltb_lt ]; assumption.
Qed.

Hint Resolve id_ltb_lt.

Lemma id_le_lteq : forall x y, id_le x y <-> id_lt x y \/ x = y.
Proof.
  intros [x] [y].
  autounfold.
  split; intro H.
  - apply Nat.le_lteq in H; destruct H; auto.
  - destruct H.
    + apply Nat.lt_le_incl; assumption.
    + inversion H. subst. auto.
Qed.

Hint Resolve id_le_lteq.

Lemma id_leb_false_not_le : forall x y, id_leb x y = false <-> ~ id_le x y.
Proof.
  intros [x] [y].
  unfold id_leb, id_le.
  apply Nat_as_DT.leb_nle.
Qed.

Hint Resolve id_leb_false_not_le.

Lemma id_linearity : forall x y, x = y \/ id_lt x y \/ id_lt y x.
Proof.
  autounfold.
  intros [x]; induction x; intros [y]; induction y.
  - auto.
  - right. left. apply Nat.lt_0_succ.
  - right. right. apply Nat.lt_0_succ.
  - specialize (IHx (Id y)).
    destruct IHx.
    + inversion H. auto.
    + destruct H.
      * right. left. apply Lt.lt_n_S; assumption.
      * right. right. apply Lt.lt_n_S; assumption.
Qed.

Hint Resolve id_linearity.

Ltac reflect_id :=
  repeat match goal with
         | [ H : beq_id _ _ = true |- _ ] =>
           rewrite beq_id_true_iff in H
         | [ H : beq_id _ _ = false |- _ ] =>
           rewrite beq_id_false_iff in H
         | [ |- context[eqb ?X ?X ] ] =>
           rewrite eqb_refl
         end.
