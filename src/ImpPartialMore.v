Require Import Ids.
Require Import Imp.
Require Import ImpState.
Require Import ImpValues.
Require Import Lattice.
Require Import LSTactics.
Require Import PrefixSets.
Require Import ImpPartial.

(* Some lemmas move to this file due to high memory usage. *)

Ltac solve_LUB_comm_bin := intros; simpl; destruct_matches; eauto.

Lemma aexpLUB_comm_APlusP_1 :
  forall a1 a2 P0 P1,
   aexpLUB (Pfx aexpPO (APlusP a1 a2) AHoleP P0)
           (Pfx aexpPO (APlusP a1 a2) AHoleP P1) =
   Pfx aexpPO (APlusP a1 a2) AHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_APlusP_2 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P0)
           (Pfx aexpPO (APlusP a1 a2) AHoleP P1) =
   Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_APlusP_3 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (APlusP a1 a2) AHoleP P0)
           (Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P1) =
   Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve aexpLUB_comm_APlusP_1 aexpLUB_comm_APlusP_2 aexpLUB_comm_APlusP_3.

Lemma aexpLUB_comm_AMinusP_1 :
  forall a1 a2 P0 P1,
   aexpLUB (Pfx aexpPO (AMinusP a1 a2) AHoleP P0)
           (Pfx aexpPO (AMinusP a1 a2) AHoleP P1) =
   Pfx aexpPO (AMinusP a1 a2) AHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_AMinusP_2 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P0)
           (Pfx aexpPO (AMinusP a1 a2) AHoleP P1) =
   Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_AMinusP_3 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (AMinusP a1 a2) AHoleP P0)
           (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P1) =
   Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve aexpLUB_comm_AMinusP_1 aexpLUB_comm_AMinusP_2 aexpLUB_comm_AMinusP_3.

Lemma aexpLUB_comm_AMultP_1 :
  forall a1 a2 P0 P1,
   aexpLUB (Pfx aexpPO (AMultP a1 a2) AHoleP P0)
           (Pfx aexpPO (AMultP a1 a2) AHoleP P1) =
   Pfx aexpPO (AMultP a1 a2) AHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_AMultP_2 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P0)
           (Pfx aexpPO (AMultP a1 a2) AHoleP P1) =
   Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma aexpLUB_comm_AMultP_3 :
  forall a1 a2 a1' a2' P0 P1,
   aexpLUB (Pfx aexpPO (AMultP a1 a2) AHoleP P0)
           (Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P1) =
   Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve aexpLUB_comm_AMultP_1 aexpLUB_comm_AMultP_2 aexpLUB_comm_AMultP_3.

Lemma bexpLUB_comm_BEqP_1 :
  forall b1 b2 P0 P1,
   bexpLUB (Pfx bexpPO (BEqP b1 b2) BHoleP P0)
           (Pfx bexpPO (BEqP b1 b2) BHoleP P1) =
   Pfx bexpPO (BEqP b1 b2) BHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BEqP_2 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BEqP b1 b2) (BEqP b1' b2') P0)
           (Pfx bexpPO (BEqP b1 b2) BHoleP P1) =
   Pfx bexpPO (BEqP b1 b2) (BEqP b1' b2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BEqP_3 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BEqP b1 b2) BHoleP P0)
           (Pfx bexpPO (BEqP b1 b2) (BEqP b1' b2') P1) =
   Pfx bexpPO (BEqP b1 b2) (BEqP b1' b2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve bexpLUB_comm_BEqP_1 bexpLUB_comm_BEqP_2 bexpLUB_comm_BEqP_3.

Lemma bexpLUB_comm_BLeP_1 :
  forall b1 b2 P0 P1,
   bexpLUB (Pfx bexpPO (BLeP b1 b2) BHoleP P0)
           (Pfx bexpPO (BLeP b1 b2) BHoleP P1) =
   Pfx bexpPO (BLeP b1 b2) BHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BLeP_2 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BLeP b1 b2) (BLeP b1' b2') P0)
           (Pfx bexpPO (BLeP b1 b2) BHoleP P1) =
   Pfx bexpPO (BLeP b1 b2) (BLeP b1' b2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BLeP_3 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BLeP b1 b2) BHoleP P0)
           (Pfx bexpPO (BLeP b1 b2) (BLeP b1' b2') P1) =
   Pfx bexpPO (BLeP b1 b2) (BLeP b1' b2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve bexpLUB_comm_BLeP_1 bexpLUB_comm_BLeP_2 bexpLUB_comm_BLeP_3.

Lemma bexpLUB_comm_BNotP_1 :
  forall b1 P0 P1,
   bexpLUB (Pfx bexpPO (BNotP b1) BHoleP P0)
           (Pfx bexpPO (BNotP b1) BHoleP P1) =
   Pfx bexpPO (BNotP b1) BHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BNotP_2 :
  forall b1 b1' P0 P1,
   bexpLUB (Pfx bexpPO (BNotP b1) (BNotP b1') P0)
           (Pfx bexpPO (BNotP b1) BHoleP P1) =
   Pfx bexpPO (BNotP b1) (BNotP b1') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BNotP_3 :
  forall b1 b1' P0 P1,
   bexpLUB (Pfx bexpPO (BNotP b1) BHoleP P0)
           (Pfx bexpPO (BNotP b1) (BNotP b1') P1) =
   Pfx bexpPO (BNotP b1) (BNotP b1') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve bexpLUB_comm_BNotP_1 bexpLUB_comm_BNotP_2 bexpLUB_comm_BNotP_3.

Lemma bexpLUB_comm_BAndP_1 :
  forall b1 b2 P0 P1,
   bexpLUB (Pfx bexpPO (BAndP b1 b2) BHoleP P0)
           (Pfx bexpPO (BAndP b1 b2) BHoleP P1) =
   Pfx bexpPO (BAndP b1 b2) BHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BAndP_2 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BAndP b1 b2) (BAndP b1' b2') P0)
           (Pfx bexpPO (BAndP b1 b2) BHoleP P1) =
   Pfx bexpPO (BAndP b1 b2) (BAndP b1' b2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma bexpLUB_comm_BAndP_3 :
  forall b1 b2 b1' b2' P0 P1,
   bexpLUB (Pfx bexpPO (BAndP b1 b2) BHoleP P0)
           (Pfx bexpPO (BAndP b1 b2) (BAndP b1' b2') P1) =
   Pfx bexpPO (BAndP b1 b2) (BAndP b1' b2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve bexpLUB_comm_BAndP_1 bexpLUB_comm_BAndP_2 bexpLUB_comm_BAndP_3.

Ltac solve_LUB_comm_fst_snd :=
  intros; simpl in *; destruct_matches;
  generalize_nested_pfx_proofs;
  invert_pfx_equalities;
  equalize_pfx_proofs;
  auto.

Lemma aexpLUB_comm_APlusP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P0)
            (Pfx aexpPO (APlusP a1 a2) (APlusP a1'' a2'') P1) =
             Pfx aexpPO (APlusP a1 a2) (APlusP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma aexpLUB_comm_APlusP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (APlusP a1 a2) (APlusP a1' a2') P0)
            (Pfx aexpPO (APlusP a1 a2) (APlusP a1'' a2'') P1) =
             Pfx aexpPO (APlusP a1 a2) (APlusP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a2 a2' R0) (Pfx aexpPO a2 a2'' R1) =
             Pfx aexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve aexpLUB_comm_APlusP_fst aexpLUB_comm_APlusP_snd.

Lemma aexpLUB_comm_AMinusP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P0)
            (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1'' a2'') P1) =
             Pfx aexpPO (AMinusP a1 a2) (AMinusP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma aexpLUB_comm_AMinusP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1' a2') P0)
            (Pfx aexpPO (AMinusP a1 a2) (AMinusP a1'' a2'') P1) =
             Pfx aexpPO (AMinusP a1 a2) (AMinusP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a2 a2' R0) (Pfx aexpPO a2 a2'' R1) =
             Pfx aexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve aexpLUB_comm_AMinusP_fst aexpLUB_comm_AMinusP_snd.

Lemma aexpLUB_comm_AMultP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P0)
            (Pfx aexpPO (AMultP a1 a2) (AMultP a1'' a2'') P1) =
             Pfx aexpPO (AMultP a1 a2) (AMultP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma aexpLUB_comm_AMultP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    aexpLUB (Pfx aexpPO (AMultP a1 a2) (AMultP a1' a2') P0)
            (Pfx aexpPO (AMultP a1 a2) (AMultP a1'' a2'') P1) =
             Pfx aexpPO (AMultP a1 a2) (AMultP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a2 a2' R0) (Pfx aexpPO a2 a2'' R1) =
             Pfx aexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve aexpLUB_comm_AMultP_fst aexpLUB_comm_AMultP_snd.

Lemma bexpLUB_comm_BEqP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BEqP a1 a2) (BEqP a1' a2') P0)
            (Pfx bexpPO (BEqP a1 a2) (BEqP a1'' a2'') P1) =
             Pfx bexpPO (BEqP a1 a2) (BEqP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma bexpLUB_comm_BEqP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BEqP a1 a2) (BEqP a1' a2') P0)
            (Pfx bexpPO (BEqP a1 a2) (BEqP a1'' a2'') P1) =
             Pfx bexpPO (BEqP a1 a2) (BEqP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a2 a2' R0) (Pfx aexpPO a2 a2'' R1) =
             Pfx aexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve bexpLUB_comm_BEqP_fst bexpLUB_comm_BEqP_snd.

Lemma bexpLUB_comm_BLeP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BLeP a1 a2) (BLeP a1' a2') P0)
            (Pfx bexpPO (BLeP a1 a2) (BLeP a1'' a2'') P1) =
             Pfx bexpPO (BLeP a1 a2) (BLeP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma bexpLUB_comm_BLeP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BLeP a1 a2) (BLeP a1' a2') P0)
            (Pfx bexpPO (BLeP a1 a2) (BLeP a1'' a2'') P1) =
             Pfx bexpPO (BLeP a1 a2) (BLeP a1LUB a2LUB) P ->
    aexpLUB (Pfx aexpPO a2 a2' R0) (Pfx aexpPO a2 a2'' R1) =
             Pfx aexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve bexpLUB_comm_BLeP_fst bexpLUB_comm_BLeP_snd.

Lemma bexpLUB_comm_BNotP :
  forall a1 a1' a1'' a1LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BNotP a1) (BNotP a1') P0)
            (Pfx bexpPO (BNotP a1) (BNotP a1'') P1) =
             Pfx bexpPO (BNotP a1) (BNotP a1LUB) P ->
    bexpLUB (Pfx bexpPO a1 a1' R0) (Pfx bexpPO a1 a1'' R1) =
             Pfx bexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve bexpLUB_comm_BNotP.

Lemma bexpLUB_comm_BAndP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BAndP a1 a2) (BAndP a1' a2') P0)
            (Pfx bexpPO (BAndP a1 a2) (BAndP a1'' a2'') P1) =
             Pfx bexpPO (BAndP a1 a2) (BAndP a1LUB a2LUB) P ->
    bexpLUB (Pfx bexpPO a1 a1' R0) (Pfx bexpPO a1 a1'' R1) =
             Pfx bexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma bexpLUB_comm_BAndP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    bexpLUB (Pfx bexpPO (BAndP a1 a2) (BAndP a1' a2') P0)
            (Pfx bexpPO (BAndP a1 a2) (BAndP a1'' a2'') P1) =
             Pfx bexpPO (BAndP a1 a2) (BAndP a1LUB a2LUB) P ->
    bexpLUB (Pfx bexpPO a2 a2' R0) (Pfx bexpPO a2 a2'' R1) =
             Pfx bexpPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve bexpLUB_comm_BAndP_fst bexpLUB_comm_BAndP_snd.

Lemma aexpLUB_smallest : forall a a' a1 a2 aLUB,
  aexpLUB a1 a2 = aLUB ->
  prefixO aexpPO a a1 a' ->
  prefixO aexpPO a a2 a' ->
  prefixO aexpPO a aLUB a'.
Proof.
  intros a.
  induction a; intros.

  (* AHoleP *)
  destruct a', a1, a2, aLUB;
    simpl in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    generalize_nested_pfx_proofs;
    match goal with
    | [ H : (_ <= AHoleP)%a, H1 : (_ <= AHoleP)%a |- _ ] =>
      inversion H; inversion H1; subst
    end; auto.

  (* ANumP / AIdP *)
  1-2:
    destruct a', a1, a2, aLUB;
    cbv in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    try invert_pfx_equalities;
    invert_prefixO; invert_expression_inequalities; subst; auto.

  (* APlusP / AMinusP / AMultP *)
  all:
    destruct a', a1, a2, aLUB;
    invert_prefixO;
    match goal with
    | [ H : aexpLUB (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) = _, P : _, Q : _ |- _ ] =>
      inversion P; inversion Q; subst;
        [ first [ rewrite aexpLUB_comm_APlusP_1  in H
                | rewrite aexpLUB_comm_AMinusP_1 in H
                | rewrite aexpLUB_comm_AMultP_1  in H ]
        | first [ rewrite aexpLUB_comm_APlusP_3  in H
                | rewrite aexpLUB_comm_AMinusP_3 in H
                | rewrite aexpLUB_comm_AMultP_3  in H ]
        | first [ rewrite aexpLUB_comm_APlusP_2  in H
                | rewrite aexpLUB_comm_AMinusP_2 in H
                | rewrite aexpLUB_comm_AMultP_2  in H ]
        | idtac ]
    end;
    invert_pfx_equalities; auto;
    (* Destruct structure of LUB result and upper bound expression.  We get
       four cases: two trivial (hole <= hole/+, solve by auto), one
       contradiciting (+ <= hole, solve by dismiss_impossible...) and one more
       involved (_ + _ <= _ + _). *)
    match goal with
    | [ |- prefixO _ _ (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) ] =>
      inversion P; inversion Q; subst
    end; auto; dismiss_impossible_aexp_inequalities; subst;
    (* Invert inequalities to obtain facts required to trigger IH *)
    repeat match goal with
           | [ H1 : (_ _ _ <= _ _ ?A)%a
             |- prefixO _ _ _ (Pfx _ _ (_ _ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ <= _ _ ?A)%a
             |- prefixO _ _ _ (Pfx _ _ _ (_ _ ?A)) ] =>
             inv H1
           end;
    try match goal with
        | [ H0 : aexpLUB (Pfx _ (_ ?T _) (_ ?A _) _) (Pfx _ _ (_ ?B _) _) =
                          Pfx _ _ (_ ?C _) _
          , H1 : (?A <= ?T)%a, H2 : (?B <= ?T)%a, H3 : (?C <= ?T)%a
          , H4 : (_ ?D _ <= _ ?T _)%a |-
            prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
          first [ pose proof (aexpLUB_comm_APlusP_fst _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0)
                | pose proof (aexpLUB_comm_AMinusP_fst _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0)
                | pose proof (aexpLUB_comm_AMultP_fst _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0) ]
        end;
    try match goal with
        | [ H0 : aexpLUB (Pfx _ (_ _ ?T) (_ _ ?A) _) (Pfx _ _ (_ _ ?B) _) =
                          Pfx _ _ (_ _ ?C) _
          , H1 : (?A <= ?T)%a, H2 : (?B <= ?T)%a, H3 : (?C <= ?T)%a
          , H4 : (_ _ ?D <= _ _ ?T)%a |-
            prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          first [ pose proof (aexpLUB_comm_APlusP_snd _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0)
                | pose proof (aexpLUB_comm_AMinusP_snd _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0)
                | pose proof (aexpLUB_comm_AMultP_snd _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0) ]
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%a, H2 : (?B <= ?D)%a, H3 : (?D <= ?T)%a
          |- prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%a, H2 : (?B <= ?D)%a, H3 : (?D <= ?T)%a
          |- prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end; invert_prefixO; auto.
Qed.

Hint Resolve aexpLUB_smallest.

Lemma bexpLUB_smallest : forall b b' b1 b2 bLUB,
  bexpLUB b1 b2 = bLUB ->
  prefixO bexpPO b b1 b' ->
  prefixO bexpPO b b2 b' ->
  prefixO bexpPO b bLUB b'.
Proof.
  intros b;
  induction b; intros.

  (* BHoleP *)
  destruct b', b1, b2, bLUB;
    simpl in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    generalize_nested_pfx_proofs;
    match goal with
    | [ H : (_ <= BHoleP)%b, H1 : (_ <= BHoleP)%b |- _ ] =>
      inversion H; inversion H1; subst
    end; auto.

  (* BTrueP / BFalseP *)
  1-2: destruct b', b1, b2, bLUB;
    cbv in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    try invert_pfx_equalities;
    invert_prefixO; invert_expression_inequalities; subst; auto.

  (* BEqP / BLeP / BNotP / BAndP *)
  1-4:
    destruct b', b1, b2, bLUB;
    invert_prefixO;
    match goal with
    | [ H : bexpLUB (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) = _, P : _, Q : _ |- _ ] =>
      inversion P; inversion Q; subst;
        [ first [ rewrite bexpLUB_comm_BEqP_1  in H
                | rewrite bexpLUB_comm_BLeP_1  in H
                | rewrite bexpLUB_comm_BNotP_1 in H
                | rewrite bexpLUB_comm_BAndP_1 in H ]
        | first [ rewrite bexpLUB_comm_BEqP_3  in H
                | rewrite bexpLUB_comm_BLeP_3  in H
                | rewrite bexpLUB_comm_BNotP_3 in H
                | rewrite bexpLUB_comm_BAndP_3 in H ]
        | first [ rewrite bexpLUB_comm_BEqP_2  in H
                | rewrite bexpLUB_comm_BLeP_2  in H
                | rewrite bexpLUB_comm_BNotP_2 in H
                | rewrite bexpLUB_comm_BAndP_2 in H ]
        | idtac ]
    end;
    invert_pfx_equalities; auto;
    (* Destruct structure of LUB result and upper bound expression.  We get
       four cases: two trivial (hole <= hole/+, solve by auto), one
       contradiciting (+ <= hole, solve by dismiss_impossible...) and one more
       involved (_ + _ <= _ + _). *)
    match goal with
    | [ |- prefixO _ _ (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) ] =>
      inversion P; inversion Q; subst
    end; auto; dismiss_impossible_bexp_inequalities; subst;
    (* Invert inequalities to obtain facts required to trigger IH *)
    repeat match goal with
           | [ H1 : (_ _ <= _ ?A)%b
             |- prefixO _ _ _ (Pfx _ _ (_ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ <= _ _ ?A)%b
             |- prefixO _ _ _ (Pfx _ _ (_ _ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ <= _ _ ?A)%b
             |- prefixO _ _ _ (Pfx _ _ _ (_ _ ?A)) ] =>
             inv H1
           end;
    try match goal with
        | [ H0 : bexpLUB (Pfx _ (_ ?T) (_ ?A) _) (Pfx _ _ (_ ?B) _) =
                          Pfx _ _ (_ ?C) _
          , H1 : (?A <= ?T)%b, H2 : (?B <= ?T)%b, H3 : (?C <= ?T)%b
          , H4 : (_ ?D <= _ ?T)%b |-
            prefixO _ (_ ?T) (Pfx _ _ (_ ?C) _) (Pfx _ _ (_ ?D) _) ] =>
          pose proof (bexpLUB_comm_BNotP _ _ _ _ _ _ _ H1 H2 H3 H0)
        end;
    try match goal with
        | [ H0 : bexpLUB (Pfx _ (_ ?T _) (_ ?A _) _) (Pfx _ _ (_ ?B _) _) =
                         Pfx _ _ (_ ?C _) _
         , H1 : (?A <= ?T)%a, H2 : (?B <= ?T)%a, H3 : (?C <= ?T)%a
         , H4 : (_ ?D _ <= _ ?T _)%b |-
           prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
         first [ pose proof (bexpLUB_comm_BEqP_fst _ _ _ _ _ _ _ _ _ _ _
                                                   H1 H2 H3 H0)
               | pose proof (bexpLUB_comm_BLeP_fst _ _ _ _ _ _ _ _ _ _ _
                                                   H1 H2 H3 H0) ]
       end;
    try match goal with
        | [ H0 : bexpLUB (Pfx _ (_ _ ?T) (_ _ ?A) _) (Pfx _ _ (_ _ ?B) _) =
                          Pfx _ _ (_ _ ?C) _
          , H1 : (?A <= ?T)%a, H2 : (?B <= ?T)%a, H3 : (?C <= ?T)%a
          , H4 : (_ _ ?D <= _ _ ?T)%b |-
            prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          first [ pose proof (bexpLUB_comm_BEqP_snd _ _ _ _ _ _ _ _ _ _ _
                                                    H1 H2 H3 H0)
                | pose proof (bexpLUB_comm_BLeP_snd _ _ _ _ _ _ _ _ _ _ _
                                                    H1 H2 H3 H0) ]
        end;
    try match goal with
       | [ H0 : bexpLUB (Pfx _ (_ ?T _) (_ ?A _) _) (Pfx _ _ (_ ?B _) _) =
                         Pfx _ _ (_ ?C _) _
         , H1 : (?A <= ?T)%b, H2 : (?B <= ?T)%b, H3 : (?C <= ?T)%b
         , H4 : (_ ?D _ <= _ ?T _)%b |-
           prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
         pose proof (bexpLUB_comm_BAndP_fst _ _ _ _ _ _ _ _ _ _ _
                                            H1 H2 H3 H0)
       end;
    try match goal with
        | [ H0 : bexpLUB (Pfx _ (_ _ ?T) (_ _ ?A) _) (Pfx _ _ (_ _ ?B) _) =
                          Pfx _ _ (_ _ ?C) _
          , H1 : (?A <= ?T)%b, H2 : (?B <= ?T)%b, H3 : (?C <= ?T)%b
          , H4 : (_ _ ?D <= _ _ ?T)%b |-
            prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          pose proof (bexpLUB_comm_BAndP_snd _ _ _ _ _ _ _ _ _ _ _
                                             H1 H2 H3 H0)
        end;
    try match goal with
        | [ H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%a, H2 : (?B <= ?D)%a, H3 : (?D <= ?T)%a
          |- prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
          pose proof (aexpLUB_smallest T (Pfx _ _ D H3) (Pfx _ _ A P)
                                       (Pfx _ _ B Q ) (Pfx _ _ C R) H
                                       (PfxO _ _ _ _ _ _ H1)
                                       (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%a, H2 : (?B <= ?D)%a, H3 : (?D <= ?T)%a
          |- prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          pose proof (aexpLUB_smallest T (Pfx _ _ D H3) (Pfx _ _ A P)
                                       (Pfx _ _ B Q ) (Pfx _ _ C R) H
                                       (PfxO _ _ _ _ _ _ H1)
                                       (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : bexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%b, H2 : (?B <= ?D)%b, H3 : (?D <= ?T)%b
          |- prefixO _ (_ ?T) (Pfx _ _ (_ ?C) _) (Pfx _ _ (_ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
       end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : bexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%b, H2 : (?B <= ?D)%b, H3 : (?D <= ?T)%b
          |- prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%b, H2 : (?B <= ?D)%b, H3 : (?D <= ?T)%b
          |- prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end; invert_prefixO; auto.
Qed.

Hint Resolve bexpLUB_smallest.

Lemma comLUB_comm_CAssP_1 :
  forall x b1 P0 P1,
   comLUB (Pfx comPO (CAssP x b1) CHoleP P0)
          (Pfx comPO (CAssP x b1) CHoleP P1) =
   Pfx comPO (CAssP x b1) CHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CAssP_2 :
  forall x b1 b1' P0 P1,
   comLUB (Pfx comPO (CAssP x b1) (CAssP x b1') P0)
          (Pfx comPO (CAssP x b1) CHoleP P1) =
   Pfx comPO (CAssP x b1) (CAssP x b1') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CAssP_3 :
  forall x b1 b1' P0 P1,
   comLUB (Pfx comPO (CAssP x b1) CHoleP P0)
          (Pfx comPO (CAssP x b1) (CAssP x b1') P1) =
   Pfx comPO (CAssP x b1) (CAssP x b1') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve comLUB_comm_CAssP_1 comLUB_comm_CAssP_2 comLUB_comm_CAssP_3.

Lemma comLUB_comm_CSeqP_1 :
  forall c1 c2 P0 P1,
   comLUB (Pfx comPO (CSeqP c1 c2) CHoleP P0)
          (Pfx comPO (CSeqP c1 c2) CHoleP P1) =
   Pfx comPO (CSeqP c1 c2) CHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CSeqP_2 :
  forall c1 c2 c1' c2' P0 P1,
   comLUB (Pfx comPO (CSeqP c1 c2) (CSeqP c1' c2') P0)
          (Pfx comPO (CSeqP c1 c2) CHoleP P1) =
   Pfx comPO (CSeqP c1 c2) (CSeqP c1' c2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CSeqP_3 :
  forall c1 c2 c1' c2' P0 P1,
   comLUB (Pfx comPO (CSeqP c1 c2) CHoleP P0)
          (Pfx comPO (CSeqP c1 c2) (CSeqP c1' c2') P1) =
   Pfx comPO (CSeqP c1 c2) (CSeqP c1' c2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve comLUB_comm_CSeqP_1 comLUB_comm_CSeqP_2 comLUB_comm_CSeqP_3.

Lemma comLUB_comm_CIfP_1 :
  forall b c1 c2 P0 P1,
   comLUB (Pfx comPO (CIfP b c1 c2) CHoleP P0)
          (Pfx comPO (CIfP b c1 c2) CHoleP P1) =
   Pfx comPO (CIfP b c1 c2) CHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CIfP_2 :
  forall b b' c1 c2 c1' c2' P0 P1,
   comLUB (Pfx comPO (CIfP b c1 c2) (CIfP b' c1' c2') P0)
          (Pfx comPO (CIfP b c1 c2) CHoleP P1) =
   Pfx comPO (CIfP b c1 c2) (CIfP b' c1' c2') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CIfP_3 :
  forall b b' c1 c2 c1' c2' P0 P1,
   comLUB (Pfx comPO (CIfP b c1 c2) CHoleP P0)
          (Pfx comPO (CIfP b c1 c2) (CIfP b' c1' c2') P1) =
   Pfx comPO (CIfP b c1 c2) (CIfP b' c1' c2') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve comLUB_comm_CIfP_1 comLUB_comm_CIfP_2 comLUB_comm_CIfP_3.

Lemma comLUB_comm_CWhileP_1 :
  forall b c1 P0 P1,
   comLUB (Pfx comPO (CWhileP b c1) CHoleP P0)
          (Pfx comPO (CWhileP b c1) CHoleP P1) =
   Pfx comPO (CWhileP b c1) CHoleP P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CWhileP_2 :
  forall b b' c1 c1' P0 P1,
   comLUB (Pfx comPO (CWhileP b c1) (CWhileP b' c1') P0)
          (Pfx comPO (CWhileP b c1) CHoleP P1) =
   Pfx comPO (CWhileP b c1) (CWhileP b' c1') P0.
Proof. solve_LUB_comm_bin. Qed.

Lemma comLUB_comm_CWhileP_3 :
  forall b b' c1 c1' P0 P1,
   comLUB (Pfx comPO (CWhileP b c1) CHoleP P0)
          (Pfx comPO (CWhileP b c1) (CWhileP b' c1') P1) =
   Pfx comPO (CWhileP b c1) (CWhileP b' c1') P1.
Proof. solve_LUB_comm_bin. Qed.

Hint Resolve comLUB_comm_CWhileP_1 comLUB_comm_CWhileP_2 comLUB_comm_CWhileP_3.

Lemma comLUB_comm_CAssP :
  forall x a1 a1' a1'' a1LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CAssP x a1) (CAssP x a1') P0)
           (Pfx comPO (CAssP x a1) (CAssP x a1'') P1) =
            Pfx comPO (CAssP x a1) (CAssP x a1LUB) P ->
    aexpLUB (Pfx aexpPO a1 a1' R0) (Pfx aexpPO a1 a1'' R1) =
             Pfx aexpPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve comLUB_comm_CAssP.

Lemma comLUB_comm_CSeqP_fst :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CSeqP a1 a2) (CSeqP a1' a2') P0)
           (Pfx comPO (CSeqP a1 a2) (CSeqP a1'' a2'') P1) =
            Pfx comPO (CSeqP a1 a2) (CSeqP a1LUB a2LUB) P ->
    comLUB (Pfx comPO a1 a1' R0) (Pfx comPO a1 a1'' R1) =
            Pfx comPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma comLUB_comm_CSeqP_snd :
  forall a1 a2 a1' a2' a1'' a2'' a1LUB a2LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CSeqP a1 a2) (CSeqP a1' a2') P0)
           (Pfx comPO (CSeqP a1 a2) (CSeqP a1'' a2'') P1) =
            Pfx comPO (CSeqP a1 a2) (CSeqP a1LUB a2LUB) P ->
    comLUB (Pfx comPO a2 a2' R0) (Pfx comPO a2 a2'' R1) =
            Pfx comPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve comLUB_comm_CSeqP_fst comLUB_comm_CSeqP_snd.

Lemma comLUB_comm_CIfP_cond :
  forall b b' b'' a1 a2 a1' a2' a1'' a2'' bLUB a1LUB a2LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CIfP b a1 a2) (CIfP b' a1' a2') P0)
           (Pfx comPO (CIfP b a1 a2) (CIfP b'' a1'' a2'') P1) =
            Pfx comPO (CIfP b a1 a2) (CIfP bLUB a1LUB a2LUB) P ->
    bexpLUB (Pfx bexpPO b b' R0) (Pfx bexpPO b b'' R1) =
             Pfx bexpPO b bLUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma comLUB_comm_CIfP_fst :
  forall b b' b'' a1 a2 a1' a2' a1'' a2'' bLUB a1LUB a2LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CIfP b a1 a2) (CIfP b' a1' a2') P0)
           (Pfx comPO (CIfP b a1 a2) (CIfP b'' a1'' a2'') P1) =
            Pfx comPO (CIfP b a1 a2) (CIfP bLUB a1LUB a2LUB) P ->
    comLUB (Pfx comPO a1 a1' R0) (Pfx comPO a1 a1'' R1) =
            Pfx comPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma comLUB_comm_CIfP_snd :
  forall b b' b'' a1 a2 a1' a2' a1'' a2'' bLUB a1LUB a2LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CIfP b a1 a2) (CIfP b' a1' a2') P0)
           (Pfx comPO (CIfP b a1 a2) (CIfP b'' a1'' a2'') P1) =
            Pfx comPO (CIfP b a1 a2) (CIfP bLUB a1LUB a2LUB) P ->
    comLUB (Pfx comPO a2 a2' R0) (Pfx comPO a2 a2'' R1) =
            Pfx comPO a2 a2LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve comLUB_comm_CIfP_cond comLUB_comm_CIfP_fst comLUB_comm_CIfP_snd.

Lemma comLUB_comm_CWhileP_cond :
  forall b b' b'' a1 a1' a1'' bLUB a1LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CWhileP b a1) (CWhileP b'   a1'  ) P0)
           (Pfx comPO (CWhileP b a1) (CWhileP b''  a1'' ) P1) =
            Pfx comPO (CWhileP b a1) (CWhileP bLUB a1LUB) P ->
    bexpLUB (Pfx bexpPO b b' R0) (Pfx bexpPO b b'' R1) =
             Pfx bexpPO b bLUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Lemma comLUB_comm_CWhileP_fst :
  forall b b' b'' a1 a1' a1'' bLUB a1LUB P0 P1 P R0 R1 R,
    comLUB (Pfx comPO (CWhileP b a1) (CWhileP b'   a1'  ) P0)
           (Pfx comPO (CWhileP b a1) (CWhileP b''  a1'' ) P1) =
            Pfx comPO (CWhileP b a1) (CWhileP bLUB a1LUB) P ->
    comLUB (Pfx comPO a1 a1' R0) (Pfx comPO a1 a1'' R1) =
            Pfx comPO a1 a1LUB R.
Proof. solve_LUB_comm_fst_snd. Qed.

Hint Resolve comLUB_comm_CWhileP_cond comLUB_comm_CWhileP_fst.

Lemma comLUB_smallest : forall c c' c1 c2 cLUB,
  comLUB c1 c2 = cLUB ->
  prefixO comPO c c1 c' ->
  prefixO comPO c c2 c' ->
  prefixO comPO c cLUB c'.
Proof.
  intros c;
  induction c; intros.

  (* CHoleP *)
  destruct c', c1, c2, cLUB;
    simpl in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    generalize_nested_pfx_proofs;
    match goal with
    | [ H : (_ <= CHoleP)%c, H1 : (_ <= CHoleP)%c |- _ ] =>
      inversion H; inversion H1; subst
    end; auto.

  (* CSkipP *)
  destruct c', c1, c2, cLUB;
    cbv in H;
    destruct_matches; dismiss_impossible_aexp_inequalities;
    try invert_pfx_equalities;
    invert_prefixO; invert_expression_inequalities; subst; auto.

  (* CAssP / CSeqP / CIfP / CWhileP *)
  1-4:
    destruct c', c1, c2, cLUB;
    invert_prefixO;
    match goal with
    | [ H : comLUB (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) = _, P : _, Q : _ |- _ ] =>
      inversion P; inversion Q; subst;
        [ first [ rewrite comLUB_comm_CAssP_1   in H
                | rewrite comLUB_comm_CSeqP_1   in H
                | rewrite comLUB_comm_CIfP_1    in H
                | rewrite comLUB_comm_CWhileP_1 in H ]
        | first [ rewrite comLUB_comm_CAssP_3   in H
                | rewrite comLUB_comm_CSeqP_3   in H
                | rewrite comLUB_comm_CIfP_3    in H
                | rewrite comLUB_comm_CWhileP_3 in H ]
        | first [ rewrite comLUB_comm_CAssP_2   in H
                | rewrite comLUB_comm_CSeqP_2   in H
                | rewrite comLUB_comm_CIfP_2    in H
                | rewrite comLUB_comm_CWhileP_2 in H ]
        | idtac ]
    end;
    invert_pfx_equalities; auto;
    match goal with
    | [ |- prefixO _ _ (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) ] =>
      inversion P; inversion Q; subst
    end; auto; dismiss_impossible_com_inequalities; subst;
    (* Invert inequalities to obtain facts required to trigger IH *)
    repeat match goal with
           | [ H1 : (_ _ <= _ ?A)%c
             |- prefixO _ _ _ (Pfx _ _ (_ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ <= _ _ ?A)%c
             |- prefixO _ _ _ (Pfx _ _ (_ _ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ <= _ _ ?A)%c
             |- prefixO _ _ _ (Pfx _ _ _ (_ _ ?A)) ] =>
             inv H1
           | [ H1 : (_ _ _ _ <= _ _ _ ?A)%c
             |- prefixO _ _ _ (Pfx _ _ (_ _ _ ?A) _) ] =>
             inv H1
           | [ H1 : (_ _ _ _ <= _ _ _ ?A)%c
             |- prefixO _ _ _ (Pfx _ _ _ (_ _ _ ?A)) ] =>
             inv H1
           end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ _ ?T) (_ _ ?A) _) (Pfx _ _ (_ _ ?B) _) =
                          Pfx _ _ (_ _ ?C) _
          , H1 : (?A <= ?T)%a, H2 : (?B <= ?T)%a, H3 : (?C <= ?T)%a
          , H4 : (_ ?D <= _ ?T)%c |-
            prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          pose proof (comLUB_comm_CAssP _ _ _ _ _ _ _ _ H1 H2 H3 H0)
        end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ ?T _) (_ ?A _) _) (Pfx _ _ (_ ?B _) _) =
                         Pfx _ _ (_ ?C _) _
         , H1 : (?A <= ?T)%c, H2 : (?B <= ?T)%c, H3 : (?C <= ?T)%c
         , H4 : (_ ?D _ <= _ ?T _)%c |-
           prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
         pose proof (comLUB_comm_CSeqP_fst _ _ _ _ _ _ _ _ _ _ _
                                           H1 H2 H3 H0)
       end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ _ ?T) (_ _ ?A) _) (Pfx _ _ (_ _ ?B) _) =
                          Pfx _ _ (_ _ ?C) _
          , H1 : (?A <= ?T)%c, H2 : (?B <= ?T)%c, H3 : (?C <= ?T)%c
          , H4 : (_ _ ?D <= _ _ ?T)%c |-
            prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          first [ pose proof (comLUB_comm_CSeqP_snd _ _ _ _ _ _ _ _ _ _ _
                                                    H1 H2 H3 H0)
                | pose proof (comLUB_comm_CWhileP_fst _ _ _ _ _ _ _ _ _ _ _
                                                      H1 H2 H3 H0) ]
        end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ ?T _ _) (_ ?A _ _) _) (Pfx _ _ (_ ?B _ _) _) =
                         Pfx _ _ (_ ?C _ _) _
         , H1 : (?A <= ?T)%b, H2 : (?B <= ?T)%b, H3 : (?C <= ?T)%b
         , H4 : (_ ?D _ _ <= _ ?T _ _)%c |-
           prefixO _ (_ ?T _ _) (Pfx _ _ (_ ?C _ _) _)
                                (Pfx _ _ (_ ?D _ _) _) ] =>
         pose proof (comLUB_comm_CIfP_cond _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                                           H1 H2 H3 H0)
       end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ _ ?T _) (_ _ ?A _) _) (Pfx _ _ (_ _ ?B _) _) =
                         Pfx _ _ (_ _ ?C _) _
         , H1 : (?A <= ?T)%c, H2 : (?B <= ?T)%c, H3 : (?C <= ?T)%c
         , H4 : (_ _ ?D _ <= _ _ ?T _)%c |-
           prefixO _ (_ _ ?T _) (Pfx _ _ (_ _ ?C _) _)
                                (Pfx _ _ (_ _ ?D _) _) ] =>
         pose proof (comLUB_comm_CIfP_fst _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                                          H1 H2 H3 H0)
       end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ _ _ ?T) (_ _ _ ?A) _) (Pfx _ _ (_ _ _ ?B) _) =
                         Pfx _ _ (_ _ _ ?C) _
         , H1 : (?A <= ?T)%c, H2 : (?B <= ?T)%c, H3 : (?C <= ?T)%c
         , H4 : (_ _ _ ?D <= _ _ _ ?T)%c |-
           prefixO _ (_ _ _ ?T) (Pfx _ _ (_ _ _ ?C) _)
                                (Pfx _ _ (_ _ _ ?D) _) ] =>
         pose proof (comLUB_comm_CIfP_snd _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
                                          H1 H2 H3 H0)
       end;
    try match goal with
        | [ H0 : comLUB (Pfx _ (_ ?T _) (_ ?A _) _) (Pfx _ _ (_ ?B _) _) =
                         Pfx _ _ (_ ?C _) _
         , H1 : (?A <= ?T)%b, H2 : (?B <= ?T)%b, H3 : (?C <= ?T)%b
         , H4 : (_ ?D _ <= _ ?T _)%c |-
           prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _)
                              (Pfx _ _ (_ ?D _) _) ] =>
         pose proof (comLUB_comm_CWhileP_cond _ _ _ _ _ _ _ _ _ _ _
                                              H1 H2 H3 H0)
       end;
    try match goal with
        | [ H  : aexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%a, H2 : (?B <= ?D)%a, H3 : (?D <= ?T)%a
          |- prefixO _ (_ ?T) (Pfx _ _ (_ ?C) _) (Pfx _ _ (_ ?D) _) ] =>
          pose proof (aexpLUB_smallest T (Pfx _ _ D H3) (Pfx _ _ A P)
                                       (Pfx _ _ B Q) (Pfx _ _ C R) H
                                       (PfxO _ _ _ _ _ _ H1)
                                       (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ H  : bexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%b, H2 : (?B <= ?D)%b, H3 : (?D <= ?T)%b
          |- prefixO _ (_ ?T _ _) (Pfx _ _ (_ ?C _ _) _)
                                  (Pfx _ _ (_ ?D _ _) _) ] =>
          pose proof (bexpLUB_smallest T (Pfx _ _ D H3) (Pfx _ _ A P)
                                       (Pfx _ _ B Q) (Pfx _ _ C R) H
                                       (PfxO _ _ _ _ _ _ H1)
                                       (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ H  : bexpLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%b, H2 : (?B <= ?D)%b, H3 : (?D <= ?T)%b
          |- prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _)
                                (Pfx _ _ (_ ?D _) _) ] =>
          pose proof (bexpLUB_smallest T (Pfx _ _ D H3) (Pfx _ _ A P)
                                       (Pfx _ _ B Q ) (Pfx _ _ C R) H
                                       (PfxO _ _ _ _ _ _ H1)
                                       (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : comLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%c, H2 : (?B <= ?D)%c, H3 : (?D <= ?T)%c
          |- prefixO _ (_ ?T _) (Pfx _ _ (_ ?C _) _) (Pfx _ _ (_ ?D _) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : comLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%c, H2 : (?B <= ?D)%c, H3 : (?D <= ?T)%c
          |- prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _) (Pfx _ _ (_ _ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : comLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%c, H2 : (?B <= ?D)%c, H3 : (?D <= ?T)%c
          |- prefixO _ (_ _ ?T) (Pfx _ _ (_ _ ?C) _)
                                (Pfx _ _ (_ _ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : comLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%c, H2 : (?B <= ?D)%c, H3 : (?D <= ?T)%c
          |- prefixO _ (_ _ ?T _) (Pfx _ _ (_ _ ?C _) _)
                                  (Pfx _ _ (_ _ ?D _) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    try match goal with
        | [ IH : forall _ _ _ _, _ -> _ -> _ -> _
          , H  : comLUB (Pfx _ ?T ?A ?P) (Pfx _ ?T ?B ?Q) = Pfx _ ?T ?C ?R
          , H1 : (?A <= ?D)%c, H2 : (?B <= ?D)%c, H3 : (?D <= ?T)%c
          |- prefixO _ (_ _ _ ?T) (Pfx _ _ (_ _ _ ?C) _)
                                  (Pfx _ _ (_ _ _ ?D) _) ] =>
          specialize (IH (Pfx _ _ D H3) (Pfx _ _ A P)
                         (Pfx _ _ B Q ) (Pfx _ _ C R) H
                         (PfxO _ _ _ _ _ _ H1) (PfxO _ _ _ _ _ _ H2))
        end;
    invert_prefixO; auto.
Qed.

Hint Resolve comLUB_smallest.

Lemma comLUB_WhileP_leq2 :
  forall b c b1 b2 c1 c2 bLUB cLUB P0 P1 P,
   (b1 <= bLUB)%b ->
   (b2 <= bLUB)%b ->
   (c1 <= cLUB)%c ->
   (c2 <= cLUB)%c ->
   prefixO comPO (CWhileP b c)
           (comLUB (Pfx comPO (CWhileP b c) (CWhileP b1 c1) P0)
                   (Pfx comPO (CWhileP b c) (CWhileP b2 c2) P1))
           (Pfx comPO (CWhileP b c) (CWhileP bLUB cLUB) P).
Proof.
  intros b c b1 b2 c1 c2 bLUB cLUB P0 P1 P H H0 H1 H2.
  simpl.
  destruct_matches.
  generalize_nested_pfx_proofs.
  invert_expression_inequalities.
  subst.
  match goal with
  | [ H : bexpLUB (Pfx _ ?T ?X ?P) (Pfx _ ?T ?Y ?Q) = _
    , H1 : (?X <= ?L)%b, H2 : (?Y <= ?L)%b, H3 : (?L <= ?T)%b |- _ ] =>
    pose proof (bexpLUB_smallest _ _ _ _ _ H
                                 (PfxO _ _ _ _ P _  H1)
                                 (PfxO _ _ _ _ Q H3 H2))
  end.
  match goal with
  | [ H : comLUB (Pfx _ ?T ?X ?P) (Pfx _ ?T ?Y ?Q) = _
    , H1 : (?X <= ?L)%c, H2 : (?Y <= ?L)%c, H3 : (?L <= ?T)%c |- _ ] =>
    pose proof (comLUB_smallest _ _ _ _ _ H
                                (PfxO _ _ _ _ P _  H1)
                                (PfxO _ _ _ _ Q H3 H2))
  end.
  invert_prefixO.
  auto.
Qed.

Hint Resolve comLUB_WhileP_leq2.
