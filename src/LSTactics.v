(** * Tactics *)

Require Import Coq.Arith.Le.
Require Export Coq.Bool.Bool.
Require Import Coq.Lists.List.
Require Import Coq.Logic.ProofIrrelevance.

(** This modules contains a handful of useful tactics *)

(** A convenient variant of inversion *)
Ltac inv H := inversion H; subst; clear H.

(** Solve a conclusion that follows directly from its premise *)
Ltac direct := let H := fresh "H" in intro H; apply H.

(** Specialize all possible hypotheses *)
Ltac specialize_all :=
  repeat match goal with
         | [ H : ?P -> _, H' : ?P |- _ ] => specialize (H H')
         end.

(** Convert boolean and/or to propositions *)
Ltac reflect_andb_orb :=
  repeat match goal with
         | [ H : _ && _ = true |- _ ] =>
           rewrite andb_true_iff in H; destruct H
         | [ H : _ || _ = true |- _ ] =>
           rewrite  orb_true_iff in H
         | [ |- _ && _ = true ] =>
           rewrite andb_true_iff
         | [ H : ~(_ \/ _) |- _ ] =>
           apply Decidable.not_or in H; destruct H
         end.

(** Extract all hypotheses from conjuncts *)
Ltac destruct_conj :=
  repeat match goal with
         | [ H : _ /\ _ |- _ ] => destruct H
         end.

(** Extract all hypotheses from disjuncts *)
Ltac destruct_or :=
  repeat match goal with
         | [ H : _ \/ _ |- _ ] => destruct H
         end.

(** Rewrite In list application to alternatives **)
Ltac In_app_cleanup :=
  repeat match goal with
         | [ |- In _ (_ ++ _) ] =>
           apply in_or_app
         | [ H : In _ (_ ++ _) |- _ ] =>
           apply in_app_or in H
         end.

(** When proving False use a premiss with negation **)
Ltac apply_negation :=
  match goal with
  | [ H : ~ _ |- False ] => apply H
  end.

(** Destruct all matches in the goal and keep equality equations  *)
Ltac destruct_goal_matches :=
    repeat match goal with
           | [ H0 : _ |- context[match ?H with _ => _ end] ] =>
             let eqnH := fresh "eqnH" in
             destruct H eqn:eqnH
           end.

(** Destruct all matches in the goal and discard equality equations  *)
Ltac destruct_goal_matches' :=
    repeat match goal with
           | [ H0 : _ |- context[match ?H with _ => _ end] ] =>
             destruct H
           end.

(** Destruct all matches in the premises and keep equality equations  *)
Ltac destruct_premiss_matches :=
    repeat match goal with
           | [ H0 : context[match ?H with _ => _ end] |- _ ] =>
             let eqnH := fresh "eqnH" in
             destruct H eqn:eqnH
           end.

(** Destruct all matches in the premises and discard equality equations  *)
Ltac destruct_premiss_matches' :=
    repeat match goal with
           | [ H0 : context[match ?H with _ => _ end] |- _ ] =>
             destruct H
           end.

(** Generalizes dependent matches in the premises *)
Ltac generalize_dependent_matches :=
    repeat match goal with
           | [ H : match _ as _ return _ with _ => _ end _ = _ |- _ ] =>
             generalize H; clear H
           end.

(** Destruct all matches in goal and premiss and keep equality equations *)
Ltac destruct_matches := destruct_goal_matches; destruct_premiss_matches.

(** Destruct all matches in goal and premiss and discard equality equations *)
Ltac destruct_matches' := destruct_goal_matches'; destruct_premiss_matches'.

(** Equalize hypotheses with the same type using proof irrelevance *)
Ltac equalize_hypotheses :=
  do 10 match goal with
        | [ H1 : ?T, H2 : ?T |- _ ] =>
          let PI := fresh "PI" in
          pose proof (proof_irrelevance _ H1 H2) as PI; rewrite PI in *;
          clear PI; try clear H2
        end.

(** Solve trivial contradictions **)
Ltac nonsense :=
  repeat match goal with
         | [ H : ?X = true, H1 : ?X = false |- _ ] =>
           rewrite H in H1
         end; try contradiction; try discriminate.

(** Proves reflexivity of ordering relation *)
Ltac solve_reflexive :=
  let a := fresh "a" in
  intro a;
  (* induct on the expressions being compared, then trivially apply
     constructors *)
  induction a; auto.

(** Proves transitivity of ordering relation *)
Ltac solve_transitive :=
  let a := fresh "a" in
  let b := fresh "b" in
  let c := fresh "c" in
  let H := fresh "H" in
  intros a b c H; generalize dependent c;
  (* induction on the structure of ordering relation *)
  induction H;
  let c1 := fresh "c" in
  let H1 := fresh "H" in
  intros c1 H1; inversion H1;
  subst; specialize_all; eauto with arith.

(** Proves antisymmetry of ordering relation *)
Ltac solve_antisymmetric :=
  let a  := fresh "a" in
  let b  := fresh "b" in
  let H  := fresh "H" in
  let H1 := fresh "H" in
  intros a b H H1;
  (* induction on the structure of ordering relation *)
  induction H; inversion H1;
  (* specialize all possible hypotheses *)
  specialize_all; subst;
  (* this call to auto crucially depends on Hint Extern that triggers
     antisymmetry hypothesis for other ordering relations whenever possible *)
  auto with arith.
