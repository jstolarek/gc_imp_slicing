Require Import Lattice.
Require Import LSTactics.

Require Import Coq.Logic.ProofIrrelevance.

Inductive prefix {X : Type} (R : relation X) (e : X) : Type :=
| Pfx : forall a, R a e -> prefix R e.

Definition mkPfx {X : Type} (R : relation X) (S : reflexive R) (e : X) :
    prefix R e := Pfx R e e (S e).

Hint Unfold mkPfx.
Hint Transparent mkPfx.

Inductive prefixO {X : Type} (R : relation X) (e : X) : relation (prefix R e) :=
| PfxO : forall e1 e2 (r1 : R e1 e) (r2 : R e2 e),
    R e1 e2 -> prefixO R e (Pfx R e e1 r1) (Pfx R e e2 r2).

Hint Constructors prefix.
Hint Constructors prefixO.

Lemma reflexive_prefixO : forall {X : Type} (e : X) (R : relation X),
    reflexive R -> reflexive (prefixO R e).
Proof.
  intros X e R H a.
  destruct a.
  auto.
Qed.

Lemma transitive_prefixO : forall {X : Type} (e : X) (R : relation X),
    transitive R -> transitive (prefixO R e).
Proof.
  intros X e R H a b c H1 H2.
  destruct a, b, c.
  inversion H1. inversion H2.
  eauto.
Qed.

Lemma antisymmetric_prefixO : forall {X : Type} (e : X) (R : relation X),
    antisymmetric R -> antisymmetric (prefixO R e).
Proof.
  intros X e R H a b H1 H2.
  destruct a as [a r1], b as [b r2].
  inversion H1. subst. inversion H2. subst.
  specialize (H a b H4 H5). subst.
  pose proof (proof_irrelevance (R b e) r1 r2) as PI.
  rewrite PI.
  reflexivity.
Qed.

Hint Resolve reflexive_prefixO transitive_prefixO antisymmetric_prefixO.

(* For some reason reflexive_prefixO is not triggered by auto.  This is a
   workaround *)
Hint Extern 1 =>
  match goal with
  | [ |- prefixO _ _ ?A ?A ] => apply reflexive_prefixO
  end.

Lemma order_prefixO : forall {X : Type} (R : relation X) (e : X),
    order R -> order (prefixO R e).
Proof.
  intros X R e [Hr [Ha Ht]].
  repeat split; auto.
Qed.

Hint Resolve order_prefixO.

(* Proof terms in Pfx expressions often get in the way.  Below are some useful
   tactics for dealing with the problem. *)

(* Solves a goal of the form:

     Pfx R T e1 P1 = Pfx R T' e1' P2

   where R is an ordering relation forming a lattice, T = T' after call to
   simpl is the top element of the lattice and e1 = e1' via simpl.  P1 and P2
   are distinct proof terms.  We rely on proof irrelevance to show their
   equality.  *)
Ltac equal_prefixes :=
  try match goal with
      | [ |- Pfx ?R _ _ ?P1 = Pfx ?R _ _ ?P2 ] =>
        solve [ simpl;
                f_equal;
                let PI := fresh "PI" in
                pose proof (proof_irrelevance _ P1 P2) as PI;
                apply PI ]
      end.

Hint Extern 2 =>
  match goal with
  | [ |- Pfx ?R _ _ _ = Pfx ?R _ _ _ ] => equal_prefixes
  end.

(* Generalize proof terms in Pfx equality.  Useful for doing rewriting *)
Ltac generalize_pfx_proofs :=
  match goal with
  | [ |- Pfx _ _ _ ?P = Pfx _ _ _ ?R ] =>
    generalize P; generalize R
  end.

(** Generalize first encountered Pfx proof term *)
Ltac generalize_pfx_proof :=
  match goal with
  | [ |- context[Pfx _ _ _ ?P] ] => let p := fresh "P" in generalize P; intro p
  end.

(* Generalize proof terms nested in calls to slicing functions.  Specialized
   tactic needed in one of the proofs *)
Ltac generalize_nested_pfx_proofs :=
  try match goal with
       | [ |- prefixO _ _ (Pfx _ _ _ ?P) (_ _ (Pfx _ _ _ ?Q
                                       , (_ _ (Pfx _ _ _ ?R, _)))) ] =>
         let p := fresh "P" in
         let q := fresh "P" in
         let r := fresh "P" in
         generalize P; intro p;
         generalize Q; intro q;
         generalize R; intro r
      end;
  try match goal with
       | [ |- prefixO _ _ (Pfx _ _ _ ?P) (_ _ ( Pfx _ _ _ ?Q, Pfx _ _ _ ?R)) ] =>
         let p := fresh "P" in
         let q := fresh "P" in
         let r := fresh "P" in
         generalize P; intro p;
         generalize Q; intro q;
         generalize R; intro r
      end;
  try match goal with
       | [ |- prefixO _ _ (Pfx _ _ _ ?P) (Pfx _ _ _ ?Q) ] =>
         let p := fresh "P" in
         let q := fresh "P" in
         generalize P; intro p;
         generalize Q; intro q
      end;
  repeat match goal with
         | [ |- context[Pfx _ _ _ (?P ?R1 ?R2)] ] =>
           let p := fresh "P" in
           generalize (P R1 R2); intro p
         end;
  repeat match goal with
         | [ H : context[Pfx _ _ _ (?P ?R1 ?R2)] |- _ ] =>
           let p := fresh "P" in
           let E := fresh "E" in
           remember (P R1 R2) eqn:E; clear E
         end.

(* Finds identical Pfx expressions that differ by their proof terms and makes
   the proof terms equal *)
Ltac equalize_pfx_proofs :=
  repeat match goal with
         | [ H1 : context[Pfx ?R ?X ?Y ?P]
           , H2 : context[Pfx ?R ?X ?Y ?Q]
           , H3 : context[Pfx ?R ?X ?Y ?S]  |- _ ] =>
           let PI := fresh "PI" in
           pose proof (proof_irrelevance _ Q P) as PI;
           rewrite PI in H2; clear PI;
           let PI := fresh "PI" in
           pose proof (proof_irrelevance _ S P) as PI;
           rewrite PI in H3; clear PI
         | [ H1 : context[Pfx ?R ?X ?Y ?P]
           , H2 : context[Pfx ?R ?X ?Y ?Q] |- _ ] =>
           let PI := fresh "PI" in
           pose proof (proof_irrelevance _ Q P) as PI;
           rewrite PI in H2; clear PI
         end;
  repeat match goal with
         | [ H1 : context[Pfx ?R ?X ?Y ?P] |- context[Pfx ?R ?X ?Y ?Q] ] =>
           let PI := fresh "PI" in
           pose proof (proof_irrelevance _ P Q) as PI;
           rewrite PI in H1; clear PI
         end.

(* Find Pfx equalities and invert them *)
Ltac invert_pfx_equalities :=
  repeat match goal with
         | [ H : Pfx _ _ _ _ = Pfx _ _ _ _ |- _ ] =>
           (* Normal equalities *)
           inv H
         | [ H : (Pfx _ _ _ _, _) = (Pfx _ _ _ _, _) |- _ ] =>
           (* Prefixes paired with a state *)
           inv H
         end.

Ltac invert_pfx_equalities' :=
  repeat match goal with
         | [ H : (Pfx _ _ _ _, _) = (_, _) |- _ ] =>
           inv H
         | [ H : (_, _) = (_, Pfx _ _ _ _) |- _ ] =>
           inv H
         end.

(* Clear all Pfx equalities *)
Ltac clear_pfx_equalities :=
  repeat match goal with
         | [ H : Pfx _ _ _ _ = Pfx _ _ _ _ |- _ ] => clear H
         end.
