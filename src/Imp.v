(** * Imp language *)

(** This module defines syntax and semantics for a simple imperative language
    Imp.  A great majority of code in this file was adapted from Xavier Leroy's
    materials for DeepSpec Summer School 2017 course on CompCert, which again is
    based on Software Foundations series by Benjamin C. Pierce et al.
 *)

Require Import Coq.Arith.EqNat.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Arith.
Require Import Coq.Lists.List.

Require Import Ids.
Require Import ImpState.
Require Import ImpValues.

(** ** Imp Syntax *)

(* Arithmetic expressions with variables *)
Inductive aexp : Type :=
  | ANum   : nat   ->         aexp
  | AId    : id    ->         aexp
  | APlus  : aexp  -> aexp -> aexp
  | AMinus : aexp  -> aexp -> aexp
  | AMult  : aexp  -> aexp -> aexp.

Notation "a1 '+' a2" := (APlus  a1 a2) : aexp_scope.
Notation "a1 '-' a2" := (AMinus a1 a2) : aexp_scope.
Notation "a1 '*' a2" := (AMult  a1 a2) : aexp_scope.

Delimit Scope aexp_scope with aexp.

(* Boolean expressions *)
Inductive bexp : Type :=
  | BTrue  :                 bexp
  | BFalse :                 bexp
  | BEq    : aexp -> aexp -> bexp
  | BLe    : aexp -> aexp -> bexp
  | BNot   : bexp         -> bexp
  | BAnd   : bexp -> bexp -> bexp.

(* Imperative commands *)
Inductive com : Type :=
  | CSkip  :                        com
  | CAss   : id   -> aexp        -> com
  | CSeq   : com  -> com         -> com
  | CIf    : bexp -> com  -> com -> com
  | CWhile : bexp -> com         -> com.

(* A convenient notation for commands *)
Notation "'SKIP'" :=
  CSkip.
Notation "x '::=' a" :=
  (CAss x a) (at level 60).
Notation "c1 ;; c2" :=
  (CSeq c1 c2) (at level 80, right associativity).
Notation "'WHILE' b 'DO' c 'END'" :=
  (CWhile b c) (at level 80, right associativity).
Notation "'IFB' c1 'THEN' c2 'ELSE' c3 'FI'" :=
  (CIf c1 c2 c3) (at level 80, right associativity).

(** ** Tracing *)

(* Arithmetic expressions trace.  AId trace records a precise nat that was
   assigned to an id during execution. *)
Inductive aexpT : Type :=
  | ANumT   : nat ->          aexpT
  | AIdT    : id    -> nat -> aexpT
  | APlusT  : aexpT -> aexpT -> aexpT
  | AMinusT : aexpT -> aexpT -> aexpT
  | AMultT  : aexpT -> aexpT -> aexpT.

Notation "a1 '+' a2" := (APlusT  a1 a2) : aexpt_scope.
Notation "a1 '-' a2" := (AMinusT a1 a2) : aexpt_scope.
Notation "a1 '*' a2" := (AMultT  a1 a2) : aexpt_scope.

Delimit Scope aexpt_scope with impt.

(* Boolean expressions trace *)
Inductive bexpT : Type :=
  | BTrueT  :                   bexpT
  | BFalseT :                   bexpT
  | BEqT    : aexpT -> aexpT -> bexpT
  | BLeT    : aexpT -> aexpT -> bexpT
  | BNotT   : bexpT ->          bexpT
  | BAndT   : bexpT -> bexpT -> bexpT.

(* Imperative commands trace.  Records which conditional branches were taken
   and how loops were executed.  *)
Inductive comT : Type :=
  | CSkipT      :                          comT
  | CAssT       : id    -> aexpT        -> comT
  | CSeqT       : comT  -> comT         -> comT
  | CIfTrue     : bexpT -> comT         -> comT
  | CIfFalse    : bexpT -> comT         -> comT
  | CWhileTrue  : bexpT -> comT -> comT -> comT
  | CWhileFalse : bexpT                 -> comT.

Notation "'SKIP'" :=
  CSkipT : trace_scope.
Notation "x '::=' a" :=
  (CAssT x a) (at level 60) : trace_scope.
Notation "c1 ;; c2" :=
  (CSeqT c1 c2) (at level 80, right associativity) : trace_scope.
Notation "'IF_True' c1 'THEN' c2 'FI'" :=
  (CIfTrue c1 c2) (at level 80, right associativity) : trace_scope.
Notation "'IF_False' c1 'ELSE' c3 'FI'" :=
  (CIfFalse c1 c3) (at level 80, right associativity) : trace_scope.
Notation "'WHILE_True' b 'DO' c 'END' ';' c'" :=
  (CWhileTrue b c c') (at level 80, right associativity) : trace_scope.
Notation "'WHILE_False' b 'END'" :=
  (CWhileFalse b) (at level 80, right associativity) : trace_scope.

Delimit Scope trace_scope with imp_t.

(** ** Semantics *)

Reserved Notation "t '::' a ',' st '\\' v"
         (at level 40, st at level 39, v at level 38).

Inductive aevalR : aexp -> state -> nat -> aexpT -> Type :=
| E_Num : forall st n,
    ANumT n :: ANum n, st \\ n
| E_Id : forall n id st,
    lookup st id = Some n ->
    AIdT id n :: AId id, st \\ n
| E_Plus : forall st a1 a2 n1 n2 t1 t2,
    t1 :: a1, st \\ n1 ->
    t2 :: a2, st \\ n2 ->
    APlusT t1 t2 :: APlus a1 a2, st \\ (n1 + n2)
| E_Minus : forall st a1 a2 n1 n2 t1 t2,
    t1 :: a1, st \\ n1 ->
    t2 :: a2, st \\ n2 ->
    AMinusT t1 t2 :: AMinus a1 a2, st \\ (n1 - n2)
| E_Mult : forall st a1 a2 n1 n2 t1 t2,
    t1 :: a1, st \\ n1 ->
    t2 :: a2, st \\ n2 ->
    AMultT t1 t2 :: AMult a1 a2, st \\ (n1 * n2)
where "t '::' a ',' st '\\' v" := (aevalR a st v t) : aevalR_scope.

Delimit Scope aevalR_scope with ar.

Inductive bevalR : bexp -> state -> bool -> bexpT -> Type :=
  | E_BTrue : forall (st : state),
      BTrueT :: BTrue, st \\ true
  | E_BFalse : forall (st : state),
      BFalseT :: BFalse, st \\ false
  | E_BEq : forall (v1 v2 : nat) (st : state) (a1 a2 : aexp) t1 t2,
      (t1 :: a1, st \\ v1)%ar ->
      (t2 :: a2, st \\ v2)%ar ->
      BEqT t1 t2 :: BEq a1 a2, st \\ beq_nat v1 v2
  | E_BLe : forall (v1 v2 : nat) (st : state) (a1 a2 : aexp) t1 t2,
      (t1 :: a1, st \\ v1)%ar ->
      (t2 :: a2, st \\ v2)%ar ->
      BLeT t1 t2 :: BLe a1 a2, st \\ Nat.leb v1 v2
  | E_BNot : forall (b : bool) (st : state) (e : bexp) t,
      t :: e, st \\ b  ->
      BNotT t :: BNot e, st \\ negb b
  | E_BAnd : forall (b1 b2 : bool) (st : state) (e1 e2 : bexp) t1 t2,
      t1 :: e1, st \\ b1 ->
      t2 :: e2, st \\ b2 ->
      BAndT t1 t2 :: BAnd e1 e2, st \\ andb b1 b2
where "t '::' b ',' st '\\' v" := (bevalR b st v t) : bevalR_scope.

Delimit Scope bevalR_scope with br.

Inductive cevalR : com -> state -> state -> comT -> Type :=
  | E_Skip : forall (st : state),
      CSkipT :: SKIP, st \\ st
  | E_Ass  : forall (x : id) (a : aexp) (st : state) (v : nat) t,
      (t :: a, st \\ v)%ar ->
      CAssT x t :: (x ::= a), st \\ (update st x v)
  | E_Seq : forall st st1 st2 (c1 c2 : com) t1 t2,
      t1 :: c1, st  \\ st1 ->
      t2 :: c2, st1 \\ st2 ->
      CSeqT t1 t2 :: (c1 ;; c2), st \\ st2
  | E_IfTrue : forall st st1 (b : bexp) (c1 c2 : com) tb tc,
      (tb :: b , st \\ true)%br ->
       tc :: c1, st \\ st1 ->
      CIfTrue tb tc :: (IFB b THEN c1 ELSE c2 FI), st \\ st1
  | E_IfFalse : forall st st1 (b : bexp) (c1 c2 : com) tb tc,
      (tb :: b , st \\ false)%br ->
       tc :: c2, st \\ st1 ->
      CIfFalse tb tc :: (IFB b THEN c1 ELSE c2 FI), st \\ st1
  | E_WhileFalse : forall st (b : bexp) (c : com) t,
      (t :: b, st \\ false)%br ->
      CWhileFalse t :: (WHILE b DO c END), st \\ st
  | E_WhileTrue : forall st st1 st2 (b : bexp) (c : com) t tc tw,
      (t  :: b, st \\ true)%br ->
       tc :: c, st \\ st1 ->
       tw :: (WHILE b DO c END), st1 \\ st2 ->
      CWhileTrue t tc tw :: (WHILE b DO c END), st  \\ st2
where "t '::' c ',' st '\\' v" :=  (cevalR c st v t) : cevalR_scope.

Delimit Scope cevalR_scope with cr.

Lemma com_evaluation_preserves_wellformedness :
  forall {st : state} {c : com} {st1 : state}
         {t : comT} (ev : (t :: c, st \\ st1)%cr),
    statePWellFormed (statePartialize st) ->
    statePWellFormed (statePartialize st1).
Proof.
  intros st c st1 t0 ev H.
  induction ev; auto.
  erewrite <- updateP_statePartialize_comm; eauto.
  Unshelve. all: auto.
Qed.

Hint Resolve com_evaluation_preserves_wellformedness.

Ltac assert_state_wellformed :=
  match goal with
  | [ EV : (_ :: _, ?S1 \\ ?S2)%cr
    , H : statePWellFormed (statePartialize ?S1) |- _ ] =>
    pose proof (com_evaluation_preserves_wellformedness EV H)
  end.
