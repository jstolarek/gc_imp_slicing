(** * Program state *)

Require Import Coq.Lists.List.
Require Import Coq.Lists.ListSet.

Require Import Ids.
Require Import ImpValues.
Require Import Lattice.
Require Import PrefixSets.
Require Import LSTactics.

(** State *)

Inductive state : Type :=
| StateNil  : state
| StateCons : forall (k : id) (v : nat) (st : state), state.

Hint Constructors state.

Fixpoint keys (st : state) : list id :=
  match st with
  | StateNil => nil
  | StateCons k _ st' => k :: keys st'
  end.

Fixpoint lookup (st : state) (k : id) :=
  match st with
  | StateNil => None
  | StateCons k' v st' => if beq_id k k' then Some v else lookup st' k
  end.

(** Update nats already present in the state *)
Fixpoint update (st : state) (k : id) (v : nat) : state :=
  match st with
  | StateNil => StateNil
  | StateCons k' v' st' => if beq_id k k'
                           then StateCons k v st'
                           else StateCons k' v' (update st' k v)
  end.

(** Partial state *)

Inductive stateP : Type :=
| StatePNil  : stateP
| StatePCons : forall (k : id) (v : natP) (st : stateP), stateP.

Hint Constructors stateP.

Fixpoint empty_stateP (ids : list id) : stateP :=
  match ids with
  | nil => StatePNil
  | k :: ids' => StatePCons k NHoleP (empty_stateP ids')
  end.

Fixpoint lookupP (stP : stateP) (k : id):=
  match stP with
  | StatePNil => NHoleP
  | StatePCons k' v st' => if beq_id k k' then v else lookupP st' k
  end.

Fixpoint statePartialize (st : state) : stateP :=
  match st with
  | StateNil => StatePNil
  | StateCons k v st' => StatePCons k (NatP v) (statePartialize st')
  end.

Fixpoint keysP (st : stateP) : list id :=
  match st with
  | StatePNil => nil
  | StatePCons k _ st' => k :: keysP st'
  end.

Fixpoint updateP (st : stateP) (k : id) (v : natP) : stateP :=
  match st with
  | StatePNil => StatePNil
  | StatePCons k' v' st' => if beq_id k k'
                            then StatePCons k v st'
                            else StatePCons k' v' (updateP st' k v)
  end.

Inductive statePWellFormed : stateP -> Prop :=
| StatePNilWF  : statePWellFormed StatePNil
| StatePConsWF : forall k v st, ~(In k (keysP st)) -> statePWellFormed st ->
                                statePWellFormed (StatePCons k v st).

Hint Constructors statePWellFormed.

Lemma updateP_head : forall k v1 v2 st,
    updateP (StatePCons k v1 st) k v2 = StatePCons k v2 st.
Proof.
  intros.
  simpl.
  rewrite (beq_id_refl k).
  reflexivity.
Qed.

Lemma updateP_tail : forall k1 k2 v1 v2 st (H : k1 <> k2),
    updateP (StatePCons k1 v1 st) k2 v2 =
    StatePCons k1 v1 (updateP st k2 v2).
Proof.
  intros k1 k2 v1 v2 st H.
  simpl.
  destruct (beq_id k2 k1) eqn:Heq.
  * rewrite beq_id_true_iff in Heq. symmetry in Heq. contradiction.
  * reflexivity.
Qed.

Lemma lookupP_head : forall k v st,
    lookupP (StatePCons k v st) k = v.
Proof.
  intros.
  simpl.
  rewrite beq_id_refl.
  trivial.
Qed.

Hint Resolve lookupP_head.

Lemma lookupP_tail : forall k k1 v st,
    k <> k1 ->
    lookupP (StatePCons k v st) k1 = lookupP st k1.
Proof.
  intros.
  simpl.
  destruct_matches; reflect_id; subst; try contradiction; auto.
Qed.

Hint Resolve lookupP_tail.

Theorem lookup_Some_NatP : forall (st : state) id n,
    lookup st id = Some n -> lookupP (statePartialize st) id = NatP n.
Proof.
  intros.
  induction st; simpl in *.

  inversion H.

  destruct (beq_id id k) eqn:Heq. inversion H. reflexivity.

  apply IHst. assumption.
Qed.

Theorem lookup_Some_In : forall (st : state) id n,
    lookup st id = Some n -> In id (keys st).
Proof.
  intros st id n H.
  induction st; simpl in *.

  - discriminate.
  - destruct (beq_id id k) eqn:Heq.
    * left. apply beq_id_true_iff in Heq. auto.
    * right. auto.
Qed.

Hint Resolve lookup_Some_In.

Theorem lookupP_NatP_In : forall (st : stateP) id n,
    lookupP st id = NatP n -> In id (keysP st).
Proof.
  intros st id n H.
  induction st; simpl in *.

  - discriminate.
  - destruct (beq_id id k) eqn:Heq.
    * left. apply beq_id_true_iff in Heq. auto.
    * right. auto.
Qed.

Hint Resolve lookupP_NatP_In.

Lemma lookupP_contra : forall st k n,
       lookupP st k = NHoleP -> lookupP st k = NatP n -> False.
Proof.
  intros st k n H1 H2.
  rewrite H1 in H2.
  discriminate.
Qed.

Hint Resolve lookupP_contra.

Theorem lookupP_updateP : forall st k v (ev : In k (keysP st)),
    (lookupP (updateP st k v) k) = v.
Proof.
  intros st k v ev.
  induction st; destruct ev eqn:Heq; simpl in *.
  - subst. rewrite (beq_id_refl k). simpl. rewrite (beq_id_refl k). reflexivity.
  - destruct (beq_id k k0) eqn:Heq'; simpl.
    * rewrite (beq_id_refl k). reflexivity.
    * rewrite Heq'. auto.
Qed.

Hint Resolve lookup_Some_NatP lookupP_updateP.

Lemma updateP_absent : forall k v st,
   ~ List.In k (keysP st) -> updateP st k v = st.
Proof.
  intros k v st H.
  induction st; auto.
  destruct (beq_id k0 k) eqn:HEq; reflect_id; subst.
  - elim H. simpl. auto.
  - rewrite updateP_tail; auto.
    simpl in *.
    reflect_andb_orb.
    rewrite IHst; auto.
Qed.

Hint Resolve updateP_absent.

Lemma lookupP_absent : forall k st,
    ~ List.In k (keysP st) -> lookupP st k = NHoleP.
Proof.
  intros k st H.
  induction st; auto.
  simpl in *.
  reflect_andb_orb.
  destruct_matches; reflect_id; subst; nonsense; auto.
Qed.

Hint Resolve lookupP_absent.

Inductive statePO : relation stateP :=
| StateNilPO  : StatePNil <= StatePNil
| StateConsPO : forall (k : id) (v1 v2 : natP) (st1 st2 : stateP),
    (v1 <= v2)%v ->
    st1 <= st2 ->
    (StatePCons k v1 st1) <= (StatePCons k v2 st2)
where "st1 '<=' st2" := (statePO st1 st2) : statePO_scope.

Delimit Scope statePO_scope with s.

Hint Constructors statePO.

Ltac dismiss_impossible_state_inequalities :=
  repeat match reverse goal with
         | [ H : (StatePNil <= StatePCons _ _ _)%s |- _ ] => inversion H
         | [ H : (StatePCons _ _ _ <= StatePNil)%s |- _ ] => inversion H
         end.

Theorem reflexive_statePO : reflexive statePO.
Proof.
  solve_reflexive.
Qed.

Theorem transitive_statePO : transitive statePO.
Proof.
  solve_transitive.
Qed.

Theorem antisymmetric_statePO : antisymmetric statePO.
Proof.
  intros a b H H1.
  induction H.

  - auto.
  - inversion H1; subst.
    specialize_all.
    pose proof (antisymmetric_natPO v1 v2  H H4) as Veq.
    subst.
    reflexivity.
Qed.

Hint Resolve reflexive_statePO transitive_statePO antisymmetric_statePO.

Theorem order_statePO : order statePO.
Proof. eauto. Qed.

Definition mkStPfx (st : stateP) := mkPfx statePO reflexive_statePO st.

Definition unPfxState {stP : stateP} (pfx : prefix statePO stP) : stateP :=
  match pfx with
  | Pfx _ _ st _ => st
  end.

Hint Unfold mkStPfx unPfxState.
Hint Transparent mkStPfx unPfxState.

Lemma empty_stateP_lookup : forall ids k, lookupP (empty_stateP ids) k = NHoleP.
Proof.
  intros ids k.
  induction ids.
  - auto.
  - simpl. destruct (beq_id k a) eqn:Heq; trivial.
Qed.

(* If lookupP in state st1 yields a hole then lookup in smaller state will also
   yield hole *)
Lemma lookupP_smaller_hole : forall st1 st2 id,
    (st2 <= st1)%s ->
    lookupP st1 id = NHoleP -> lookupP st2 id = NHoleP.
Proof.
  intros st1 st2 id HS H.
  generalize dependent st2.
  induction st1; intros st2 HS; destruct st2; simpl;
    (* Solves two simple cases *)
    auto;
    (* Solves a contradiciting case, provides facts for recursive case *)
    inversion HS.

  subst.
  (* unfolds call to lookupP in one of the presmisses *)
  simpl in *.

  (* Case analysis for id equality in lookupP *)
  destruct (beq_id id k).
  * subst. inversion H2. trivial.
  * auto.
Qed.

Hint Resolve lookupP_smaller_hole.

Theorem lookupP_monotone : forall (st1 st2 : stateP) k,
    (st1 <= st2)%s -> (lookupP st1 k <= lookupP st2 k)%v.
Proof.
  intros.
  induction H.
  - (* StatePNil *)
    auto.
  - (* StatePCons *)
    unfold lookupP.
    destruct (beq_id k k0) eqn:Heq; auto.
Qed.

Hint Resolve empty_stateP_lookup lookupP_monotone.

Theorem state_partialize_keys : forall (st : state),
    keys st = keysP (statePartialize st).
Proof.
  intro st.
  induction st.
  - reflexivity.
  - simpl. rewrite IHst. reflexivity.
Qed.

Theorem empty_state_partialize_keys : forall (st : state),
    (empty_stateP (keysP (statePartialize st)) <= empty_stateP (keys st))%s.
Proof.
  intro st.
  rewrite state_partialize_keys.
  eauto.
Qed.

Hint Resolve state_partialize_keys empty_state_partialize_keys.

Theorem in_partial_state : forall (k : id) (st : state),
    In k (keys st) -> In k (keysP (statePartialize st)).
Proof.
  intros k st H.
  rewrite <- state_partialize_keys.
  assumption.
Qed.

Hint Resolve in_partial_state.

Theorem empty_state_keys : forall (st : state),
    keys st = keysP (empty_stateP (keys st)).
Proof.
  intro st.
  induction st.
  - reflexivity.
  - simpl. rewrite <- IHst. reflexivity.
Qed.

Hint Resolve empty_state_keys.

Theorem state_In_cons_leq : forall k v st st2,
    (StatePCons k v st <= statePartialize st2)%s ->
    In k (keysP (statePartialize st2)).
Proof.
  intros.
  inv H.
  simpl. auto.
Qed.

Hint Resolve state_In_cons_leq.

Theorem state_in_cons_tail : forall k k2 v st,
    In k (keysP st) -> In k (keysP (StatePCons k2 v st)).
Proof.
  intros.
  simpl. auto.
Qed.

Hint Resolve state_in_cons_tail.

Theorem empty_stateP_update : forall st k v,
    (empty_stateP (keysP (statePartialize st)) <=
     updateP (empty_stateP (keys st)) k v)%s.
Proof.
  intros st k v.
  induction st; simpl.
  - eauto.
  - destruct (beq_id k k0) eqn:idEq.
    * rewrite beq_id_true_iff in idEq.
      rewrite idEq. eauto.
    * eauto.
Qed.

Hint Resolve empty_stateP_update.

Theorem in_keys_monotone : forall k st1 st2,
    (st1 <= st2)%s -> In k (keysP st1) -> In k (keysP st2).
Proof.
  intros k st1; induction st1; intros.
  - inv H. assumption.
  - inv H. inv H0; simpl; auto.
Qed.

Hint Resolve in_keys_monotone.

Theorem lookup_Some_In2 : forall (st : state) id n,
    lookup st id = Some n -> In id (keysP (empty_stateP (keys st))).
Proof.
  intros st id n H.
  pose proof (lookup_Some_In st id n H) as inSt.
  rewrite empty_state_keys in inSt.
  assumption.
Qed.

Hint Resolve lookup_Some_In2.

Theorem updateP_with_greater : forall st k v1 v2,
    (v1 <= v2)%v -> (updateP st k v1 <= updateP st k v2)%s.
Proof.
  intros st k v1 v2 H.
  induction st; simpl; [|destruct (beq_id k k0)]; eauto.
Qed.

Hint Resolve updateP_with_greater.

Theorem updateP_monotone : forall st1 st2 k v,
    (st1 <= st2)%s -> (updateP st1 k v <= updateP st2 k v)%s.
Proof.
  intros st1; induction st1; intros st2 x v' H.
  - inv H. auto.
  - inv H. destruct (beq_id k x) eqn:HEq; reflect_id; subst.
    + repeat rewrite updateP_head. auto.
    + repeat rewrite updateP_tail; auto.
Qed.

Hint Resolve updateP_monotone.

Theorem updateP_shadow : forall st k v v0,
    updateP (updateP st k v0) k v = updateP st k v.
Proof.
  intros st k v v0.
  induction st.
  - auto.
  - simpl. destruct_matches; reflect_id; subst.
    + rewrite updateP_head. trivial.
    + rewrite updateP_tail; try f_equal; auto.
Qed.

Hint Resolve updateP_shadow.

Theorem updateP_keys_const : forall k v st,
    keysP st = keysP (updateP st k v).
Proof.
  intros.
  induction st; simpl; destruct_matches; reflect_id; subst; simpl; f_equal;
    auto.
Qed.

Hint Resolve updateP_keys_const.

Theorem updateP_of_existing : forall k v st,
    lookupP st k = v -> updateP st k v = st.
Proof.
  intros k v st H.
  induction st; simpl; destruct_matches; reflect_id; subst; f_equal; auto.
  apply IHst.
  rewrite lookupP_tail; auto.
Qed.

Hint Resolve updateP_of_existing.

Theorem updateP_statePartialize_comm : forall st x v vp,
    vp = natPartialize v ->
    (updateP (statePartialize st) x vp = statePartialize (update st x v))%s.
Proof.
  intros st.
  induction st; intros; subst; simpl.
  - auto.
  - destruct (beq_id x k) eqn:HEq; simpl; try erewrite IHst; eauto.
Qed.

Hint Resolve updateP_statePartialize_comm.

Theorem updateP_statePartialize_comm_leq : forall st x v vp,
    (vp <= natPartialize v)%v ->
    (updateP (statePartialize st) x vp <= statePartialize (update st x v))%s.
Proof.
  intros st.
  induction st; intros; subst; simpl.
  - auto.
  - destruct (beq_id x k) eqn:HEq; simpl; eauto.
Qed.

Hint Resolve updateP_statePartialize_comm_leq.

Arguments updateP st k v : simpl never.

Lemma statePWellFormed_after_update : forall k v st,
    statePWellFormed st -> statePWellFormed (updateP st k v).
Proof.
  intros k v st H.
  induction H; simpl in *.
  - auto.
  - destruct (beq_id k0 k) eqn:HEq; reflect_id; subst.
    + rewrite updateP_head. auto.
    + rewrite updateP_tail; auto.
      apply StatePConsWF; auto.
      rewrite <- updateP_keys_const; auto.
Qed.

Hint Resolve statePWellFormed_after_update.

Lemma ordered_states_WellFormed : forall st1 st2,
    statePWellFormed st2 -> (st1 <= st2)%s -> statePWellFormed st1.
Proof.
  intros st1; induction st1; intros st2 H H0; auto.
  inv H; dismiss_impossible_state_inequalities.
  inv H0. apply StatePConsWF.
  - intro. elim H1. eauto.
  - eauto.
Qed.

Hint Resolve ordered_states_WellFormed.

Theorem empty_stateP_leq : forall (st : stateP),
    (empty_stateP (keysP st) <= st)%s.
Proof.
  intros st.
  induction st; simpl; auto.
Qed.

Hint Resolve empty_stateP_leq.

Theorem empty_state_leq : forall (st : state),
    (empty_stateP (keys st) <= statePartialize st)%s.
Proof.
  intros st.
  induction st; simpl; auto.
Qed.

Hint Resolve empty_state_leq.

Theorem empty_state_leq_trans : forall (st : state) (st1 : stateP),
    (st1 <= statePartialize st)%s -> (empty_stateP (keys st) <= st1)%s.
Proof.
  intros st.
  induction st; intros st1 H; simpl; inv H; auto.
Qed.

Hint Resolve empty_state_leq_trans.

Theorem in_empty_state : forall (k : id) (st : state),
    In k (keys st) -> In k (keysP (empty_stateP (keys st))).
Proof.
  intros.
  rewrite <- empty_state_keys.
  assumption.
Qed.

Hint Resolve in_empty_state.

Theorem empty_state_prefixO : forall st stPfx P,
    prefixO statePO (statePartialize st)
            (Pfx statePO (statePartialize st) (empty_stateP (keys st)) P) stPfx.
Proof.
  intros st [a s] P.
  apply PfxO.
  generalize dependent a.
  induction st; intros a s;
    inversion s; simpl; auto.
Qed.

Hint Resolve empty_state_prefixO.

Theorem empty_state_P_prefixO : forall st stPfx P,
    prefixO statePO (statePartialize st)
            (Pfx statePO (statePartialize st)
                 (empty_stateP (keysP (statePartialize st)))
                 P) stPfx.
Proof.
  intros st [a s] P.
  apply PfxO.
  induction s; simpl; eauto.
Qed.

Hint Resolve empty_state_P_prefixO.

Lemma statePNil_leq_L : forall st,
    (StatePNil <= statePartialize st)%s -> st = StateNil.
Proof.
  intros st H. induction st.
  - trivial.
  - simpl in H. inv H.
Qed.

Hint Resolve statePNil_leq_L.

Lemma statePNil_leq_R : forall st,
    (statePartialize st <= StatePNil)%s -> st = StateNil.
Proof.
  intros st H. induction st.
  - trivial.
  - simpl in H. inv H.
Qed.

Hint Resolve statePNil_leq_R.

Lemma update_nil : forall st k v, update st k v = StateNil -> st = StateNil.
Proof.
  intros st k v H.
  induction st.
  - trivial.
  - simpl in H. destruct_matches; inv H.
Qed.

Hint Resolve update_nil.

Lemma updateP_nil_leq : forall st k v,
    (updateP st k v <= StatePNil)%s -> st = StatePNil.
Proof.
  intros st k v H.
  induction st.
  - trivial.
  - unfold updateP in H. destruct_matches; inv H.
Qed.

Hint Resolve updateP_nil_leq.

Ltac basic_empty_state_reasoning :=
  repeat match goal with
         | [ H : StateNil = update _ _ _ |- _ ] =>
           symmetry in H
         end;
  try match goal with
         | [ H : (StatePNil <= statePartialize ?S)%s |- _ ] =>
           let P := fresh "P" in
           pose proof (statePNil_leq_L _ H) as P;
           subst
         end;
  try match goal with
         | [ H : (statePartialize ?S <= StatePNil)%s |- _ ] =>
           let P := fresh "P" in
           pose proof (statePNil_leq_R _ H) as P;
           subst
         end;
  try match goal with
         | [ H : update ?S ?K ?V = StateNil |- _ ] =>
           let P := fresh "P" in
           pose proof (update_nil S K V H) as P;
           subst
         end;
  try match goal with
         | [ H : (updateP ?S ?K ?V <= StatePNil)%s |- _ ] =>
           let P := fresh "P" in
           pose proof (updateP_nil_leq S K V H) as P;
           subst
         end.

Lemma stateP_leq_nil_and_cons_False : forall st k v st',
    (StatePNil <= st)%s -> (StatePCons k v st' <= st)%s -> False.
Proof.
  intros st k v st' HNil HCons.
  inversion HNil. subst.
  inversion HCons.
Qed.

Lemma stateP_cons_leq_nil_False : forall k v st,
    (StatePCons k v st <= StatePNil)%s -> False.
Proof.
  intros k v st H.
  inversion H.
Qed.

Lemma stateP_nil_leq_cons_False : forall k v st,
    (StatePNil <= StatePCons k v st)%s -> False.
Proof.
  intros k v st H.
  inversion H.
Qed.

Lemma stateP_leq_V : forall st1 st2 k1 k2 v1 v2,
    (StatePCons k1 v1 st1 <= StatePCons k2 v2 st2)%s -> (v1 <= v2)%v.
Proof.
  intros st1 st2 k1 k2 v1 v2 H.
  inversion H. assumption.
Qed.

Lemma stateP_leq_St : forall st1 st2 k1 k2 v1 v2,
    (StatePCons k1 v1 st1 <= StatePCons k2 v2 st2)%s -> (st1 <= st2)%s.
Proof.
  intros st1 st2 k1 k2 v1 v2 H.
  inversion H. assumption.
Qed.

Hint Resolve stateP_leq_nil_and_cons_False stateP_cons_leq_nil_False
     stateP_nil_leq_cons_False stateP_leq_V stateP_leq_St.

Lemma update_hole_leq : forall st1 st2 k v,
    (st1 <= statePartialize (update st2 k v))%s ->
    (lookupP st1 k = NHoleP) ->
    (st1 <= statePartialize st2)%s.
Proof.
  intro st1.
  induction st1; intros st2 k1 v1 H1 H2;
    inv H1; destruct st2; simpl in *; trivial; try discriminate;
    destruct_matches; simpl in *; try inv H;
      try rewrite beq_id_true_iff in *; try rewrite beq_id_false_iff in *;
      inv H5; try contradiction; eauto.
Qed.

Hint Resolve update_hole_leq.

Theorem updateP_with_smaller :
  forall (st1 st2 : stateP) (k : id) (v1 : natP),
    (st1 <= st2)%s -> (v1 <= lookupP st2 k)%v ->
    (updateP st1 k v1 <= st2)%s.
Proof.
  intros st1 st2 k v1 H1 H3.
  generalize dependent st2.
  induction st1; intros st2 H1 H3; destruct st2.
  + simpl. auto.
  + simpl. auto.
  + inversion H1.
  + unfold updateP. destruct (beq_id k k0) eqn:Heq.
    * rewrite beq_id_true_iff in Heq. subst.
      inversion H1; subst. simpl in H3. rewrite (beq_id_refl k1) in H3.
      auto.
    * inversion H1; subst. apply StateConsPO.
      - assumption.
      - apply IHst1.
        ++ assumption.
        ++ rewrite beq_id_false_iff in Heq. simpl in H3.
           destruct (beq_id k k1) eqn:Heq'.
           rewrite beq_id_true_iff in Heq'. contradiction.
           assumption.
Qed.

Hint Resolve updateP_with_smaller.

Theorem updateP_with_value_leq : forall st1 st2 k v,
    (st1 <= st2)%s -> lookupP st1 k = v -> (st1 <= updateP st2 k v)%s.
Proof.
  intros st1.
  induction st1; intros; inv H; unfold updateP at 1; simpl; destruct_matches;
    reflect_id; subst; auto.
Qed.

Hint Resolve updateP_with_value_leq.

Theorem updateP_cons_leq : forall v1 v2 v3 k st1 st2,
  (v2 <= v3)%v ->
  (StatePCons k v1 st1 <= st2)%s -> (StatePCons k v2 st1 <= updateP st2 k v3)%s.
Proof.
  intros v1 v2 v3 k st1 st2 H H0.
  inv H0.
  rewrite updateP_head.
  auto.
Qed.

Hint Resolve updateP_cons_leq.

Theorem updateP_arg_leq : forall k v1 v2 st st2,
   In k (keysP (statePartialize st2)) ->
   (StatePCons k v1 st <= statePartialize (update st2 k v2))%s ->
   (v1 <= NatP v2)%v.
Proof.
  intros k v1 v2 st st2 H0 H.
  rewrite <- updateP_statePartialize_comm with (vp := NatP v2) in H; auto.
  pose proof (lookupP_monotone _ _ k H); auto.
  rewrite lookupP_updateP in *; auto.
  rewrite lookupP_head in *; auto.
Qed.

Hint Resolve updateP_arg_leq.

Lemma updateP_smaller_state_hole : forall stP st k v,
      (stP <= statePartialize st)%s ->
      (updateP stP k NHoleP <= statePartialize (update st k v))%s.
Proof.
  intros stP st k v H.
  erewrite <- updateP_statePartialize_comm with (vp := NatP v); eauto.
Qed.

Hint Resolve updateP_smaller_state_hole.

Lemma updateP_smaller_state_hole_2 : forall stP st k v,
      (stP <= statePartialize (update st k v))%s ->
      (updateP stP k NHoleP <= statePartialize st)%s.
Proof.
  intros stP; induction stP; intros.
  - erewrite <- updateP_statePartialize_comm with (vp := NatP v) in H; eauto.
  - destruct st; simpl in H.
    * dismiss_impossible_state_inequalities.
    * destruct_matches.
      + reflect_id; subst. inv H. unfold updateP. simpl. rewrite beq_id_refl.
        auto.
      + inv H. unfold updateP. simpl. destruct_matches; try discriminate. eauto.
Qed.

Hint Resolve updateP_smaller_state_hole_2.

Fixpoint stateLUB {st : stateP} (st1 st2 : prefix statePO st) :
    prefix statePO st.
  refine (
      match st1, st2 with
      | Pfx _ _ st1' st1'R, Pfx _ _ st2' st2'R =>
       match st, st1' as st1Ref, st2' as st2Ref
             return (st1Ref <= st)%s -> (st2Ref <= st)%s -> _ with
       | _, StatePNil, StatePNil => fun R _ =>
         Pfx statePO _ StatePNil R
       | StatePCons k v st', StatePCons k1 v1 st1'', StatePCons k2 v2 st2'' =>
         fun R S =>
         let v1leq := stateP_leq_V _ _ _ _ _ _ R in
         let v2leq := stateP_leq_V _ _ _ _ _ _ S in
         let vlub  := natLUB (Pfx _ _ v1 v1leq) (Pfx _ _ v2 v2leq) in
         match vlub with
         | Pfx _ _ v' T =>
           let st1leq := stateP_leq_St _ _ _ _ _ _ R in
           let st2leq := stateP_leq_St _ _ _ _ _ _ S in
           let stlub  := stateLUB st' (Pfx _ _ st1'' st1leq)
                                      (Pfx _ _ st2'' st2leq) in
           match stlub with
           | Pfx _ _ stlub' _ =>
             Pfx statePO (StatePCons k v st') (StatePCons k v' stlub') _
           end
         end
       | _, StatePCons _ _ _, StatePNil => fun R S => _
       | _, StatePNil, StatePCons _ _ _ => fun R S => _
       | StatePNil, StatePCons _ _ _, StatePCons _ _ _ => fun R S => _
       end st1'R st2'R
      end
    ).
  all: eauto.
Defined.

(* Find premisses that contain stateLUB with Pfx arguments and remember these
   arguments *)
Ltac remember_stateLUB_args :=
  repeat match goal with
         | [ H : context[stateLUB (Pfx ?R ?S ?X ?P) _ ] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R S X P) as p
         | [ H : context[stateLUB _ (Pfx ?R ?S ?Y ?Q)] |- _ ] =>
           let q := fresh "q" in
           remember (Pfx R S Y Q) as q
         end.

(* Solves both stateLUB_prefixO lemmas below *)
Ltac solve_stateLUB_prefixO :=
  intros st st1pfx st2pfx;
  (* Induction on the state forming a lattice.  There are two inductive cases:
     one when state is empty (nil) and another when it is a cons. *)
  induction st;
    (* We begin by decomposing prefix expressions about states *)
    destruct st1pfx as [st1 Hst1], st2pfx as [st2 Hst2];
    (* Now invert evidence that st1 and st2 are smaller than the state at the
       top of a lattice.  This reveals the structure of st1 and st2: either a
       nil (first inductive case) or a cons (second case). *)
    inversion Hst1; inversion Hst2; subst;
    (* simplify call to stateLUB.  This turns nil case into a trivial one.  In
       the cons case it uncovers matches on nats (inlined call to natLUB)
       and call to stateLUB.*)
    simpl;
    (* solves the nil case *)
    auto 2;

    (* Now for the cons case *)

    (* Destruct nats being matched in inlined call to natLUB *)
    repeat match goal with
           | [ H : (?V <= _)%v |- context[match ?V with _ => _ end] ] =>
             destruct V
           end;

    (* Destruct a match on stateLUB, remember the equation as a premiss *)
    destruct_goal_matches;
    (* We need to extract arguments passed to stateLUB in the match.  We will
       need them to specialize th inductive hypothesis. *)
    remember_stateLUB_args;
    match reverse goal with
    | [ H : stateLUB ?P ?Q = _, IH : forall _ _, _ |- _ ] =>
      (* Specialize IH *)
      specialize (IH P Q);
        (* Replace result if stateLUB in IH with the remembered result of a
           match *)
        rewrite H in IH;
        (* Substitute back the stateLUB arguments we have remembered *)
        subst;
        (* Inverting the IH gives us inequalities needed to prove the goal *)
        inversion IH
    end;

    (* Need also inequalities between nats.  Interestingly this is only
       needed by the _snd lemma *)
    repeat match goal with
           | [ H : (NatP _ <= _)%v |- _ ] => inversion H; subst; clear H
           end; auto.

Lemma stateLUB_prefixO_fst : forall st st1 st2,
    prefixO statePO st st1 (stateLUB st1 st2).
Proof.
  solve_stateLUB_prefixO.
Qed.

Lemma stateLUB_prefixO_snd : forall st st1 st2,
    prefixO statePO st st2 (stateLUB st1 st2).
Proof.
  solve_stateLUB_prefixO.
Qed.

Hint Resolve stateLUB_prefixO_fst stateLUB_prefixO_snd.

Lemma stateLUB_prefixO_fst_weak : forall st st1 st2 st3,
    prefixO statePO st st1 st2 -> prefixO statePO st st1 (stateLUB st2 st3).
Proof.
  intros st st1 st2 st3 H.
  eapply transitive_prefixO; eauto.
Qed.

Hint Resolve stateLUB_prefixO_fst_weak.

Lemma stateLUB_leq : forall st (st1 st2 stT : prefix statePO st),
    prefixO statePO st st1 stT ->
    prefixO statePO st st2 stT ->
    prefixO statePO st (stateLUB st1 st2) stT.
Proof.
  intro st.
  (* Proof by induction on the shape of state at the top of the lattice *)
  induction st; intros [st1 Hst1] [st2 Hst2] [stT HstT] H H0;
    (* invert inequalities to learn more about st1, st2 and stT *)
    inversion Hst1; inversion Hst2; inversion HstT; subst;
    (* make Nil case trivial, uncover matches in cons case *)
    simpl;
    (* Solve the Nil case *)
    auto;
    (* Now the Cons case.  Begin by destroying matches in definition of
       stateLUB *)
    destruct_matches;
    (* Remember Pfx arguments passed to stateLUB... *)
    remember_stateLUB_args;
    (* ...and use them to sepcialize the inductive hypothesis *)
    specialize (IHst p q);
    (* Deconstruct PfxO goal into simpler one *)
    apply PfxO;
    (* Invert a Pfx equality we have in the context *)
    invert_pfx_equalities;
    (* Invert all prefixO facts *)
    repeat match goal with
           | [ H : prefixO _ _ _ _ |- _ ] =>
             inversion H; clear H
           end;
    (* Invert all inequalities between StatePCons *)
    repeat match goal with
           | [ H : (StatePCons _ _ _ <= StatePCons _ _ _)%s |- _ ] =>
             inversion H; clear H
           end;
    subst;
    (* Specialize the inductive hypothesis with a state arguments and two
       prefixO arguments *)
    match goal with
    | [ IH : forall (_ : prefix _ ?ST), _, H : (?S <= ?ST)%s
        |- (StatePCons _ _ _ <= StatePCons _ _ ?S)%s ] =>
      specialize (IH (Pfx _ ST S H));
        repeat match goal with
               | [ IH : prefixO _ ?ST (Pfx _ ?ST ?S1 _) (Pfx _ ?ST ?S2 _) -> _
                 , H1 : (?S1 <= ?ST)%s, H2 : (?S2 <= ?ST)%s
                 , H : (?S1 <= ?S2)%s |- _ ] =>
                 let pO := fresh "pO" in
                 pose proof (PfxO _ ST _ _ H1 H2 H) as pO;
                   (* Equalize proof terms so that they don't get in the way
                      when doing rewrites *)
                   equalize_pfx_proofs;
                   specialize (IH pO)
               end
    end;
    (* Rewrite the IH with the equality we have in the context, then invert *)
    match goal with
    | [ H1 : context[stateLUB ?A ?B], H2 : stateLUB ?A ?B = _ |- _ ] =>
      rewrite H2 in H1; inversion H1
    end;
    (* finish with auto *)
    auto.
Qed.

Hint Resolve stateLUB_leq.

Lemma stateLUB_preserves_prefixO : forall st st1 st2 st3,
      prefixO statePO st st1 st2 ->
      prefixO statePO st (stateLUB st1 st3) (stateLUB st2 st3).
Proof. eauto. Qed.

Hint Resolve stateLUB_preserves_prefixO.

Ltac generalize_stateLUB_pfx_proof :=
  try match goal with
      | [ H : stateLUB (Pfx _ _ _ ?P) _ = _ |- _ ] =>
        generalize H; clear H; generalize P; intros
      end;
  try match goal with
      | [ H : stateLUB _ (Pfx _ _ _ ?P) = _ |- _ ] =>
        generalize H; clear H; generalize P; intros
      end.

Ltac stateLUB_rewrite :=
  repeat match goal with
         | [ H : stateLUB _ _ = ?R |- context[?R] ] => rewrite <- H
         end.

Ltac destruct_stateLUB_args :=
  match goal with
  | [ |- context [ stateLUB ?S1 ?S2 ] ] => destruct S1, S2
  | [ H : context [ stateLUB ?S1 ?S2 ] |- _ ] => destruct S1, S2
  end.

Ltac lookupP_updateP_reasoning :=
  try match goal with
  | [ H : lookupP ?S ?K = NatP _, H1 : updateP _ ?K _ = ?S |- _ ] =>
    let P := fresh "H" in
    (* First, conclude that K is in S *)
    pose proof (lookupP_NatP_In _ _ _ H) as P;
    (* K is also in the updated state... *)
    rewrite <- H1 in P;
    (* ...but updating doesn't change the state's domain *)
    rewrite <- updateP_keys_const in P;
    (* Now substitute updateP into lookupP... *)
    rewrite <- H1 in H;
    (* ...and simplify from lookupP-updateP of the same key *)
    rewrite lookupP_updateP in H;
    (* Solve lookupP_updateP assumption using P we have derived earlier *)
    [ idtac | apply P ]
  end;
  (* Try reason about updateP shadowing *)
  try match goal with
      | [ H : updateP _ ?K _ = ?S
        |- context[stateLUB (Pfx _ _ (updateP ?S ?K _) ?P)] ] =>
        (* Generalize the proof term so that we can rewrite its type as well *)
        generalize P;
        (* Rewrite using a premise... *)
        rewrite <- H;
        (* ... and reason about updateP shadowing *)
        rewrite updateP_shadow;
        (* Intro generalized proof term *)
        intro
  end;
  (* Or just try updateP shadow *)
  try rewrite updateP_shadow.
