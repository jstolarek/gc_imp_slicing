(** * Lattice *)

(** This file provides some basic definitions and lemmas for relations, orders,
    lattices and Galois connections.  Comments in square brackets are references
    to definitions, lemmas and theorems from "Introduction to Lattices and
    Order" by Davey and Priestley. *)

Require Import LSTactics.

(** ** Relations *)

Definition relation (X : Type) := X -> X -> Prop.

Definition reflexive {X : Type} (R : relation X) :=
  forall a : X, R a a.

Definition transitive {X : Type} (R : relation X) :=
  forall a b c : X, (R a b) -> (R b c) -> (R a c).

Definition antisymmetric {X : Type} (R : relation X) :=
  forall a b : X, (R a b) -> (R b a) -> a = b.

(** ** Ordering and lattices *)

(* [Definition 1.2] *)
Definition order {X : Type} (R : relation X) :=
  (reflexive R) /\ (antisymmetric R) /\ (transitive R).

Reserved Notation "a '<=' b" (at level 70).

(* [Definition 1.34(i)] *)
Definition monotone {X Y : Type} (R : relation X) (Q : relation Y)
           (f : X -> Y) :=
  forall a b : X, R a b -> Q (f a) (f b).

Hint Unfold reflexive transitive antisymmetric order monotone :
  lattice_db core.

Reserved Notation "a '<=[' R '/' Q ']' b"
         (at level 70, R at level 69, Q at level 68).

(** ** Galois connections *)

(** Let (P, <=) and (Q, <=) be two lattices and let there exist two monotone
    functions:

       F : P -> Q
       G : Q -> P

    If it is the case that:

       forall p q, F p <= q <-> p <= G q

    then F and G form a Galois connection.  F is called the "lower adjoint" and
    G is called the "upper adjoint". *)
(* [Definition 7.23] *)
Definition galoisConnection {P Q: Type}
         {P_rel  : relation P}
         {Q_rel  : relation Q}
         (lo_adj : P -> Q)
         (up_adj : Q -> P)
         (P_ord  : order P_rel)
         (Q_ord  : order Q_rel) :=
   forall p q, Q_rel (lo_adj p) q <-> P_rel p (up_adj q).

(** Consistency (inflation/deflation)

    Let (F : P -> Q) and (G : Q -> P) form a Galois connection. We have:

      p <= G (F p)   (inflation)
      F (G q) <= q   (deflation)

    *)
(* [Lemma 7.26(Gal1)] *)

Definition inflation {P Q : Type}
         (P_rel  : relation P)
         (lo_adj : P -> Q)
         (up_adj : Q -> P) :=
  forall (p : P), P_rel p (up_adj (lo_adj p)).

Definition deflation {P Q : Type}
         (Q_rel  : relation Q)
         (lo_adj : P -> Q)
         (up_adj : Q -> P) :=
  forall (q : Q), Q_rel (lo_adj (up_adj q)) q.

Hint Unfold inflation deflation : lattice_db core.

Ltac prove_consistency :=
  intro;
  repeat autounfold with lattice_db in *;
  unfold galoisConnection in *;
  (* destruct all conjuncts in hypotheses *)
  destruct_conj;
  (* make use of Galois connection property *)
  repeat
    match goal with
    | [ H : forall _ _, _ <-> (?P _ (?up _)) |- ?P _ (?up _) ] =>
        apply H; trivial
    | [ H : forall _ _, (?Q (?lo _) _) <-> _ |- ?Q (?lo _) _ ] =>
        apply H; trivial
    end.

Lemma gc_inflating {P Q : Type}
         {P_rel  : relation P}
         {Q_rel  : relation Q}
         {lo_adj : P -> Q}
         {up_adj : Q -> P}
         {P_ord  : order P_rel}
         {Q_ord  : order Q_rel}
         (gc     : galoisConnection lo_adj up_adj P_ord Q_ord) :
  inflation P_rel lo_adj up_adj.
Proof.
  prove_consistency.
Qed.

Lemma gc_deflating {P Q : Type}
         {P_rel  : relation P}
         {Q_rel  : relation Q}
         {lo_adj : P -> Q}
         {up_adj : Q -> P}
         {P_ord  : order P_rel}
         {Q_ord  : order Q_rel}
         (gc     : galoisConnection lo_adj up_adj P_ord Q_ord) :
  deflation Q_rel lo_adj up_adj.
Proof.
  prove_consistency.
Qed.

(** Monotonicity of lower and upper adjoint

    Let (F : P -> Q) and (G : Q -> P) form a Galois connection. We have:

      p1 <= p2    ==>   F p1 <= F p2
      q1 <= q2    ==>   G q1 <= G q2

    *)
(* [Lemma 7.26(Gal2)] *)
Ltac solve_monotonicty :=
  unfold order, monotone in *; intros; destruct_conj;
  repeat match goal with
         | [ H0 : transitive ?P |- ?P _ _ ] =>
           (* Use transitivity of ordering relation, but only if first case
              solves trivially *)
           eapply H0; [ solve [ eassumption ] | idtac ]
         | [ H0 : transitive ?P |- ?P _ _ ] =>
           (* Use transitivity of ordering relation, but only if second case
              solves trivially *)
           eapply H0; [ idtac | solve [ eassumption ] ]
         | [ |- ?P ?a (_ (_ ?a)) ] =>
           (* Use consistency *)
           eapply gc_inflating
         | [ |- ?Q (_ (_ ?a)) ?a ] =>
           (* Use consistency *)
           eapply gc_deflating
         | [ H : galoisConnection _ _ _ _ |- _ ] =>
           (* Use Galois connection property. Ordering of match cases matters
              here.  If this is not last then repeat will loop *)
           apply H
         end; try eassumption.

Lemma gc_monotone_lo_adj {P Q : Type}
         {P_rel  : relation P}
         {Q_rel  : relation Q}
         {lo_adj : P -> Q}
         {up_adj : Q -> P}
         {P_ord  : order P_rel}
         {Q_ord  : order Q_rel}
         (gc     : galoisConnection lo_adj up_adj P_ord Q_ord) :
  (monotone P_rel Q_rel lo_adj).
Proof.
  solve_monotonicty.
Qed.

Lemma gc_monotone_up_adj {P Q : Type}
         {P_rel  : relation P}
         {Q_rel  : relation Q}
         {lo_adj : P -> Q}
         {up_adj : Q -> P}
         {P_ord  : order P_rel}
         {Q_ord  : order Q_rel}
         (gc     : galoisConnection lo_adj up_adj P_ord Q_ord) :
  (monotone Q_rel P_rel up_adj).
Proof.
  solve_monotonicty.
Qed.

(** Let (P, <=) and (Q, <=) be two lattices and let there exist two monotone
    functions:

       F : P -> Q
       G : Q -> P

    If they are deflating and inflating:

       (forall q, F (G q) <= q) /\ (forall p, p <= G (F p))

    then F and G form a Galois connection.

    Variable names specialized to slicing-specific ones.

    Outline of the proof in each direction:

      bwd(V) <= E ==> fwd (bwd(V)) <= fwd (E) ==> V <= fwd(E)

      V <= fwd(E) ==> bwd (V) <= bwd (fwd(E)) ==> bwd(V) <= E

    First transition follows from monotonicity, second from consistency and
    transitivity of <=.
*)

(* [Lemma 7.26] *)
Lemma cons_mono__gc {exp val : Type}
      {expRel : relation exp}
      {valRel : relation val}
      {expOrd : order expRel}
      {valOrd : order valRel}
      {fwd    : exp -> val}
      {bwd    : val -> exp}
      (fwdC   : deflation expRel bwd fwd)
      (bwdC   : inflation valRel bwd fwd)
      (mfwd   : monotone expRel valRel fwd)
      (mbwd   : monotone valRel expRel bwd) :
  galoisConnection bwd fwd valOrd expOrd.
Proof.
  (* prove each direction of Galois connection equivalence separately *)
  split; intro H;
  (* unfold all the definitions related to ordering and lattices *)
  repeat autounfold with lattice_db in *; destruct_conj;
    [ specialize (mfwd _ _ H) | specialize (mbwd _ _ H) ];
    match goal with
    | [ H : forall _ _ _, _ -> _ -> ?R _ _ |- ?R _ _ ] =>
      (* transitivity of ordering relation *)
      eapply H
    end; specialize_all; eassumption.
Qed.

(** Galois connection implies minimality and consistency. *)

(* Consistency is identical to inflation. *)
Definition consistency {P Q : Type}
         (P_rel  : relation P)
         (lo_adj : P -> Q)
         (up_adj : Q -> P) :=
  forall (p : P), P_rel p (up_adj (lo_adj p)).

(* Galois connection implies consistency (inflation) of forward and backward
   slicing.  This is already proven as lemma gc_inflating, repeated here for
   clarity. *)
Lemma gc_implies_consistency {exp val : Type}
      {expRel : relation exp}
      {valRel : relation val}
      {expOrd : order expRel}
      {valOrd : order valRel}
      {fwd    : exp -> val}
      {bwd    : val -> exp}
      (gc     : galoisConnection bwd fwd valOrd expOrd) :
  consistency valRel bwd fwd.
Proof.
  apply (gc_inflating gc).
Qed.

Definition minimality {P Q : Type}
         (P_rel  : relation P)
         (Q_rel  : relation Q)
         (lo_adj : P -> Q)
         (up_adj : Q -> P) :=
  forall (p : P) (q : Q), P_rel p (up_adj q) -> Q_rel (lo_adj p) q.

(* [Lemma 7.33]

   Given a Galois connection, for any partial expression e whose forward slice
   is consistent with a partial value v, the result of backward slicing v will
   always be smaller than e. *)
Lemma gc_implies_minimality {exp val : Type}
      {expRel : relation exp}
      {valRel : relation val}
      {expOrd : order expRel}
      {valOrd : order valRel}
      {fwd    : exp -> val}
      {bwd    : val -> exp}
      (gc     : galoisConnection bwd fwd valOrd expOrd) :
  minimality valRel expRel bwd fwd.
Proof.
  intros v e H.
  unfold galoisConnection in gc. apply gc.
  assumption.
Qed.
