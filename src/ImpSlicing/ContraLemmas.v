(** Boring lemmas used to discharge impossible case matches in definitions of
    slicing functions *)

Require Import ImpPartial.
Require Import ImpState.

Ltac boring_contra :=
  intros;
  match goal with
  | [ H : (_ <= _)%a |- _ ] => inversion H
  | [ H : (_ <= _)%b |- _ ] => inversion H
  | [ H : (_ <= _)%c |- _ ] => inversion H
  | [ H : (_ <= _)%s |- _ ] => inversion H
  end.

(** ** Aexp contras *)

Lemma aexpIdNumContra : forall i n, (AIdP i <= ANumP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpPlusNumContra : forall a1 a2 n, (APlusP a1 a2 <= ANumP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMinusNumContra : forall a1 a2 n, (AMinusP a1 a2 <= ANumP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMultNumContra : forall a1 a2 n, (AMultP a1 a2 <= ANumP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpNumIdContra : forall n i, (ANumP n <= AIdP i)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpPlusIdContra : forall a1 a2 n, (APlusP a1 a2 <= AIdP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMinusIdContra : forall a1 a2 n, (AMinusP a1 a2 <= AIdP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMultIdContra : forall a1 a2 n, (AMultP a1 a2 <= AIdP n)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpIdPlusContra : forall a1 a2 i, (AIdP i <= APlusP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpNumPlusContra : forall a1 a2 n, (ANumP n <= APlusP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMinusPlusContra : forall a1 a2 a3 a4,
    (AMinusP a1 a2 <= APlusP a3 a4)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMultPlusContra : forall a1 a2 a3 a4,
    (AMultP a1 a2 <= APlusP a3 a4)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpIdMinusContra : forall a1 a2 i, (AIdP i <= AMinusP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpNumMinusContra : forall a1 a2 n, (ANumP n <= AMinusP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpPlusMinusContra : forall a1 a2 a3 a4,
    (APlusP a1 a2 <= AMinusP a3 a4)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMultMinusContra : forall a1 a2 a3 a4,
    (AMultP a1 a2 <= AMinusP a3 a4)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpIdMultContra : forall a1 a2 i, (AIdP i <= AMultP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpNumMultContra : forall a1 a2 n, (ANumP n <= AMultP a1 a2)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpPlusMultContra : forall a1 a2 a3 a4,
    (APlusP a1 a2 <= AMultP a3 a4)%a -> False.
Proof. boring_contra. Qed.

Lemma aexpMinusMultContra : forall a1 a2 a3 a4,
    (AMinusP a1 a2 <= AMultP a3 a4)%a -> False.
Proof. boring_contra. Qed.

(** ** Bexp contras *)

Lemma bexpFalseTrueContra : (BFalseP <= BTrueP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpEqTrueContra : forall a1 a2, (BEqP a1 a2 <= BTrueP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpLeTrueContra : forall a1 a2, (BLeP a1 a2 <= BTrueP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpNotTrueContra : forall b, (BNotP b <= BTrueP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpAndTrueContra : forall b1 b2, (BAndP b1 b2 <= BTrueP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpTrueFalseContra : (BTrueP <= BFalseP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpEqFalseContra : forall a1 a2, (BEqP a1 a2 <= BFalseP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpLeFalseContra : forall a1 a2, (BLeP a1 a2 <= BFalseP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpNotFalseContra : forall b, (BNotP b <= BFalseP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpAndFalseContra : forall b1 b2, (BAndP b1 b2 <= BFalseP)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpTrueEqContra : forall b1 b2, (BTrueP <= BEqP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpFalseEqContra : forall b1 b2, (BFalseP <= BEqP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpAndEqContra : forall a1 a2 b1 b2, (BAndP a1 a2 <= BEqP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpLeEqContra : forall a1 a2 b1 b2, (BLeP a1 a2 <= BEqP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpNotEqContra : forall b b1 b2, (BNotP b <= BEqP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpTrueLeContra : forall b1 b2, (BTrueP <= BLeP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpFalseLeContra : forall b1 b2, (BFalseP <= BLeP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpAndLeContra : forall a1 a2 b1 b2, (BAndP a1 a2 <= BLeP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpEqLeContra : forall a1 a2 b1 b2, (BEqP a1 a2 <= BLeP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpNotLeContra : forall b b1 b2, (BNotP b <= BLeP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpTrueAndContra : forall b1 b2, (BTrueP <= BAndP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpFalseAndContra : forall b1 b2, (BFalseP <= BAndP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpEqAndContra : forall a1 a2 b1 b2, (BEqP a1 a2 <= BAndP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpLeAndContra : forall a1 a2 b1 b2, (BLeP a1 a2 <= BAndP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpNotAndContra : forall b b1 b2, (BNotP b <= BAndP b1 b2)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpTrueNotContra : forall b, (BTrueP <= BNotP b)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpFalseNotContra : forall b, (BFalseP <= BNotP b)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpEqNotContra : forall a1 a2 b, (BEqP a1 a2 <= BNotP b)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpLeNotContra : forall a1 a2 b, (BLeP a1 a2 <= BNotP b)%b -> False.
Proof. boring_contra. Qed.

Lemma bexpAndNotContra : forall b b1 b2, (BAndP b1 b2 <= BNotP b)%b -> False.
Proof. boring_contra. Qed.

(** ** Com contras *)

Lemma comAssSkipContra : forall x a, (CAssP x a <= CSkipP)%c -> False.
Proof. boring_contra. Qed.

Lemma comSeqSkipContra : forall c1 c2, (CSeqP c1 c2 <= CSkipP)%c -> False.
Proof. boring_contra. Qed.

Lemma comIfSkipContra : forall b c1 c2, (CIfP b c1 c2 <= CSkipP)%c -> False.
Proof. boring_contra. Qed.

Lemma comWhileSkipContra : forall b c, (CWhileP b c <= CSkipP)%c -> False.
Proof. boring_contra. Qed.

Lemma comSkipAssContra : forall x a, (CSkipP <= CAssP x a)%c -> False.
Proof. boring_contra. Qed.

Lemma comSeqAssContra : forall c1 c2 x a, (CSeqP c1 c2 <= CAssP x a)%c -> False.
Proof. boring_contra. Qed.

Lemma comIfAssContra : forall b c1 c2 x a, (CIfP b c1 c2 <= CAssP x a)%c -> False.
Proof. boring_contra. Qed.

Lemma comWhileAssContra : forall b c x a, (CWhileP b c <= CAssP x a)%c -> False.
Proof. boring_contra. Qed.

Lemma comSkipSeqContra : forall c1 c2, (CSkipP <= CSeqP c1 c2)%c -> False.
Proof. boring_contra. Qed.

Lemma comAssSeqContra : forall c1 c2 c3 c4, (CAssP c1 c2 <= CSeqP c3 c4)%c -> False.
Proof. boring_contra. Qed.

Lemma comIfSeqContra : forall b c1 c2 c3 c4, (CIfP b c1 c2 <= CSeqP c3 c4)%c -> False.
Proof. boring_contra. Qed.

Lemma comWhileSeqContra : forall b c c1 c2, (CWhileP b c <= CSeqP c1 c2)%c -> False.
Proof. boring_contra. Qed.

Lemma comSkipIfContra : forall b c1 c2, (CSkipP <= CIfP b c1 c2)%c -> False.
Proof. boring_contra. Qed.

Lemma comAssIfContra : forall c1 c2 b c3 c4, (CAssP c1 c2 <= CIfP b c3 c4)%c -> False.
Proof. boring_contra. Qed.

Lemma comSeqIfContra : forall b c1 c2 c3 c4, (CSeqP c1 c2 <= CIfP b c3 c4)%c -> False.
Proof. boring_contra. Qed.

Lemma comWhileIfContra : forall b1 c1 b2 c2 c3, (CWhileP b1 c1 <= CIfP b2 c2 c3)%c -> False.
Proof. boring_contra. Qed.

Lemma comSkipWhileContra : forall b c, (CSkipP <= CWhileP b c)%c -> False.
Proof. boring_contra. Qed.

Lemma comAssWhileContra : forall c1 c2 b c3, (CAssP c1 c2 <= CWhileP b c3)%c -> False.
Proof. boring_contra. Qed.

Lemma comSeqWhileContra : forall b c1 c2 c3, (CSeqP c1 c2 <= CWhileP b c3)%c -> False.
Proof. boring_contra. Qed.

Lemma comIfWhileContra : forall b1 c1 b2 c2 c3, (CIfP b1 c1 c2 <= CWhileP b2 c3)%c -> False.
Proof. boring_contra. Qed.
