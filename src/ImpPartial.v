(** * Partial Imp expression *)

(** This module defines partial Imp expressions and their ordering relations. *)

Require Import Ids.
Require Import Imp.
Require Import ImpState.
Require Import ImpValues.
Require Import Lattice.
Require Import LSTactics.
Require Import PrefixSets.

(* Arithmetic expressions with variables *)
Inductive aexpP : Type :=
  | AHoleP  :                   aexpP
  | ANumP   : nat   ->          aexpP
  | AIdP    : id    ->          aexpP
  | APlusP  : aexpP -> aexpP -> aexpP
  | AMinusP : aexpP -> aexpP -> aexpP
  | AMultP  : aexpP -> aexpP -> aexpP.

(* Boolean expressions *)
Inductive bexpP : Type :=
  | BHoleP  :                   bexpP
  | BTrueP  :                   bexpP
  | BFalseP :                   bexpP
  | BEqP    : aexpP -> aexpP -> bexpP
  | BLeP    : aexpP -> aexpP -> bexpP
  | BNotP   : bexpP ->          bexpP
  | BAndP   : bexpP -> bexpP -> bexpP.

(* Imperative commands *)
Inductive comP : Type :=
  | CHoleP  :                           comP
  | CSkipP  :                           comP
  | CAssP   : id    -> aexpP ->         comP
  | CSeqP   : comP  -> comP  ->         comP
  | CIfP    : bexpP -> comP  -> comP -> comP
  | CWhileP : bexpP -> comP  ->         comP.

(** ** Turning total expression into partial ones *)

Fixpoint aexpPartialize (a : aexp) : aexpP :=
  match a with
  | ANum n       => ANumP n
  | AId  i       => AIdP i
  | APlus  a1 a2 => APlusP  (aexpPartialize a1) (aexpPartialize a2)
  | AMinus a1 a2 => AMinusP (aexpPartialize a1) (aexpPartialize a2)
  | AMult  a1 a2 => AMultP  (aexpPartialize a1) (aexpPartialize a2)
  end.

Fixpoint bexpPartialize (b : bexp) : bexpP :=
  match b with
  | BTrue      => BTrueP
  | BFalse     => BFalseP
  | BEq a1 a2  => BEqP (aexpPartialize a1) (aexpPartialize a2)
  | BLe a1 a2  => BLeP (aexpPartialize a1) (aexpPartialize a2)
  | BNot b     => BNotP (bexpPartialize b)
  | BAnd b1 b2 => BAndP (bexpPartialize b1) (bexpPartialize b2)
  end.

Fixpoint comPartialize (c : com) : comP :=
  match c with
  | CSkip       => CSkipP
  | CAss x a    => CAssP x (aexpPartialize a)
  | CSeq c1 c2  => CSeqP (comPartialize c1) (comPartialize c2)
  | CIf b c1 c2 => CIfP (bexpPartialize b) (comPartialize c1) (comPartialize c2)
  | CWhile b c  => CWhileP (bexpPartialize b) (comPartialize c)
  end.

Ltac unfold_expression_partialize :=
  (* With aexp, bexp and comp we are need to be more careful though.  We
     simplify only if function is applied directly to a constructor, so that we
     know it will really simplify *)
  repeat match goal with
         | [ H : context[aexpPartialize (ANum _)    ]  |- _] =>
           simpl aexpPartialize in H
         | [ H : context[aexpPartialize (AId _)     ]  |- _] =>
           simpl aexpPartialize in H
         | [ H : context[aexpPartialize (APlus _ _ )]  |- _] =>
           simpl aexpPartialize in H
         | [ H : context[aexpPartialize (AMinus _ _)]  |- _] =>
           simpl aexpPartialize in H
         | [ H : context[aexpPartialize (AMult _ _ )]  |- _] =>
           simpl aexpPartialize in H

         | [ H : context[bexpPartialize BTrue     ]    |- _] =>
           simpl bexpPartialize in H
         | [ H : context[bexpPartialize BFalse    ]    |- _] =>
           simpl bexpPartialize in H
         | [ H : context[bexpPartialize (BEq _ _ )]    |- _] =>
           simpl bexpPartialize in H
         | [ H : context[bexpPartialize (BLe _ _ )]    |- _] =>
           simpl bexpPartialize in H
         | [ H : context[bexpPartialize (BNot _  )]    |- _] =>
           simpl bexpPartialize in H
         | [ H : context[bexpPartialize (BAnd _ _)]    |- _] =>
           simpl bexpPartialize in H

         | [ H : context[comPartialize CSkip        ]  |- _] =>
           simpl comPartialize in H
         | [ H : context[comPartialize (CAss _ _)   ]  |- _] =>
           simpl comPartialize in H
         | [ H : context[comPartialize (CSeq _ _)   ]  |- _] =>
           simpl comPartialize in H
         | [ H : context[comPartialize (CIf _ _ _)  ]  |- _] =>
           simpl comPartialize in H
         | [ H : context[comPartialize (CWhile _ _) ]  |- _] =>
           simpl comPartialize in H

         | [ H : context[statePartialize StateNil  ]  |- _] =>
           simpl statePartialize in H
         | [ H : context[statePartialize (StateCons _ _ _) ] |- _] =>
           simpl statePartialize in H
         end.

(** ** Ordering relations *)

Inductive aexpPO : relation aexpP :=
  | AHolePO : forall a,
      AHoleP <= a
  | ANumPO : forall n n',
      n = n' ->
      ANumP n <= ANumP n'
  | AIdPO : forall id id',
      id = id' ->
      AIdP id <= AIdP id'
  | APlusPO : forall a1 a2 a1' a2',
      a1 <= a1' ->
      a2 <= a2' ->
      (APlusP a1 a2) <= (APlusP a1' a2')
  | AMinusPO : forall a1 a2 a1' a2',
      a1 <= a1' ->
      a2 <= a2' ->
      (AMinusP a1 a2) <= (AMinusP a1' a2')
  | AMultPO : forall a1 a2 a1' a2',
      a1 <= a1' ->
      a2 <= a2' ->
      (AMultP a1 a2) <= (AMultP a1' a2')
  where "a1 '<=' a2" := (aexpPO a1 a2) : aexpPO_scope.

Delimit Scope aexpPO_scope with a.

Inductive bexpPO : relation bexpP :=
  | BHolePO : forall b,
      BHoleP <= b
  | BTruePO :
      BTrueP <= BTrueP
  | BFalsePO :
      BFalseP <= BFalseP
  | BEqPO : forall a1 a2 a1' a2',
      (a1 <= a1')%a ->
      (a2 <= a2')%a ->
      BEqP a1 a2 <= BEqP a1' a2'
  | BLePO : forall a1 a2 a1' a2',
      (a1 <= a1')%a ->
      (a2 <= a2')%a ->
      BLeP a1 a2 <= BLeP a1' a2'
  | BNotPO : forall b1 b2,
      b1 <= b2 ->
      BNotP b1 <= BNotP b2
  | BAndPO : forall b1 b2 b1' b2',
      b1 <= b1' ->
      b2 <= b2' ->
      BAndP b1 b2 <= BAndP b1' b2'
  where "b1 '<=' b2" := (bexpPO b1 b2) : bexpPO_scope.

Delimit Scope bexpPO_scope with b.

Inductive comPO : relation comP :=
  | CHolePO : forall c,
      CHoleP <= c
  | CSkipPO :
      CSkipP <= CSkipP
  | CAssPO : forall id1 id2 a1 a2,
      id1 = id2 ->
      (a1 <= a2)%a ->
      CAssP id1 a1 <= CAssP id2 a2
  | CSeqPO : forall c1 c2 c1' c2',
      c1 <= c1' ->
      c2 <= c2' ->
      CSeqP c1 c2 <= CSeqP c1' c2'
  | CIfPO : forall b b' c1 c2 c1' c2',
      (b <= b')%b  ->
      c1 <= c1' ->
      c2 <= c2' ->
      CIfP b c1 c2 <= CIfP b' c1' c2'
  | CWhilePO : forall b b' c c',
      (b <= b')%b ->
      c <= c' ->
      CWhileP b c <= CWhileP b' c'
  where "c1 '<=' c2" := (comPO c1 c2) : comPO_scope.

Delimit Scope comPO_scope with c.

(* Dismiss cases containing impossible premises about inequalities.  Need to
   explicitly list all the combinations because there are <= proofs that contain
   expressions (not constructors) and inverting those produces more subgoals.
   Also, doing reverse match speeds things up.  *)

Ltac dismiss_impossible_aexp_inequalities :=
  repeat match reverse goal with
         | [ H : (_ <= AHoleP)%a |- _ ] => inversion H; clear H

         | [ H : (AIdP    _   <= ANumP   _  )%a |- _ ] => inversion H
         | [ H : (APlusP  _ _ <= ANumP   _  )%a |- _ ] => inversion H
         | [ H : (AMinusP _ _ <= ANumP   _  )%a |- _ ] => inversion H
         | [ H : (AMultP  _ _ <= ANumP   _  )%a |- _ ] => inversion H

         | [ H : (ANumP   _   <= AIdP    _  )%a |- _ ] => inversion H
         | [ H : (APlusP  _ _ <= AIdP    _  )%a |- _ ] => inversion H
         | [ H : (AMinusP _ _ <= AIdP    _  )%a |- _ ] => inversion H
         | [ H : (AMultP  _ _ <= AIdP    _  )%a |- _ ] => inversion H

         | [ H : (ANumP   _   <= APlusP  _ _)%a |- _ ] => inversion H
         | [ H : (AIdP    _   <= APlusP  _ _)%a |- _ ] => inversion H
         | [ H : (AMinusP _ _ <= APlusP  _ _)%a |- _ ] => inversion H
         | [ H : (AMultP  _ _ <= APlusP  _ _)%a |- _ ] => inversion H

         | [ H : (ANumP   _   <= AMinusP _ _)%a |- _ ] => inversion H
         | [ H : (AIdP    _   <= AMinusP _ _)%a |- _ ] => inversion H
         | [ H : (APlusP  _ _ <= AMinusP _ _)%a |- _ ] => inversion H
         | [ H : (AMultP  _ _ <= AMinusP _ _)%a |- _ ] => inversion H

         | [ H : (ANumP   _   <= AMultP  _ _)%a |- _ ] => inversion H
         | [ H : (AIdP    _   <= AMultP  _ _)%a |- _ ] => inversion H
         | [ H : (APlusP  _ _ <= AMultP  _ _)%a |- _ ] => inversion H
         | [ H : (AMinusP _ _ <= AMultP  _ _)%a |- _ ] => inversion H
         end.

Ltac dismiss_impossible_bexp_inequalities :=
  repeat match reverse goal with
         | [ H : (_ <= BHoleP)%b |- _ ] => inversion H; clear H

         | [ H : (BFalseP   <= BTrueP)%b |- _ ] => inversion H
         | [ H : (BEqP  _ _ <= BTrueP)%b |- _ ] => inversion H
         | [ H : (BLeP  _ _ <= BTrueP)%b |- _ ] => inversion H
         | [ H : (BNotP _   <= BTrueP)%b |- _ ] => inversion H
         | [ H : (BAndP _ _ <= BTrueP)%b |- _ ] => inversion H

         | [ H : (BTrueP    <= BFalseP)%b |- _ ] => inversion H
         | [ H : (BEqP  _ _ <= BFalseP)%b |- _ ] => inversion H
         | [ H : (BLeP  _ _ <= BFalseP)%b |- _ ] => inversion H
         | [ H : (BNotP _   <= BFalseP)%b |- _ ] => inversion H
         | [ H : (BAndP _ _ <= BFalseP)%b |- _ ] => inversion H

         | [ H : (BFalseP   <= BEqP _ _)%b |- _ ] => inversion H
         | [ H : (BTrueP    <= BEqP _ _)%b |- _ ] => inversion H
         | [ H : (BLeP  _ _ <= BEqP _ _)%b |- _ ] => inversion H
         | [ H : (BNotP _   <= BEqP _ _)%b |- _ ] => inversion H
         | [ H : (BAndP _ _ <= BEqP _ _)%b |- _ ] => inversion H

         | [ H : (BFalseP   <= BLeP _ _)%b |- _ ] => inversion H
         | [ H : (BTrueP    <= BLeP _ _)%b |- _ ] => inversion H
         | [ H : (BEqP  _ _ <= BLeP _ _)%b |- _ ] => inversion H
         | [ H : (BNotP _   <= BLeP _ _)%b |- _ ] => inversion H
         | [ H : (BAndP _ _ <= BLeP _ _)%b |- _ ] => inversion H

         | [ H : (BFalseP   <= BNotP _)%b |- _ ] => inversion H
         | [ H : (BTrueP    <= BNotP _)%b |- _ ] => inversion H
         | [ H : (BEqP  _ _ <= BNotP _)%b |- _ ] => inversion H
         | [ H : (BLeP  _ _ <= BNotP _)%b |- _ ] => inversion H
         | [ H : (BAndP _ _ <= BNotP _)%b |- _ ] => inversion H

         | [ H : (BFalseP   <= BAndP _ _)%b |- _ ] => inversion H
         | [ H : (BTrueP    <= BAndP _ _)%b |- _ ] => inversion H
         | [ H : (BEqP _ _  <= BAndP _ _)%b |- _ ] => inversion H
         | [ H : (BLeP  _ _ <= BAndP _ _)%b |- _ ] => inversion H
         | [ H : (BNotP _   <= BAndP _ _)%b |- _ ] => inversion H
         end.

Ltac dismiss_impossible_com_inequalities :=
  repeat match reverse goal with
         | [ H : (_ <= CHoleP)%c |- _ ] => inversion H; clear H

         | [ H : (CAssP _ _   <= CSkipP)%c |- _ ] => inversion H
         | [ H : (CSeqP _ _   <= CSkipP)%c |- _ ] => inversion H
         | [ H : (CIfP  _ _ _ <= CSkipP)%c |- _ ] => inversion H
         | [ H : (CWhileP _ _ <= CSkipP)%c |- _ ] => inversion H

         | [ H : (CSkipP      <= CAssP _ _)%c |- _ ] => inversion H
         | [ H : (CSeqP _ _   <= CAssP _ _)%c |- _ ] => inversion H
         | [ H : (CIfP  _ _ _ <= CAssP _ _)%c |- _ ] => inversion H
         | [ H : (CWhileP _ _ <= CAssP _ _)%c |- _ ] => inversion H

         | [ H : (CSkipP      <= CSeqP _ _)%c |- _ ] => inversion H
         | [ H : (CAssP _ _   <= CSeqP _ _)%c |- _ ] => inversion H
         | [ H : (CIfP  _ _ _ <= CSeqP _ _)%c |- _ ] => inversion H
         | [ H : (CWhileP _ _ <= CSeqP _ _)%c |- _ ] => inversion H

         | [ H : (CSkipP      <= CIfP _ _ _)%c |- _ ] => inversion H
         | [ H : (CAssP   _ _ <= CIfP _ _ _)%c |- _ ] => inversion H
         | [ H : (CSeqP   _ _ <= CIfP _ _ _)%c |- _ ] => inversion H
         | [ H : (CWhileP _ _ <= CIfP _ _ _)%c |- _ ] => inversion H

         | [ H : (CSkipP      <= CWhileP _ _)%c |- _ ] => inversion H
         | [ H : (CAssP _ _   <= CWhileP _ _)%c |- _ ] => inversion H
         | [ H : (CSeqP _ _   <= CWhileP _ _)%c |- _ ] => inversion H
         | [ H : (CIfP  _ _ _ <= CWhileP _ _)%c |- _ ] => inversion H
         end.

(* Invert expression inequalities in the context. *)
Ltac invert_expression_inequalities :=
  unfold_expression_partialize;
  repeat match reverse goal with
         | [ H : (ANumP   _   <= ANumP   _  )%a |- _ ] => inversion H; clear H
         | [ H : (AIdP    _   <= AIdP    _  )%a |- _ ] => inversion H; clear H
         | [ H : (APlusP  _ _ <= APlusP  _ _)%a |- _ ] => inversion H; clear H
         | [ H : (AMinusP _ _ <= AMinusP _ _)%a |- _ ] => inversion H; clear H
         | [ H : (AMultP  _ _ <= AMultP  _ _)%a |- _ ] => inversion H; clear H

         | [ H : (BEqP    _ _ <= BEqP    _ _)%b |- _ ] => inversion H; clear H
         | [ H : (BLeP    _ _ <= BLeP    _ _)%b |- _ ] => inversion H; clear H
         | [ H : (BNotP   _   <= BNotP   _  )%b |- _ ] => inversion H; clear H
         | [ H : (BAndP   _ _ <= BAndP   _ _)%b |- _ ] => inversion H; clear H

         | [ H : (CAssP   _ _ <= CAssP   _ _)%c |- _ ] => inversion H; clear H
         | [ H : (CSeqP   _ _ <= CSeqP   _ _)%c |- _ ] => inversion H; clear H
         | [ H : (CIfP  _ _ _ <= CIfP  _ _ _)%c |- _ ] => inversion H; clear H
         | [ H : (CWhileP _ _ <= CWhileP _ _)%c |- _ ] => inversion H; clear H

         | [ H : (StatePNil <= _)%s             |- _ ] => inversion H; clear H
         | [ H : (_ <= StatePNil)%s             |- _ ] => inversion H; clear H
         | [ H : (StatePCons _ _ _ <= _)%s      |- _ ] => inversion H; clear H
         | [ H : (_ <= StatePCons _ _ _)%s      |- _ ] => inversion H; clear H
         end.

(** ** Proofs *)

(** Proofs that defined relations are indeed ordering relations, ie. that they
    are reflexive, transitive and antisymmetric. *)

Hint Constructors aexpPO bexpPO comPO.

Theorem reflexive_aexpPO : reflexive aexpPO.
Proof. solve_reflexive. Qed.

Theorem transitive_aexpPO : transitive aexpPO.
Proof. solve_transitive. Qed.

Theorem antisymmetric_aexpPO : antisymmetric aexpPO.
Proof. solve_antisymmetric. Qed.

Hint Resolve reflexive_aexpPO transitive_aexpPO antisymmetric_aexpPO.

Hint Extern 1 =>
  match goal with
  | [ H : aexpPO ?a ?b, H' : aexpPO ?b ?a |- _ ] =>
    let P := fresh "P" in
    pose proof (antisymmetric_aexpPO _ _ H H') as P; clear H; clear H'
  end; subst.

Theorem reflexive_bexpPO : reflexive bexpPO.
Proof. solve_reflexive. Qed.

Theorem transitive_bexpPO : transitive bexpPO.
Proof. solve_transitive. Qed.

Theorem antisymmetric_bexpPO : antisymmetric bexpPO.
Proof. solve_antisymmetric. Qed.

Hint Extern 1 =>
  match goal with
  | [ H : bexpPO ?a ?b, H' : bexpPO ?b ?a |- _ ] =>
    let P := fresh "P" in
    pose proof (antisymmetric_bexpPO _ _ H H') as P; clear H; clear H'
  end; subst.

Hint Resolve reflexive_bexpPO transitive_bexpPO antisymmetric_bexpPO.

Theorem reflexive_comPO : reflexive comPO.
Proof. solve_reflexive. Qed.

Theorem transitive_comPO : transitive comPO.
Proof. solve_transitive. Qed.

Theorem antisymmetric_comPO : antisymmetric comPO.
Proof. solve_antisymmetric. Qed.

Hint Resolve reflexive_comPO transitive_comPO antisymmetric_comPO.

Theorem order_aexpPO : order aexpPO.
Proof. eauto. Qed.

Theorem order_bexpPO : order bexpPO.
Proof. eauto. Qed.

Theorem order_comPO : order comPO.
Proof. eauto. Qed.

Hint Resolve order_aexpPO order_bexpPO order_comPO.

(** ** Various lemmas and definitions **)

Lemma nat_anum_lt : forall n n',
    (NatP n <= NatP n')%v -> (ANumP n <= ANumP n')%a.
Proof.
  intros n n' H.
  inversion H.
  eauto.
Qed.

Hint Resolve nat_anum_lt.

Fixpoint aexpLUB {a : aexpP} (a1 a2 : prefix aexpPO a) : prefix aexpPO a.
  refine (
      match a1, a2 with
      | Pfx _ _ a1' a1'R, Pfx _ _ a2' a2'R =>
       match a as L, a1' as a1Ref, a2' as a2Ref
             return (a1Ref <= L)%a -> (a2Ref <= L)%a -> L = a -> _ with
       | AHoleP, AHoleP, AHoleP  => fun R S H => Pfx _ _ AHoleP _

       | ANumP _, ANumP n, AHoleP  => fun R S H => Pfx _ _ (ANumP n) _
       | ANumP _, AHoleP , ANumP n => fun R S H => Pfx _ _ (ANumP n) _
       | ANumP _, ANumP n, ANumP _ => fun R S H => Pfx _ _ (ANumP n) _

       | AIdP_ , AIdP i, AHoleP => fun R S H => Pfx _ _ (AIdP i) _
       | AIdP _, AHoleP, AIdP i => fun R S H => Pfx _ _ (AIdP i) _
       | AIdP _, AIdP i, AIdP _ => fun R S H => Pfx _ _ (AIdP i) _

       | APlusP _ _, APlusP a1 a2, AHoleP =>
         fun R S H => Pfx _ _ (APlusP a1 a2) _
       | APlusP _ _, AHoleP, APlusP a1 a2 =>
         fun R S H => Pfx _ _ (APlusP a1 a2) _
       | APlusP a1t a2t, APlusP a1 a2, APlusP a1' a2' => fun R S H =>
         match aexpLUB a1t (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ =>
           match aexpLUB a2t (Pfx _ _ a2 _) (Pfx _ _ a2' _) with
           | Pfx _ _ a2LUB _ => Pfx _ _ (APlusP a1LUB a2LUB) _
           end
         end

       | AMinusP _ _, AMinusP a1 a2, AHoleP =>
         fun R S H => Pfx _ _ (AMinusP a1 a2) _
       | AMinusP _ _, AHoleP, AMinusP a1 a2 =>
         fun R S H => Pfx _ _ (AMinusP a1 a2) _
       | AMinusP a1t a2t, AMinusP a1 a2, AMinusP a1' a2' => fun R S H =>
         match aexpLUB a1t (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ =>
           match aexpLUB a2t (Pfx _ _ a2 _) (Pfx _ _ a2' _) with
           | Pfx _ _ a2LUB _ => Pfx _ _ (AMinusP a1LUB a2LUB) _
           end
         end

       | AMultP _ _, AMultP a1 a2, AHoleP =>
         fun R S H => Pfx _ _ (AMultP a1 a2) _
       | AMultP _ _, AHoleP, AMultP a1 a2 =>
         fun R S H => Pfx _ _ (AMultP a1 a2) _
       | AMultP a1t a2t, AMultP a1 a2, AMultP a1' a2' => fun R S H =>
         match aexpLUB a1t (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ =>
           match aexpLUB a2t (Pfx _ _ a2 _) (Pfx _ _ a2' _) with
           | Pfx _ _ a2LUB _ => Pfx _ _ (AMultP a1LUB a2LUB) _
           end
         end

       | _, _, _ => fun R S H => _
       end a1'R a2'R eq_refl
      end
    ).
  all: (* 128 *) try solve [ inversion R; inversion S; eassumption ].
  all: (* 98 *) subst; eauto.
Defined.

Fixpoint bexpLUB {b : bexpP} (b1 b2 : prefix bexpPO b) : prefix bexpPO b.
  refine (
      match b1, b2 with
      | Pfx _ _ b1' b1'R, Pfx _ _ b2' b2'R =>
       match b as L, b1' as b1Ref, b2' as b2Ref
             return (b1Ref <= L)%b -> (b2Ref <= L)%b -> L = b -> _ with
       | BHoleP, BHoleP, BHoleP  => fun R S H => Pfx _ _ BHoleP _

       | BTrueP, BHoleP , BTrueP  => fun R S H => Pfx _ _ BTrueP _
       | BTrueP, BTrueP , BHoleP  => fun R S H => Pfx _ _ BTrueP _
       | BTrueP, BTrueP , BTrueP  => fun R S H => Pfx _ _ BTrueP _

       | BFalseP, BFalseP, BFalseP => fun R S H => Pfx _ _ BFalseP _
       | BFalseP, BHoleP , BFalseP => fun R S H => Pfx _ _ BFalseP _
       | BFalseP, BFalseP, BHoleP  => fun R S H => Pfx _ _ BFalseP _

       | BEqP a1t a2t, BEqP a1 a2, BHoleP =>
         fun R S H => Pfx _ _ (BEqP a1 a2) _
       | BEqP a1t a2t, BHoleP, BEqP a1 a2 =>
         fun R S H => Pfx _ _ (BEqP a1 a2) _
       | BEqP a1t a2t, BEqP a1 a2, BEqP a1' a2' => fun R S H =>
         match aexpLUB (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ =>
           match aexpLUB (Pfx _ _ a2 _) (Pfx _ _ a2' _) with
           | Pfx _ _ a2LUB _ => Pfx _ _ (BEqP a1LUB a2LUB) _
           end
         end

       | BLeP a1t a2t, BLeP a1 a2, BHoleP =>
         fun R S H => Pfx _ _ (BLeP a1 a2) _
       | BLeP a1t a2t, BHoleP, BLeP a1 a2 =>
         fun R S H => Pfx _ _ (BLeP a1 a2) _
       | BLeP a1t a2t, BLeP a1 a2, BLeP a1' a2' => fun R S H =>
         match aexpLUB (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ =>
           match aexpLUB (Pfx _ _ a2 _) (Pfx _ _ a2' _) with
           | Pfx _ _ a2LUB _ => Pfx _ _ (BLeP a1LUB a2LUB) _
           end
         end

       | BNotP a1t, BNotP a1, BHoleP =>
         fun R S H => Pfx _ _ (BNotP a1) _
       | BNotP a1t, BHoleP, BNotP a1 =>
         fun R S H => Pfx _ _ (BNotP a1) _
       | BNotP b1t, BNotP b1, BNotP b1' => fun R S H =>
         match bexpLUB b1t (Pfx _ _ b1 _) (Pfx _ _ b1' _) with
         | Pfx _ _ b1LUB _ => Pfx _ _ (BNotP b1LUB) _
         end

       | BAndP a1t a2t, BAndP a1 a2, BHoleP =>
         fun R S H => Pfx _ _ (BAndP a1 a2) _
       | BAndP a1t a2t, BHoleP, BAndP a1 a2 =>
         fun R S H => Pfx _ _ (BAndP a1 a2) _
       | BAndP b1t b2t, BAndP b1 b2, BAndP b1' b2' => fun R S H =>
         match bexpLUB b1t (Pfx _ _ b1 _) (Pfx _ _ b1' _) with
         | Pfx _ _ b1LUB _ =>
           match bexpLUB b2t (Pfx _ _ b2 _) (Pfx _ _ b2' _) with
           | Pfx _ _ b2LUB _ => Pfx _ _ (BAndP b1LUB b2LUB) _
           end
         end

       | _, _, _ => fun R S H => _
       end b1'R b2'R eq_refl
      end
    ).
  all: (* 141 *) try solve [ inversion R; inversion S; eassumption ].
  all: (* 112 *) subst; eauto.
Defined.

Fixpoint comLUB {c : comP} (c1 c2 : prefix comPO c) : prefix comPO c.
  refine (
      match c1, c2 with
      | Pfx _ _ c1' c1'R, Pfx _ _ c2' c2'R =>
       match c as L, c1' as c1Ref, c2' as c2Ref
             return (c1Ref <= c)%c -> (c2Ref <= c)%c -> c = L -> _ with
       | CHoleP, CHoleP, CHoleP => fun R S H => Pfx _ _ CHoleP _

       | CSkipP, CSkipP, CHoleP => fun R S H => Pfx _ _ CSkipP _
       | CSkipP, CHoleP, CSkipP => fun R S H => Pfx _ _ CSkipP _
       | CSkipP, CSkipP, CSkipP => fun R S H => Pfx _ _ CSkipP _

       | CAssP x a, CAssP x' a1, CHoleP =>
         fun R S H => Pfx _ _ (CAssP x' a1) _
       | CAssP x a, CHoleP, CAssP x' a1 =>
         fun R S H => Pfx _ _ (CAssP x' a1) _
       | CAssP x a, CAssP _ a1, CAssP _ a1' => fun R S H =>
         match aexpLUB (Pfx _ _ a1 _) (Pfx _ _ a1' _) with
         | Pfx _ _ a1LUB _ => Pfx _ _ (CAssP x a1LUB) _
         end

       | CSeqP c1t c2t, CSeqP c1 c2, CHoleP =>
         fun R S H => Pfx _ _ (CSeqP c1 c2) _
       | CSeqP c1t c2t, CHoleP, CSeqP c1 c2 =>
         fun R S H => Pfx _ _ (CSeqP c1 c2) _
       | CSeqP c1t c2t, CSeqP c1 c2, CSeqP c1' c2' => fun R S H =>
         match comLUB c1t (Pfx _ _ c1 _) (Pfx _ _ c1' _) with
         | Pfx _ _ c1LUB _ =>
           match comLUB c2t (Pfx _ _ c2 _) (Pfx _ _ c2' _) with
           | Pfx _ _ c2LUB _ => Pfx _ _ (CSeqP c1LUB c2LUB) _
           end
         end

       | CIfP bt c1t c2t, CIfP b c1 c2, CHoleP =>
         fun R S H => Pfx _ _ (CIfP b c1 c2) _
       | CIfP bt c1t c2t, CHoleP, CIfP b c1 c2 =>
         fun R S H => Pfx _ _ (CIfP b c1 c2) _
       | CIfP bt c1t c2t, CIfP b c1 c2, CIfP b' c1' c2' => fun R S H =>
         match bexpLUB (Pfx _ _ b _) (Pfx _ _ b' _) with
         | Pfx _ _ bLUB _ =>
           match comLUB c1t (Pfx _ _ c1 _) (Pfx _ _ c1' _) with
           | Pfx _ _ c1LUB _ =>
             match comLUB c2t (Pfx _ _ c2 _) (Pfx _ _ c2' _) with
             | Pfx _ _ c2LUB _ => Pfx _ _ (CIfP bLUB c1LUB c2LUB) _
             end
           end
         end

       | CWhileP bt c1t, CWhileP b c1, CHoleP =>
         fun R S H => Pfx _ _ (CWhileP b c1) _
       | CWhileP bt c1t, CHoleP, CWhileP b c1 =>
         fun R S H => Pfx _ _ (CWhileP b c1) _
       | CWhileP bt c1t, CWhileP b c1, CWhileP b' c1' => fun R S H =>
         match bexpLUB (Pfx _ _ b _) (Pfx _ _ b' _) with
         | Pfx _ _ bLUB _ =>
           match comLUB c1t (Pfx _ _ c1 _) (Pfx _ _ c1' _) with
           | Pfx _ _ c1LUB _ => Pfx _ _ (CWhileP bLUB c1LUB) _
           end
         end

       | _, _, _ => fun R S H => _
       end c1'R c2'R eq_refl
      end
    ).
  all: (* 107 *) try solve [ subst; inversion R; inversion S; eassumption ].
  all: (* 79 *) subst; eauto.
Defined.

Ltac solve_aexpLUB_prefixO :=
  intros a a1 a2 aLUB H;
  (* Induction on the expression structure (determines lattice top element) *)
  induction a;
  (* Destruct all expression prefixes to uncover their internal structure *)
  destruct a1, a2, aLUB;
  (* simplify calls to aexpLUB to expose matches; case-analyse them *)
  simpl in *; destruct_matches;
  generalize_nested_pfx_proofs;
  (* Most of the cases are contradiciting - dismiss them *)
  dismiss_impossible_aexp_inequalities;
  (* Invert inequalities to get more facts *)
  invert_expression_inequalities;
  invert_pfx_equalities;
  (* Trigger the inductive hypotheses *)
  repeat match goal with
         | [ IH : forall _ _ _, _ -> _, H : aexpLUB _ _ = _ |- _ ] =>
           specialize (IH _ _ _ H)
         end;
  (* Extract all the facts that we've learned *)
  invert_prefixO;
  (* and solve! *)
  auto.

Lemma aexpLUB_prefixO_fst : forall a a1 a2 aLUB,
    aexpLUB a1 a2 = aLUB -> prefixO aexpPO a a1 aLUB.
Proof. solve_aexpLUB_prefixO. Qed.

Hint Resolve aexpLUB_prefixO_fst.

Lemma aexpLUB_prefixO_snd : forall a a1 a2 aLUB,
    aexpLUB a1 a2 = aLUB -> prefixO aexpPO a a2 aLUB.
Proof. solve_aexpLUB_prefixO. Qed.

Hint Resolve aexpLUB_prefixO_snd.

Ltac pose_aexpLUB_prefixO :=
  match goal with
  | [ H1 : aexpLUB _ _ = _, H2 : aexpLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply aexpLUB_prefixO_fst in H1 as P;
    let P2 := fresh "H" in
    apply aexpLUB_prefixO_snd in H1 as P2;
    let P := fresh "H" in
    apply aexpLUB_prefixO_fst in H2 as P3;
    let P2 := fresh "H" in
    apply aexpLUB_prefixO_snd in H2 as P4
  | [ H : aexpLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply aexpLUB_prefixO_fst in H as P;
    let P2 := fresh "H" in
    apply aexpLUB_prefixO_snd in H as P2
  end.

Ltac solve_bexpLUB_prefixO :=
  intros b b1 b2 bLUB H;
  (* Induction on the expression structure (determines lattice top element) *)
  induction b;
  (* Destruct all expression prefixes to uncover their internal structure *)
  destruct b1, b2, bLUB;
  (* simplify calls to bexpLUB to expose matches; case-analyse them *)
  simpl in *; destruct_matches;
  generalize_nested_pfx_proofs;
  (* Most of the cases are contradiciting - dismiss them *)
  dismiss_impossible_bexp_inequalities;
  (* Invert inequalities to get more facts *)
  invert_expression_inequalities;
  invert_pfx_equalities;
  (* Use properties of aexpLUB *)
  try pose_aexpLUB_prefixO;
  (* Trigger the inductive hypotheses *)
  repeat match goal with
         | [ IH : forall _ _ _, _ -> _, H : bexpLUB _ _ = _ |- _ ] =>
           specialize (IH _ _ _ H)
         end;
  (* Extract all the facts that we've learned *)
  invert_prefixO;
  (* and solve! *)
  auto.

Lemma bexpLUB_prefixO_fst : forall b b1 b2 bLUB,
    bexpLUB b1 b2 = bLUB -> prefixO bexpPO b b1 bLUB.
Proof. solve_bexpLUB_prefixO. Qed.

Hint Resolve bexpLUB_prefixO_fst.

Lemma bexpLUB_prefixO_snd : forall b b1 b2 bLUB,
    bexpLUB b1 b2 = bLUB -> prefixO bexpPO b b2 bLUB.
Proof. solve_bexpLUB_prefixO. Qed.

Hint Resolve bexpLUB_prefixO_snd.

Ltac pose_bexpLUB_prefixO :=
  match goal with
  | [ H1 : bexpLUB _ _ = _, H2 : bexpLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply bexpLUB_prefixO_fst in H1 as P;
    let P2 := fresh "H" in
    apply bexpLUB_prefixO_snd in H1 as P2;
    let P := fresh "H" in
    apply bexpLUB_prefixO_fst in H2 as P3;
    let P2 := fresh "H" in
    apply bexpLUB_prefixO_snd in H2 as P4
  | [ H : bexpLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply bexpLUB_prefixO_fst in H as P;
    let P2 := fresh "H" in
    apply bexpLUB_prefixO_snd in H as P2
  end.

Ltac solve_comLUB_prefixO :=
  intros c c1 c2 cLUB H;
  (* Induction on the command structure (determines lattice top element) *)
  induction c;
  (* Destruct all command prefixes to uncover their internal structure *)
  destruct c1, c2, cLUB;
  (* simplify calls to comLUB to expose matches; case-analyse them *)
  simpl in *; destruct_matches;
  generalize_nested_pfx_proofs;
  (* Most of the cases are contradiciting - dismiss them *)
  dismiss_impossible_com_inequalities;
  (* Invert inequalities to get more facts *)
  invert_expression_inequalities;
  invert_pfx_equalities;
  (* Use properties of aexpLUB and bexpLUB *)
  try pose_aexpLUB_prefixO;
  try pose_bexpLUB_prefixO;
  (* Trigger the inductive hypotheses *)
  repeat match goal with
         | [ IH : forall _ _ _, _ -> _, H : comLUB _ _ = _ |- _ ] =>
           specialize (IH _ _ _ H)
         end;
  (* Extract all the facts that we've learned *)
  invert_prefixO;
  (* and solve! *)
  auto.

Lemma comLUB_prefixO_fst : forall c c1 c2 cLUB,
    comLUB c1 c2 = cLUB -> prefixO comPO c c1 cLUB.
Proof. solve_comLUB_prefixO. Qed.

Hint Resolve comLUB_prefixO_fst.

Lemma comLUB_prefixO_snd : forall c c1 c2 cLUB,
    comLUB c1 c2 = cLUB -> prefixO comPO c c2 cLUB.
Proof. solve_comLUB_prefixO. Qed.

Hint Resolve comLUB_prefixO_snd.

Ltac pose_comLUB_prefixO :=
  match goal with
  | [ H1 : comLUB _ _ = _, H2 : comLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply comLUB_prefixO_fst in H1 as P;
    let P2 := fresh "H" in
    apply comLUB_prefixO_snd in H1 as P2;
    let P := fresh "H" in
    apply comLUB_prefixO_fst in H2 as P3;
    let P2 := fresh "H" in
    apply comLUB_prefixO_snd in H2 as P4
  | [ H : comLUB _ _ = _ |- _ ] =>
    let P := fresh "H" in
    apply comLUB_prefixO_fst in H as P;
    let P2 := fresh "H" in
    apply comLUB_prefixO_snd in H as P2
  end.

Lemma aexpLUB_prefixO : forall a a1 a1' a2 a2' aLUB1 aLUB2,
    aexpLUB a1  a2  = aLUB1 ->
    aexpLUB a1' a2' = aLUB2 ->
    prefixO aexpPO a a1 a1' ->
    prefixO aexpPO a a2 a2' -> prefixO aexpPO a aLUB1 aLUB2.
Proof.
  intros a a1 a1' a2 a2' aLUB1 aLUB2 H H0 H1 H2.
  induction a;
  destruct a1, a2, a1', a2', aLUB1, aLUB2;
  simpl in *; destruct_matches;
  (* We have 2276 goals at this point.  Almost all are contradiciting *)
  dismiss_impossible_aexp_inequalities;
  (* 81 goals *)
  generalize_nested_pfx_proofs;
  (* Invert inequalities to get more facts that we need to trigger IH *)
  invert_pfx_equalities;
  invert_prefixO;
  (* By this point we've uncovered some more contradictions - dismiss *)
  dismiss_impossible_aexp_inequalities;
  (* 46 goals *)
  invert_expression_inequalities;
  (* pose facts to trigger IH *)
  try pose_aexpLUB_prefixO;
  try inversion H; try inversion H0; subst;
  (* Smart specialization of inductive hypotheses *)
  repeat match goal with
           | [ IH : forall _ _ _ _ _ _, _ -> _ -> _ -> _ -> _
             , H1 : aexpLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
             , H2 : aexpLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
             , H3 : (?X1 <= ?X2)%a, H4 : (?Y1 <= ?Y2)%a |- _ ] =>
             specialize (IH _ _ _ _ _ _ H1 H2 (PfxO _ _ _ _ _ _ H3)
                            (PfxO _ _ _ _ _ _ H4))
         end;
  invert_prefixO;
  eauto.
Qed.

Hint Resolve aexpLUB_prefixO.

Lemma bexpLUB_prefixO : forall b b1 b1' b2 b2' bLUB1 bLUB2,
    bexpLUB b1  b2  = bLUB1 ->
    bexpLUB b1' b2' = bLUB2 ->
    prefixO bexpPO b b1 b1' ->
    prefixO bexpPO b b2 b2' -> prefixO bexpPO b bLUB1 bLUB2.
Proof.
  intros b b1 b1' b2 b2' bLUB1 bLUB2 H H0 H1 H2.
  induction b;
  destruct b1, b2, b1', b2', bLUB1, bLUB2;
  simpl in *; destruct_matches;
  (* We have 7776 goals at this point.  Almost all are contradiciting *)
  dismiss_impossible_bexp_inequalities;
  (* 81 goals *)
  generalize_nested_pfx_proofs;
  (* Invert inequalities to get more facts that we need to trigger IH *)
  invert_pfx_equalities;
  invert_prefixO;
  (* By this point we've uncovered some more contradictions - dismiss *)
  dismiss_impossible_bexp_inequalities;
  invert_expression_inequalities;
  (* pose facts to trigger IH *)
  try pose_aexpLUB_prefixO;
  try pose_bexpLUB_prefixO;
  try inversion H; try inversion H0; subst;
  repeat match goal with
         | [ H1 : aexpLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
           , H2 : aexpLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
           , H3 : (?X1 <= ?X2)%a, H4 : (?Y1 <= ?Y2)%a |- _ ] =>
           let H := fresh "H" in
           pose proof (aexpLUB_prefixO _ _ _ _ _ _ _ H1 H2
                                       (PfxO _ _ _ _ _ _ H3)
                                       (PfxO _ _ _ _ _ _ H4)) as H;
           clear H1; clear H2
         end;
  repeat match goal with
         | [ IH : forall _ _ _ _ _ _, _ -> _ -> _ -> _ -> _
           , H1 : bexpLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
           , H2 : bexpLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
           , H3 : (?X1 <= ?X2)%b, H4 : (?Y1 <= ?Y2)%b |- _ ] =>
           specialize (IH _ _ _ _ _ _ H1 H2 (PfxO _ _ _ _ _ _ H3)
                          (PfxO _ _ _ _ _ _ H4))
         end;
  invert_prefixO; eauto.
Qed.

Hint Resolve bexpLUB_prefixO.

Lemma comLUB_prefixO : forall c c1 c2 c1' c2' cLUB1 cLUB2,
    comLUB c1  c2  = cLUB1 ->
    comLUB c1' c2' = cLUB2 ->
    prefixO comPO c c1 c1' ->
    prefixO comPO c c2 c2' -> prefixO comPO c cLUB1 cLUB2.
Proof.
  intros c c1 c2 c1' c2' cLUB1 cLUB2 H H0 H1 H2.
  induction c;
    destruct c1, c2, c1', c2', cLUB1, cLUB2;
    simpl in *; destruct_matches;
    (* 7776 goals *)
    (* Most of the cases are contradiciting - dismiss them *)
    subst; dismiss_impossible_com_inequalities;
    (* 81 goals *)
    generalize_nested_pfx_proofs;
    (* Invert inequalities to get more facts *)
    invert_pfx_equalities;
    invert_prefixO;
    (* By this point we've uncovered some more contradictions - dismiss *)
    dismiss_impossible_com_inequalities;
    invert_expression_inequalities;
    (* pose facts to trigger IH *)
    try pose_aexpLUB_prefixO;
    try pose_bexpLUB_prefixO;
    try pose_comLUB_prefixO;
    try inversion H; try inversion H0; subst;
    repeat match goal with
           | [ H1 : aexpLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
             , H2 : aexpLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
             , H3 : (?X1 <= ?X2)%a, H4 : (?Y1 <= ?Y2)%a |- _ ] =>
             let H := fresh "H" in
             pose proof (aexpLUB_prefixO _ _ _ _ _ _ _ H1 H2
                                         (PfxO _ _ _ _ _ _ H3)
                                         (PfxO _ _ _ _ _ _ H4)) as H;
             clear H1; clear H2
           end;
    repeat match goal with
           | [ H1 : bexpLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
             , H2 : bexpLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
             , H3 : (?X1 <= ?X2)%b, H4 : (?Y1 <= ?Y2)%b |- _ ] =>
             let H := fresh "H" in
             pose proof (bexpLUB_prefixO _ _ _ _ _ _ _ H1 H2
                                         (PfxO _ _ _ _ _ _ H3)
                                         (PfxO _ _ _ _ _ _ H4)) as H;
             clear H1; clear H2
           end;
    repeat match goal with
           | [ IH : forall _ _ _ _ _ _, _ -> _ -> _ -> _ -> _
             , H1 : comLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
             , H2 : comLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
             , H3 : (?X1 <= ?X2)%c, H4 : (?Y1 <= ?Y2)%c |- _ ] =>
             specialize (IH _ _ _ _ _ _ H1 H2 (PfxO _ _ _ _ _ _ H3)
                            (PfxO _ _ _ _ _ _ H4))
           end;
    invert_prefixO; eauto.
Qed.

Hint Resolve comLUB_prefixO.

Lemma comLUB_comm_WhileP :
  forall b c b1 b2 c1 c2 P0 P1,
   exists bLUB, exists cLUB, exists P,
   comLUB (Pfx comPO (CWhileP b c) (CWhileP b1 c1) P0)
          (Pfx comPO (CWhileP b c) (CWhileP b2 c2) P1) =
   Pfx comPO (CWhileP b c) (CWhileP bLUB cLUB) P.
Proof.
  intros b c b1 b2 c1 c2 P0 P1.
  simpl.
  destruct_matches.
  eauto.
Qed.

Hint Resolve comLUB_comm_WhileP.

Lemma comLUB_WhileP_leq :
  forall b c b1 b2 c1 c2 bLUB cLUB P0 P1 P,
   comLUB (Pfx comPO (CWhileP b c) (CWhileP b1 c1) P0)
          (Pfx comPO (CWhileP b c) (CWhileP b2 c2) P1) =
   Pfx comPO (CWhileP b c) (CWhileP bLUB cLUB) P ->
   (b1 <= bLUB)%b /\ (b2 <= bLUB)%b /\ (c1 <= cLUB)%c /\ (c2 <= cLUB)%c.
Proof.
  intros b c b1 b2 c1 c2 bLUB cLUB P0 P1 P H.
  simpl in H.
  destruct_matches.
  generalize_nested_pfx_proofs.
  invert_pfx_equalities.
  pose_bexpLUB_prefixO.
  pose_comLUB_prefixO.
  invert_prefixO.
  auto.
Qed.

Hint Resolve comLUB_WhileP_leq.
