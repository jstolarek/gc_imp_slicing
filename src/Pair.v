Require Import Lattice.

(* Turns a pair of relations into relation on pair *)
Inductive pairR {X Y : Type} (R : relation X * relation Y) : relation (X * Y) :=
| PairR : forall a b c d, (fst R) a c -> (snd R) b d -> pairR R (a, b) (c, d).

Hint Constructors pairR.

Hint Extern 1 =>
  match goal with
  [ |- pairR _ _ _ ] => solve [ apply PairR; simpl; eauto ]
  end.

(* Invert a pair, simplify calls to fst and snd and try further inverting
   components of a pair *)
Ltac invert_pairR :=
  repeat match goal with
         | [ IH : pairR _ _ _ |- _ ] =>
           let a := fresh "H" in
           let b := fresh "H" in
           let c := fresh "H" in
           let d := fresh "H" in
           let e := fresh "H" in
           let f := fresh "H" in
           let g := fresh "H" in
           inversion IH as [a b c d e f g]; subst; clear IH;
           simpl in e; try inversion e;
           simpl in f; try inversion f
         end.

Theorem reflexive_pairPO :
  forall {X Y : Type} (P : relation X) (Q : relation Y),
    reflexive P -> reflexive Q -> reflexive (pairR (P, Q)).
Proof.
  intros X Y P Q H H0 [x y]. auto.
Qed.

Theorem transitive_pairPO :
  forall {X Y : Type} (P : relation X) (Q : relation Y),
    transitive P -> transitive Q -> transitive (pairR (P, Q)).
Proof.
  intros X Y P Q PT QT [x1 y1] [x2 y2] [x3 y3] H1 H2.
  invert_pairR.
  auto.
Qed.

Theorem antisymmetric_pairPO :
  forall {X Y : Type} (P : relation X) (Q : relation Y),
    antisymmetric P -> antisymmetric Q -> antisymmetric (pairR (P, Q)).
Proof.
  intros X Y P Q PA QA [x1 y1] [x2 y2] H1 H2.
  invert_pairR.
  f_equal; eauto.
Qed.

Hint Resolve reflexive_pairPO transitive_pairPO antisymmetric_pairPO.

Theorem order_pair : forall {X Y : Type} (P : relation X) (Q : relation Y),
    order P -> order Q -> order (pairR (P, Q)).
Proof.
  intros X Y P Q [PR [PT PA]] [QR [QT QA]].
  auto.
Qed.
