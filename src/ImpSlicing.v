(** * Slicing arithmetic expressions *)

Require Import Ids.
Require Import Imp.
Require Import ImpPartial.
Require Import ImpPartialMore.
Require Import ImpSlicing.ContraLemmas.
Require Import ImpState.
Require Import ImpValues.
Require Import Lattice.
Require Import LSTactics.
Require Import Pair.
Require Import PrefixSets.

Require Import Coq.Arith.Arith.
Require Import Coq.Arith.EqNat.
Require Import Coq.Logic.ProofIrrelevance.

(** ** Slicing definitions *)

Fixpoint aexpFwd {st : state} {a : aexp} {v : nat}
           {t : aexpT} (ev : (t :: a, st \\ v)%ar):
     (prefix aexpPO  (aexpPartialize a) *
      prefix statePO (statePartialize st)) ->
      prefix natPO   (natPartialize v).
  refine (
    match ev with
    | E_Num st n => fun aP' =>
      match aP' with | (Pfx _ _ aP R, _)  =>
      match aP return (aP <= aexpPartialize (ANum n))%a -> prefix _ _ with
      | AHoleP      => fun _      => Pfx _ _ _ (NHolePO _)
      | ANumP   _   => fun _      => Pfx _ _   (NatP n) (NatPO _ _ eq_refl)
      | _  => fun _ => _
      end R
      end
    | E_Id n id st H =>
      fun aP => match aP with | (Pfx _ _ aP R, Pfx _ _ st' R') =>
      match aP return (aP <= AIdP id)%a -> prefix _ _ with
      | AHoleP      => fun _      => Pfx _ _ _ (NHolePO _)
      | AIdP    i   => fun _      =>
        Pfx _ _ (lookupP st' id)
            (* constructed using refine tactic *)
            (transitive_natPO (lookupP st' id)
                                (lookupP (statePartialize st) id) (NatP n)
                              (lookupP_monotone st' (statePartialize st) id R')
                              (eq_ind_r (fun v' => (v' <= NatP n)%v)
                                        (reflexive_natPO (NatP n))
                                        (lookup_Some_NatP st id n H)))
      | _  => fun _ => _
      end R
      end
    | E_Plus st a1 a2 n1 n2 t1 t2 H1 H2 =>
      fun aP => match aP with | (Pfx _ _ aP R, Pfx _ _ stP T) =>
      match aP return (aP <= aexpPartialize (APlus a1 a2))%a -> prefix _ _ with
      | AHoleP      => fun _      => Pfx _ _ _ (NHolePO _)
      | APlusP a1p a2p => fun R =>
        let a1Pfx := Pfx aexpPO (aexpPartialize a1) a1p _ in (* refine *)
        let a2Pfx := Pfx aexpPO (aexpPartialize a2) a2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match aexpFwd st a1 n1 t1 H1 (a1Pfx, stPfx),
              aexpFwd st a2 n2 t2 H2 (a2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | NHoleP  , _        => Pfx _ _ NHoleP (NHolePO _)
        | _       , NHoleP   => Pfx _ _ NHoleP (NHolePO _)
        | NatP v1, NatP v2 => Pfx _ _ (NatP (n1 + n2)) (NatPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    | E_Minus st a1 a2 n1 n2 t1 t2 H1 H2 =>
      fun aP => match aP with | (Pfx _ _ aP R, Pfx _ _ stP T) =>
      match aP return (aP <= aexpPartialize (AMinus a1 a2))%a -> prefix _ _ with
      | AHoleP      => fun _      => Pfx _ _ _ (NHolePO _)
      | AMinusP a1p a2p => fun R =>
        let a1Pfx := Pfx aexpPO (aexpPartialize a1) a1p _ in (* refine *)
        let a2Pfx := Pfx aexpPO (aexpPartialize a2) a2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match aexpFwd st a1 n1 t1 H1 (a1Pfx, stPfx),
              aexpFwd st a2 n2 t2 H2 (a2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | NHoleP  , _        => Pfx _ _ NHoleP (NHolePO _)
        | _       , NHoleP   => Pfx _ _ NHoleP (NHolePO _)
        | NatP v1, NatP v2 => Pfx _ _ (NatP (n1 - n2)) (NatPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    | E_Mult st a1 a2 n1 n2 t1 t2 H1 H2 =>
      fun aP => match aP with | (Pfx _ _ aP R, Pfx _ _ stP T) =>
      match aP return (aP <= aexpPartialize (AMult a1 a2))%a -> prefix _ _ with
      | AHoleP      => fun _      => Pfx _ _ _ (NHolePO _)
      | AMultP a1p a2p => fun R =>
        let a1Pfx := Pfx aexpPO (aexpPartialize a1) a1p _ in (* refine *)
        let a2Pfx := Pfx aexpPO (aexpPartialize a2) a2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match aexpFwd st a1 n1 t1 H1 (a1Pfx, stPfx),
              aexpFwd st a2 n2 t2 H2 (a2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | NHoleP  , _        => Pfx _ _ NHoleP (NHolePO _)
        | _       , NHoleP   => Pfx _ _ NHoleP (NHolePO _)
        | NatP v1, NatP v2 => Pfx _ _ (NatP (n1 * n2)) (NatPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    end ).
  Unshelve.
  all: try inversion R; eauto.
Defined.

Fixpoint aexpBwd {st : state} {a : aexp} {v : nat}
           {t : aexpT} (ev : (t :: a, st \\ v)%ar):
     (prefix natPO (natPartialize v)) ->
     (prefix aexpPO  (aexpPartialize  a) *
      prefix statePO (statePartialize st)).
  refine (
    match ev with
    | E_Num st n => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= natPartialize n)%v -> _ with
      | NHoleP => fun R' =>
       ( Pfx _ _ AHoleP (AHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | NatP n' => fun _ =>
       ( Pfx _ _ (ANumP n') _
       , Pfx _ _ (empty_stateP (keys st)) _)
      end R
      end
    | E_Id n id st H => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= natPartialize n)%v -> _ with
      | NHoleP => fun _ =>
       ( Pfx _ _ AHoleP (AHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | NatP n' => fun _ =>
       ( Pfx _ _ (AIdP id) _
       , Pfx _ _ (updateP (empty_stateP (keys st)) id v) _)
      end R
      end
    | E_Plus st a1 a2 n1 n2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= natPartialize (n1 + n2))%v -> _ with
      | NHoleP => fun _ =>
       ( Pfx _ _ AHoleP (AHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | NatP n' => fun R' =>
        let n1Pfx := Pfx _ (natPartialize n1) (natPartialize n1) _ in
        let n2Pfx := Pfx _ (natPartialize n2) (natPartialize n2) _ in
        match aexpBwd st a1 n1 t1 H1 n1Pfx,
              aexpBwd st a2 n2 t2 H2 n2Pfx with
        | (Pfx _ _ b1 _, st1), (Pfx _ _ b2 _, st2) =>
          (Pfx _ _ (APlusP b1 b2) _, stateLUB st1 st2)
        end
      end R
      end
    | E_Minus st a1 a2 n1 n2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= natPartialize (n1 - n2))%v -> _ with
      | NHoleP => fun _ =>
       ( Pfx _ _ AHoleP (AHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | NatP n' => fun R' =>
        let n1Pfx := Pfx _ (natPartialize n1) (natPartialize n1) _ in
        let n2Pfx := Pfx _ (natPartialize n2) (natPartialize n2) _ in
        match aexpBwd st a1 n1 t1 H1 n1Pfx,
              aexpBwd st a2 n2 t2 H2 n2Pfx with
        | (Pfx _ _ b1 _, st1), (Pfx _ _ b2 _, st2) =>
          (Pfx _ _ (AMinusP b1 b2) _, stateLUB st1 st2)
        end
      end R
      end
    | E_Mult st a1 a2 n1 n2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= natPartialize (n1 * n2))%v -> _ with
      | NHoleP => fun _ =>
       ( Pfx _ _ AHoleP (AHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | NatP n' => fun R' =>
        let n1Pfx := Pfx _ (natPartialize n1) (natPartialize n1) _ in
        let n2Pfx := Pfx _ (natPartialize n2) (natPartialize n2) _ in
        match aexpBwd st a1 n1 t1 H1 n1Pfx,
              aexpBwd st a2 n2 t2 H2 n2Pfx with
        | (Pfx _ _ b1 _, st1), (Pfx _ _ b2 _, st2) =>
          (Pfx _ _ (AMultP b1 b2) _, stateLUB st1 st2)
        end
      end R
      end
    end ).
  (* E_Num *)
  - simpl in *. auto.
  - rewrite state_partialize_keys. auto.
  (* E_Id *)
  - auto.
  - eapply updateP_with_smaller; eauto.
    * pose proof (lookup_Some_NatP _ _ _ H) as H'. rewrite H'. trivial.
  (* E_Plus *)
  - eauto.
  (* E_Minus *)
  - eauto.
  (* E_Mult *)
  - eauto.
  Unshelve.
  all: auto.
Defined.

Fixpoint bexpFwd {st : state} {b : bexp} {v : bool}
           {t : bexpT} (ev : (t :: b, st \\ v)%br):
     (prefix bexpPO  (bexpPartialize b) *
      prefix statePO (statePartialize st)) ->
      prefix boolPO  (boolPartialize v).
  refine (
    match ev with
    | E_BTrue st => fun bP' =>
      match bP' with | (Pfx _ _ bP R, _)  =>
      match bP return (bP <= bexpPartialize BTrue)%b -> prefix _ _ with
      | BHoleP     => fun _ => Pfx _ _ _ (BVHolePO _)
      | BTrueP     => fun _ => Pfx _ _ (BoolP true) (BoolPO _ _ eq_refl)
      | _  => fun _ => _
      end R
      end
    | E_BFalse st => fun bP' =>
      match bP' with | (Pfx _ _ bP R, _)  =>
      match bP return (bP <= bexpPartialize BFalse)%b -> prefix _ _ with
      | BHoleP     => fun _ => Pfx _ _ _ (BVHolePO _)
      | BFalseP    => fun _ => Pfx _ _ (BoolP false) (BoolPO _ _ eq_refl)
      | _  => fun _ => _
      end R
      end
    | E_BEq n1 n2 st a1 a2 t1 t2 H1 H2 =>
      fun bP => match bP with | (Pfx _ _ bP R, Pfx _ _ stP T) =>
      match bP return (bP <= bexpPartialize (BEq a1 a2))%b -> prefix _ _ with
      | BHoleP      => fun _      => Pfx _ _ _ (BVHolePO _)
      | BEqP a1p a2p => fun R =>
        let a1Pfx := Pfx aexpPO (aexpPartialize a1) a1p _ in (* refine *)
        let a2Pfx := Pfx aexpPO (aexpPartialize a2) a2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match @aexpFwd st a1 n1 t1 H1 (a1Pfx, stPfx),
              @aexpFwd st a2 n2 t2 H2 (a2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | NHoleP , _       => Pfx _ _ BVHoleP (BVHolePO _)
        | _      , NHoleP  => Pfx _ _ BVHoleP (BVHolePO _)
        | NatP v1, NatP v2 => Pfx _ _ (BoolP (beq_nat n1 n2)) (BoolPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    | E_BLe n1 n2 st a1 a2 t1 t2 H1 H2 =>
      fun bP => match bP with | (Pfx _ _ bP R, Pfx _ _ stP T) =>
      match bP return (bP <= bexpPartialize (BLe a1 a2))%b -> prefix _ _ with
      | BHoleP      => fun _      => Pfx _ _ _ (BVHolePO _)
      | BLeP a1p a2p => fun R =>
        let a1Pfx := Pfx aexpPO (aexpPartialize a1) a1p _ in (* refine *)
        let a2Pfx := Pfx aexpPO (aexpPartialize a2) a2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match @aexpFwd st a1 n1 t1 H1 (a1Pfx, stPfx),
              @aexpFwd st a2 n2 t2 H2 (a2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | NHoleP , _       => Pfx _ _ BVHoleP (BVHolePO _)
        | _      , NHoleP  => Pfx _ _ BVHoleP (BVHolePO _)
        | NatP v1, NatP v2 => Pfx _ _ (BoolP (leb n1 n2)) (BoolPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    | E_BNot b st e t H =>
      fun bP => match bP with | (Pfx _ _ bP R, Pfx _ _ stP T) =>
      match bP return (bP <= bexpPartialize (BNot e))%b -> prefix _ _ with
      | BHoleP      => fun _      => Pfx _ _ _ (BVHolePO _)
      | BNotP bp => fun R =>
        let bPfx  := Pfx bexpPO (bexpPartialize e) bp _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match bexpFwd st e b t H (bPfx, stPfx) with
        | Pfx _ _ v1p _ =>
        match v1p with
        | BVHoleP  => Pfx _ _ BVHoleP (BVHolePO _)
        | BoolP v1 => Pfx _ _ (BoolP (negb b)) (BoolPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    | E_BAnd b1 b2 st e1 e2 t1 t2 H1 H2 =>
      fun bP => match bP with | (Pfx _ _ bP R, Pfx _ _ stP T) =>
      match bP return (bP <= bexpPartialize (BAnd e1 e2))%b -> prefix _ _ with
      | BHoleP      => fun _      => Pfx _ _ _ (BVHolePO _)
      | BAndP b1p b2p => fun R =>
        let b1Pfx := Pfx bexpPO (bexpPartialize e1) b1p _ in (* refine *)
        let b2Pfx := Pfx bexpPO (bexpPartialize e2) b2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        match bexpFwd st e1 b1 t1 H1 (b1Pfx, stPfx),
              bexpFwd st e2 b2 t2 H2 (b2Pfx, stPfx) with
        | Pfx _ _ v1p _, Pfx _ _ v2p _ =>
        match v1p, v2p with
        | BVHoleP , _        => Pfx _ _ BVHoleP (BVHolePO _)
        | _       , BVHoleP  => Pfx _ _ BVHoleP (BVHolePO _)
        | BoolP v1, BoolP v2 => Pfx _ _ (BoolP (andb b1 b2)) (BoolPO _ _ eq_refl)
        end
        end
      | _  => fun _ => _
      end R
      end
    end ).
  Unshelve.
  all: try inversion R; eauto.
Defined.

Fixpoint bexpBwd {st : state} {b : bexp} {v : bool}
           {t : bexpT} (ev : (t :: b, st \\ v)%br):
     (prefix boolPO  (boolPartialize  v )) ->
     (prefix bexpPO  (bexpPartialize  b ) *
      prefix statePO (statePartialize st)).
  refine (
    match ev with
    | E_BTrue st => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize true)%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
       ( Pfx _ _ BTrueP _
       , Pfx _ _ (empty_stateP (keys st)) _)
      end R
      end
    | E_BFalse st => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize false)%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
       ( Pfx _ _ BFalseP _
       , Pfx _ _ (empty_stateP (keys st)) _)
      end R
      end
    | E_BEq n1 n2 st a1 a2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize (beq_nat n1 n2))%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
        let n1Pfx := Pfx _ (natPartialize n1) (natPartialize n1) _ in
        let n2Pfx := Pfx _ (natPartialize n2) (natPartialize n2) _ in
        match @aexpBwd st a1 n1 t1 H1 n1Pfx,
              @aexpBwd st a2 n2 t2 H2 n2Pfx with
        | (Pfx _ _ b1 _, st1), (Pfx _ _ b2 _, st2) =>
          (Pfx _ _ (BEqP b1 b2) _, stateLUB st1 st2)
        end
      end R
      end
    | E_BLe n1 n2 st a1 a2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize (leb n1 n2))%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
        let n1Pfx := Pfx _ (natPartialize n1) (natPartialize n1) _ in
        let n2Pfx := Pfx _ (natPartialize n2) (natPartialize n2) _ in
        match @aexpBwd st a1 n1 t1 H1 n1Pfx,
              @aexpBwd st a2 n2 t2 H2 n2Pfx with
        | (Pfx _ _ b1 _, st1), (Pfx _ _ b2 _, st2) =>
          (Pfx _ _ (BLeP b1 b2) _, stateLUB st1 st2)
        end
      end R
      end
    | E_BNot b st e t H => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize (negb b))%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
        let bPfx := Pfx _ (boolPartialize b) (boolPartialize b) _ in
        match bexpBwd st e b t H bPfx with
        | (Pfx _ _ b' _, st1) => (Pfx _ _ (BNotP b') _, st1)
        end
      end R
      end
    | E_BAnd b1 b2 st e1 e2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= boolPartialize (andb b1 b2))%vb -> _ with
      | BVHoleP => fun _ =>
       ( Pfx _ _ BHoleP (BHolePO _)
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | BoolP _ => fun _ =>
        let b1Pfx := Pfx _ (boolPartialize b1) (boolPartialize b1) _ in
        let b2Pfx := Pfx _ (boolPartialize b2) (boolPartialize b2) _ in
        match bexpBwd st e1 b1 t1 H1 b1Pfx,
              bexpBwd st e2 b2 t2 H2 b2Pfx with
        | (Pfx _ _ b1' _, st1), (Pfx _ _ b2' _, st2) =>
          (Pfx _ _ (BAndP b1' b2') _, stateLUB st1 st2)
        end
      end R
      end
    end ).
  Unshelve.
  all: inversion R; simpl; auto.
Defined.

Fixpoint comFwd {st : state} {c : com} {v : state}
           {t : comT} (ev : (t :: c, st \\ v)%cr):
     (prefix comPO   (comPartialize c) *
      prefix statePO (statePartialize st)) ->
      prefix statePO (statePartialize v).
  refine (
    match ev with
    | E_Skip st => fun cP' =>
      match cP' with | (Pfx _ _ cP R, T)  =>
      match cP return (cP <= comPartialize CSkip)%c -> prefix _ _ with
      | CHoleP      => fun R => T
      | CSkipP      => fun R => T
      | _  => fun _ => _
      end R
      end
    | E_Ass x a st v t HA => fun cP' =>
      match cP' with | (Pfx _ _ cP R, Pfx _ _ stP T) =>
      match cP return (cP <= comPartialize (CAss x a))%c -> prefix _ _ with
      | CHoleP => fun R =>
        Pfx _ _ (updateP stP x NHoleP) _ (* refine *)
      | CAssP _ ap => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let apPfx := Pfx aexpPO  (aexpPartialize  a ) ap  _ in (* refine *)
        match @aexpFwd st a v t HA (apPfx, stPfx) with
        | Pfx _ _ vp _ =>
          Pfx _ _ (updateP stP x vp) _ (* refine *)
        end
      | _  => fun _ => _
      end R
      end
    | E_Seq st st1 st2 c1 c2 t1 t2 H1 H2 => fun cP' =>
      match cP' with | (Pfx _ _ cP R, Pfx _ _ stP T)  =>
      match cP return (cP <= comPartialize (CSeq c1 c2))%c -> prefix _ _ with
      | CHoleP      => fun R =>
        let c1Pfx := Pfx comPO   (comPartialize c1) CHoleP _ in (* refine *)
        let c2Pfx := Pfx comPO   (comPartialize c2) CHoleP _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        comFwd st1 c2 st2 t2 H2 (c2Pfx, comFwd st c1 st1 t1 H1 (c1Pfx, stPfx))
      | CSeqP c1p c2p => fun R =>
        let c1Pfx := Pfx comPO (comPartialize c1) c1p _ in (* refine *)
        let c2Pfx := Pfx comPO (comPartialize c2) c2p _ in (* refine *)
        let stPfx := Pfx statePO (statePartialize st) stP T in
        comFwd st1 c2 st2 t2 H2 (c2Pfx, comFwd st c1 st1 t1 H1 (c1Pfx, stPfx))
      | _  => fun _ => _
      end R
      end
    | E_IfTrue st st1 b c1 c2 tb tc HB HC => fun cP' =>
      match cP' with | (Pfx _ _ cP R, Pfx _ _ stP T)  =>
      match cP return (cP <= comPartialize (CIf b c1 c2))%c -> prefix _ _ with
      | CHoleP        => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let c1Pfx := Pfx comPO (comPartialize c1) CHoleP  _ in (* refine *)
        comFwd st c1 st1 tc HC (c1Pfx, stPfx)
      | CIfP bp c1p _ => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let bPfx  := Pfx bexpPO  (bexpPartialize  b ) bp _  in (* refine *)
        match @bexpFwd st b true tb HB (bPfx, stPfx) with
        | Pfx _ _ vp _ =>
          match vp with
          | BVHoleP =>
            let c1Pfx := Pfx comPO (comPartialize c1) CHoleP  _ in (* refine *)
            comFwd st c1 st1 tc HC (c1Pfx, stPfx)
          | BoolP _ =>
            let c1Pfx := Pfx comPO (comPartialize c1) c1p  _ in (* refine *)
            comFwd st c1 st1 tc HC (c1Pfx, stPfx)
          end
        end
      | _  => fun _ => _
      end R
      end
    | E_IfFalse st st1 b c1 c2 tb tc HB HC => fun cP' =>
      match cP' with | (Pfx _ _ cP R, Pfx _ _ stP T)  =>
      match cP return (cP <= comPartialize (CIf b c1 c2))%c -> prefix _ _ with
      | CHoleP     => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let c2Pfx := Pfx comPO (comPartialize c2) CHoleP  _ in (* refine *)
        comFwd st c2 st1 tc HC (c2Pfx, stPfx)
      | CIfP bp _ c2p => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let bPfx  := Pfx bexpPO  (bexpPartialize  b ) bp  _ in (* refine *)
        match @bexpFwd st b false tb HB (bPfx, stPfx) with
        | Pfx _ _ vp _ =>
          match vp with
          | BVHoleP =>
            let stPfx := Pfx statePO (statePartialize st) stP T in
            let c2Pfx := Pfx comPO (comPartialize c2) CHoleP  _ in (* refine *)
            comFwd st c2 st1 tc HC (c2Pfx, stPfx)
          | BoolP _ =>
            let c2Pfx := Pfx comPO (comPartialize c2) c2p  _ in (* refine *)
            comFwd st c2 st1 tc HC (c2Pfx, stPfx)
          end
        end
      | _  => fun _ => _
      end R
      end
    | E_WhileFalse st b c t HB => fun cP' =>
      match cP' with | (Pfx _ _ cP R, T)  =>
      match cP return (cP <= comPartialize (CWhile b c))%c -> prefix _ _ with
      | CHoleP      => fun R => T
      | CWhileP _ _ => fun R => T
      | _  => fun _ => _
      end R
      end
    | E_WhileTrue st st1 st2 b c tb tc tw HB HC HW => fun cP' =>
      match cP' with | (Pfx _ _ cP R, Pfx _ _ stP T)  =>
      match cP return (cP <= comPartialize (CWhile b c))%c -> prefix _ _ with
      | CHoleP      => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let cPfx  := Pfx comPO (comPartialize c) CHoleP _ in (* refine *)
        let cwPfx := Pfx comPO (comPartialize (CWhile b c))
                         CHoleP _ in (* refine *)
        comFwd st1 (CWhile b c) st2 tw HW
               (cwPfx, comFwd st c st1 tc HC (cPfx, stPfx))
      | CWhileP bp cp => fun R =>
        let stPfx := Pfx statePO (statePartialize st) stP T in
        let bPfx  := Pfx bexpPO (bexpPartialize b) bp _ in (* refine *)
        match @bexpFwd st b true tb HB (bPfx, stPfx) with
        | Pfx _ _ vp _ =>
          match vp with
          | BVHoleP =>
            let cPfx  := Pfx comPO (comPartialize c) CHoleP _ in (* refine *)
            let cwPfx := Pfx comPO (comPartialize (CWhile b c))
                             CHoleP _ in (* refine *)
            comFwd st1 (CWhile b c) st2 tw HW
                   (cwPfx, comFwd st c st1 tc HC (cPfx, stPfx))
          | BoolP _ =>
            let cPfx  := Pfx comPO (comPartialize c) cp _ in (* refine *)
            let cwPfx := Pfx comPO (comPartialize (CWhile b c))
                             (CWhileP bp cp) _ in (* refine *)
            comFwd st1 (CWhile b c) st2 tw HW
                   (cwPfx, comFwd st c st1 tc HC (cPfx, stPfx))
          end
        end
      | _  => fun _ => _
      end R
      end
    end
    ).
  Unshelve.
  all: try inversion R; eauto.
Defined.

Fixpoint comBwd {st : state} {c : com} {v : state}
           {t : comT} (ev : (t :: c, st \\ v)%cr):
     (prefix statePO (statePartialize v )) ->
     (prefix comPO   (comPartialize   c ) *
      prefix statePO (statePartialize st)).
  refine (
    match ev with
    | E_Skip st => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
       ( Pfx _ _ CHoleP _, Pfx _ _ v _)
      end R
      end
    | E_Ass x a st av t HA => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize (update st x av))%s -> _ with
      | StatePNil => fun R =>
       (* If this happens it means we are assigning to a variable that does
          not exist in the state *)
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
        match lookupP v x with
        | NHoleP =>
          ( Pfx _ _ CHoleP _
          , Pfx _ _ (updateP v x NHoleP) _ )
        | NatP n =>
          let aPfx  := Pfx _ _ (natPartialize av) _ in
          let stPfx := Pfx _ _ (updateP v x NHoleP) _ in
          match @aexpBwd st a av t HA aPfx with
          | ( Pfx _ _ aP _, st' ) =>
            ( Pfx _ _ (CAssP x aP) _, stateLUB stPfx st' )
          end
        end
      end R
      end
    | E_Seq st st1 st2 c1 c2 t1 t2 H1 H2 => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st2)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
        let st2Pfx := Pfx _ (statePartialize st2) v _ in
        match comBwd st1 c2 st2 t2 H2 st2Pfx with
        | ( Pfx _ _ CHoleP _, st1P ) =>
          match comBwd st c1 st1 t1 H1 st1P with
          | ( Pfx _ _ CHoleP _, stP ) =>
            ( Pfx _ _ CHoleP _, stP )
          | ( Pfx _ _ c1P _   , stP ) =>
            ( Pfx _ _ (CSeqP c1P CHoleP) _, stP )
          end
        | ( Pfx _ _ c2P _, st1P ) =>
          match comBwd st c1 st1 t1 H1 st1P with
          | ( Pfx _ _ c1P _   , stP ) =>
            ( Pfx _ _ (CSeqP c1P c2P) _, stP )
          end
        end
      end R
      end
    | E_IfTrue st st1 b c1 c2 tb tc HB HC => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st1)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
        let st1Pfx := Pfx _ (statePartialize st1) v _ in
        match comBwd st c1 st1 tc HC st1Pfx with
        | ( Pfx _ _ CHoleP _, stP ) => ( Pfx _ _ CHoleP _, stP )
        | ( Pfx _ _ c1P _, stP ) =>
          let bPfx := Pfx _ (boolPartialize true) (boolPartialize true) _ in
          match @bexpBwd st b true tb HB bPfx with
          | ( Pfx _ _ bP _, stB ) =>
            ( Pfx _ _ (CIfP bP c1P CHoleP) _, stateLUB stP stB )
          end
        end
      end R
      end
    | E_IfFalse st st1 b c1 c2 tb tc HB HC => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st1)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
        let st1Pfx := Pfx _ (statePartialize st1) v _ in
        match comBwd st c2 st1 tc HC st1Pfx with
        | ( Pfx _ _ CHoleP _, stP ) => ( Pfx _ _ CHoleP _, stP )
        | ( Pfx _ _ c2P _, stP ) =>
          let bPfx := Pfx _ (boolPartialize false) (boolPartialize false) _ in
          match @bexpBwd st b false tb HB bPfx with
          | ( Pfx _ _ bP _, stB ) =>
            ( Pfx _ _ (CIfP bP CHoleP c2P) _, stateLUB stP stB )
          end
        end
      end R
      end
    | E_WhileFalse st b c t HB => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
       ( Pfx _ _ CHoleP _, arg )
      end R
      end
    | E_WhileTrue st st1 st2 b c tb tc tw HB HC HW => fun arg =>
      match arg with Pfx _ _ v R =>
      match v return (v <= statePartialize st2)%s -> _ with
      | StatePNil => fun R =>
       ( Pfx _ _ CHoleP _
       , Pfx _ _ (empty_stateP (keysP (statePartialize st)))
                 (empty_stateP_leq (statePartialize st)))
      | StatePCons _ _ _ => fun R =>
        let st2Pfx := Pfx _ (statePartialize st2) v _ in
        match comBwd st1 (CWhile b c) st2 tw HW st2Pfx with
        | (Pfx _ _ CHoleP _, st2P) =>
          match comBwd st c st1 tc HC st2P with
          | ( Pfx _ _ CHoleP _, stP ) => ( Pfx _ _ CHoleP _, stP )
          | ( Pfx _ _ cP _, stP ) =>
            let bPfx := Pfx _ (boolPartialize true) (boolPartialize true) _ in
            match @bexpBwd st b true tb HB bPfx with
            | ( Pfx _ _ bP _, stBP ) =>
              ( Pfx _ _ (CWhileP bP cP) _, stateLUB stP stBP )
            end
          end
        | (cWP, st2P) =>
          match comBwd st c st1 tc HC st2P with
          | ( Pfx _ _ cP _, stP ) =>
            let bPfx := Pfx _ (boolPartialize true) (boolPartialize true) _ in
            match @bexpBwd st b true tb HB bPfx with
            | ( Pfx _ _ bP _, stBP ) =>
              ( comLUB (Pfx _ _ (CWhileP bP cP) _) cWP, stateLUB stP stBP )
            end
          end
        end
      end R
      end
    end ).
  Unshelve.
  all: simpl; eauto.
Defined.

(** ** Slicing helper tactics  *)

(* Find (Pfx arg, Pfx state) arguments to a forward slicing function and
   remember them *)
Ltac remember_fwd_args :=
  repeat match goal with
         | [ H : context[aexpFwd _ (Pfx ?R ?T ?X ?P, ?S)] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R T X P, S) as p
         | [ H : context[bexpFwd _ (Pfx ?R ?T ?X ?P, ?S)] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R T X P, S) as p
         | [ H : context[comFwd  _ (Pfx ?R ?T ?X ?P, ?S)] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R T X P, S) as p
         end.

(* Find (Pfx arg, Pfx state) arguments to a forward slicing function and
   remember each of them separately *)
Ltac remember_fwd_args_separately :=
  repeat match goal with
         | [ H : context[aexpFwd _ (Pfx ?R ?T ?X ?P, ?S)] |- _ ] =>
           let p := fresh "p" in
           let q := fresh "p" in
           remember (Pfx R T X P) as p;
           remember S as q
         | [ H : context[bexpFwd _ (Pfx ?R ?T ?X ?P, ?S)] |- _ ] =>
           let p := fresh "p" in
           let q := fresh "p" in
           remember (Pfx R T X P) as p;
           remember S as q
         end.

Ltac remember_comFwd :=
  repeat match goal with
         | [ |- context[comFwd ?EV ?ARG] ] =>
           let p := fresh "p" in
           remember (comFwd EV ARG) as p; destruct p
         end.

(* Find Pfx argument to backwards slicing function and remember them *)
Ltac remember_bwd_args :=
  repeat match goal with
         | [ H : context[aexpBwd _ (Pfx ?R ?T ?X ?P)] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R T X P) as p
         | [ H : context[bexpBwd _ (Pfx ?R ?T ?X ?P)] |- _ ] =>
           let p := fresh "p" in
           remember (Pfx R T X P) as p
         end.

(* do_*_rewrites is used in consistency proofs. Once we have specialized the
   inductive hypothesis IH we can perform rewrites using other premisses.  first
   match should trigger premisses like:

      IH : prefixO _ _ _ (aexpFwd _ _ (aexpBwd A B)), H : aexpBwd A B = _

   In other words these deals with calls to bwd nested in fwd or the other way
   around.
 *)
Ltac do_prefixO_rewrites_l2r :=
  repeat match goal with
         | [ IH : prefixO _ _ _ (_ _ ?R), H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ IH : prefixO _ _ _ ?R, H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ IH : prefixO _ _ ?R _, H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ H : ?X = Pfx _ _ _ _ |- prefixO _ _ ?X _ ] =>
           rewrite H
         | [ H : ?X = Pfx _ _ _ _ |- prefixO _ _ _ ?X ] =>
           rewrite H
         end.

Ltac do_prefixO_rewrites_r2l :=
  repeat match goal with
         | [ IH : prefixO _ _ _ (_ _ ?R), H : _ = ?R |- _ ] =>
           rewrite <- H in IH
         | [ IH : prefixO _ _ _ ?R, H : _ = ?R |- _ ] =>
           rewrite <- H in IH
         | [ IH : prefixO _ _ ?R _, H : _ = ?R |- _ ] =>
           rewrite <- H in IH
         | [ H : Pfx _ _ _ _ = ?R |- prefixO _ _ ?R _ ] =>
           rewrite <- H
         | [ H : Pfx _ _ _ _ = ?R |- prefixO _ _ _ ?R ] =>
           rewrite <- H
         | [ H : Pfx _ _ _ _ = ?R |- prefixO _ _ _ (_ _ ?R) ] =>
           rewrite <- H
         | [ H : Pfx _ _ _ _ = ?R |- prefixO _ _ _ (_ _ (_, ?R)) ] =>
           rewrite <- H
         end.

Ltac do_pairR_rewrites :=
  repeat match goal with
         | [ IH : pairR _ (_ _ ?R) _, H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ IH : pairR _ _ (_ _ ?R), H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ IH : pairR _ ?R _, H : ?R = _ |- _ ] =>
           rewrite H in IH
         | [ IH : pairR _ _ ?R, H : ?R = _ |- _ ] =>
           rewrite H in IH
         end.

Ltac solve_pairR_by_rewrites_l2r :=
  apply PairR;
    (* eliminate calls to fst and snd originating from definition of PairR *)
    simpl;
    (* Rewrite variables originating from IH instantiations with their
       actual definitions. *)
    do_prefixO_rewrites_l2r;
    (* We have all the facts in the context, finish with eauto *)
    eauto.

Ltac pose_state_and_specialize :=
  repeat match goal with
         | [ H : (_ <= ?S)%s, IH : forall (_ : prefix _ ?S), _ |- _ ] =>
           specialize (IH (Pfx _ _ _ H))
         end.

Ltac transitivity :=
  match goal with
  | [ H1 : (?S <= ?E)%s, H2 : Pfx _ _ ?E _ = ?R
      |- prefixO _ _ (Pfx _ _ ?S _) _ ] =>
    apply transitive_prefixO with R; auto
  end.

Ltac progress_by_prefixO_transitivty :=
    match goal with
    | [ H : prefixO statePO _ ?A ?B |- prefixO _ _ ?A ?C ] =>
      apply (transitive_prefixO _ _ transitive_statePO A B C);
        [ assumption | idtac ]
    end.

(** ** Empty state lemmas *)

Lemma cevalR_empty_initial_state :
  forall {s : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, s \\ v)%cr),
    s = StateNil -> v = StateNil.
Proof.
  intros s c v t ev H.
  induction ev; subst; auto.
Qed.

Hint Resolve cevalR_empty_initial_state.

Lemma cevalR_empty_final_state :
  forall {s : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, s \\ v)%cr),
    v = StateNil -> s = StateNil.
Proof.
  intros s c v t ev H.
  induction ev; subst; basic_empty_state_reasoning; auto.
Qed.

Hint Resolve cevalR_empty_final_state.

Lemma comBwd_empty_state :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr)
         (H  : (StatePNil <= (statePartialize v ))%s)
         (HC : (CHoleP    <= (comPartialize   c ))%c)
         (HS : (StatePNil <= (statePartialize st))%s),
    comBwd ev (Pfx statePO (statePartialize v) StatePNil H) =
      ( Pfx comPO   (comPartialize   c ) CHoleP    HC
      , Pfx statePO (statePartialize st) StatePNil HS ).
Proof.
  intros st c v t ev H HC HS.
  destruct ev; basic_empty_state_reasoning; simpl; f_equal; auto.
Qed.

Hint Resolve comBwd_empty_state.

Lemma comFwd_empty_state :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr)
         (H  : (StatePNil <= (statePartialize v ))%s)
         (HC : (CHoleP    <= (comPartialize   c ))%c)
         (HS : (StatePNil <= (statePartialize st))%s),
    comFwd ev ( Pfx comPO   (comPartialize   c ) CHoleP    HC
              , Pfx statePO (statePartialize st) StatePNil HS ) =
    (Pfx statePO (statePartialize v) StatePNil H).
Proof.
  intros st c v t ev H HC HS.
  induction ev; basic_empty_state_reasoning; simpl;
  generalize_nested_pfx_proofs; auto.
  erewrite IHev1. erewrite IHev2. eauto.
  erewrite IHev1. erewrite IHev2. eauto.

  Unshelve.
  all: pose proof (cevalR_empty_initial_state ev1 eq_refl);
    subst; auto.
Qed.

Hint Resolve comFwd_empty_state.

Ltac empty_state_reasoning :=
  basic_empty_state_reasoning;
  try match goal with
      | [ H : (_ :: _, StateNil \\ _)%cr |- _ ] =>
        let P := fresh "P" in
        pose proof (cevalR_empty_initial_state H eq_refl) as P;
        subst
      end;
  try match goal with
      | [ H : (_ :: _, _ \\ StateNil)%cr |- _ ] =>
        let P := fresh "P" in
        pose proof (cevalR_empty_final_state H eq_refl) as P;
        subst
      end.

(** ** Monotonicity proofs *)

Ltac solve_monotonicity_by_IH :=
  match reverse goal with
  | [ IH : forall (e1 : ?X * ?Y) (e2 : ?X * ?Y), _ -> _, E1 : ?X, S1: ?Y
    , E2 : ?X, S2 : ?Y |- _ ] =>
    (* Get past silly Ltac scope checker that would otherwise complain that
       HFresh is not in scope *)
    let HFresh := fresh "HFresh" in
    (* Instantiate inductive hypothesis to derive contradiction.  The third
       argument is a pair, whose construction requires simplifying calls to fst
       and snd (cf. definition of pairR) as well as rewriting using equalities
       in the context.  We use refine to create a shelved goal corresponding to
       a pair - we will create it later with tactics.  Since we don't really
       care about the definition of HFresh, only its type, we use clearbody.
       Type of IH is a prefixO that represents inequality between calls to
       forward slicing *)
    refine (let HFresh := IH (E1, S1) (E2, S2) _ in _); clearbody HFresh;

    (* Again, we make use of equalities created with destruct.  We rewrite call
       to aexpFwd with its result, so that instead of inequality between calls
       to aexpFwd we now have inequality between resulting nats. *)

    do_prefixO_rewrites_l2r;
    (* We take the prefixO apart to extract contradicting inequalities.  Having
       contradicting inequalities allows us to finish the proof.  We insist that
       this solves the goal, since there are a couple of valid ways pattern
       matching above can instantiate IH, not all of them leading to solving the
       goal *)
    solve [ invert_prefixO; eauto ]
  end.

Theorem aexpFwd_monotone :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    monotone (pairR (prefixO aexpPO  (aexpPartialize a),
                     prefixO statePO (statePartialize st)))
             (prefixO natPO (NatP v)) (aexpFwd ev).
Proof.
  intros st a v t ev e1 e2 H.
  (* proof proceeds by induction on the structure of evaluation derivation *)
  induction ev;
    (* e1 and e2 are two prefix expression in <= relation.  We destruct them to
       get the actual expressions, their respective states and evidence of
       <= relation out from a prefix set representation. *)
    destruct e1 as [[e1 He1] [st1 Hst1]], e2 as [[e2 He2] [st2 Hst2]];
    (* H contains evidence that e1 is smaller than e2 (both being prefix
       expression of common (e, st)).  We extract these evidence so that we can
       invert them later. *)
    inversion H as [a b c d HExp HSt [eqA eqB] [eqC eqD]]; subst;
    (* we need to simplify calls to fst and snd in both evidence -
       cf. definition of pairR *)
    simpl in HExp, HSt;
    (* simplify main goal.  This essentially unfolds the structure of aexpFwd,
       uncovering matches on e1 and e2 that we are going to destruct. *)
    simpl;
    (* destruct the first expression *)
    destruct e1;
    (* now that we have learned something about e1 we can invert evidence HExp
       and HSt.  We mostly need HExp, but dealing with AIdP cases also
       requires facts from HSt. *)
    invert_prefixO;
    (* Further destroy any subexpression being matched on in aexpFwd.  This is
       crucial for recursive cases (APlusP, AMinusP, AMultP) *)
    destruct_goal_matches;
    (* At this point we have 56 goals.  eauto solves most of them, leaving only
       contradicting ones.

       Note: it is important to have lookupP_monotone in the hint database
       to deal with (AIdP x <= AIdP y) case *)
    eauto;

    (* Discharge impossible cases. All the cases should be solved by doing at
       most two iterations of the loop inside the tactic. *)
    dismiss_impossible_aexp_inequalities;
    dismiss_impossible_value_inequalities;

    (* At this point we are left with six contradicting cases that can only be
       discharged by relying on inductive hypothesis.  There are two cases each
       for plus, minus and mult, one case being a contradiction in the first
       subexpression and second case being contradiction in the second
       subexpression. *)

    (* Extract inequalities between subexpressions *)
    invert_expression_inequalities;

    (* Equalities saved earlier when doing destruct contain facts we need to
       pass as arguments to an indeuctive hypothesis.  We extract them to become
       stand-alone premises *)
    remember_fwd_args_separately;
    solve_monotonicity_by_IH.

  (* We are now left with constructing pairs that act as arguments to inductive
     hypotheses.  We begin by unshelving the goals *)
  Unshelve.

  all: solve_pairR_by_rewrites_l2r.
Qed.

Ltac solve_by_aexpFwd_monotonicity :=
  match reverse goal with
  | [ H : aexpFwd ?EV _ = _ |- _ ] =>
    pose proof (aexpFwd_monotone EV); unfold monotone in *;
    (* We insist the the goal is solved, because there are two sets of evidence
       lying around (for first and second subexpression).  If chosing the first
       does not solve the goal then chosing the second one will *)
    solve [ solve_monotonicity_by_IH ]
  end.

Theorem aexpBwd_monotone :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    monotone (prefixO natPO (NatP v))
             (pairR (prefixO aexpPO  (aexpPartialize a),
                     prefixO statePO (statePartialize st)))
             (aexpBwd ev).
Proof.
  intros st a v t ev v1 v2 H.
  (* proof proceeds by induction on the structure of evaluation derivation *)
  induction ev;
    (* v1 and v2 are two prefix nats in <= relation.  We destruct them to get
       the actual nats and evidence of <= relation out from a prefix set
       representation. *)
    destruct v1 as [v1 Hv1], v2 as [v2 Hv2];
    (* H contains evidence that v1 is smaller than v2 (both being prefix
       expression of common v).  We extract these evidence so that we can invert
       them later. *)
    inv H;
    (* simplify main goal.  This essentially unfolds the structure of aexpBwd,
       uncovering matches on v1 and v2 that we are going to destruct. *)
    simpl;
    (* destruct the first nat *)
    destruct v1;
    (* Further destroy any subnats being matched on in aexpBwd. *)
    destruct_goal_matches;
    (* solves most of the goals *)
    eauto;
    (* Discharge the remaining impossible cases. *)
    dismiss_impossible_value_inequalities.
Qed.

Hint Resolve aexpFwd_monotone aexpBwd_monotone.

Theorem bexpFwd_monotone :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    monotone (pairR (prefixO bexpPO  (bexpPartialize b),
                     prefixO statePO (statePartialize st)))
             (prefixO boolPO (BoolP v)) (bexpFwd ev).
Proof.
  intros st b v t ev e1 e2 H.
  (* proof proceeds by induction on the structure of evaluation derivation *)
  induction ev;
    (* e1 and e2 are two prefix expression in <= relation.  We destruct them to
       get the actual expressions, their respective states and evidence of
       <= relation out from a prefix set representation. *)
    destruct e1 as [[e1 He1] [st1 Hst1]], e2 as [[e2 He2] [st2 Hst2]];
    (* H contains evidence that e1 is smaller than e2 (both being prefix
       expression of common (e, st)).  We extract these evidence so that we can
       invert them later. *)
    inversion H as [f g c d HExp HSt [eqA eqB] [eqC eqD]]; subst;
    (* we need to simplify calls to fst and snd in both evidence -
       cf. definition of pairR *)
    simpl in HExp, HSt;
    (* simplify main goal.  This essentially unfolds the structure of bexpFwd,
       uncovering matches on e1 and e2 that we are going to destruct. *)
    simpl;
    (* destruct the first expression. Results in 42 goals *)
    destruct e1;
    (* now that we have learned something about e1 we can invert evidence HExp
       and HSt.  We mostly need HExp, but dealing with AIdP cases also
       requires facts from HSt. *)
    invert_prefixO;
    (* Further destroy any subexpression being matched on in bexpFwd.  This is
       crucial for recursive cases (BEq, BLe, BAnd, BNot) *)
    destruct_goal_matches;
    (* At this point we have 65 goals.  eauto solves most of them, leaving only
       contradicting ones. *)
    eauto;

    (* Discharge impossible cases. All the cases should be solved by doing at
       most two iterations of the loop inside the tactic. *)
    dismiss_impossible_bexp_inequalities;
    dismiss_impossible_value_inequalities;

    (* At this point we are left with 7 contradicting cases that can only be
       discharged by relying on inductive hypothesis.  There are two cases each
       for BEq, BLe and BAnd, one case being a contradiction in the first
       subexpression and second case being contradiction in the second
       subexpression.  There is one case for BNot.  BEq and BLe cases are solved
       by deriving contradiction from monotonicity of aexpFwd.  BAnd and BNot
       cases are solved by deriving contradiction from IH *)

    (* Extract inequalities between subexpressions *)
    invert_expression_inequalities;

    (* Equalities saved earlier when doing destruct contain facts we need to
       pass as arguments to an indeuctive hypothesis.  We extract them to become
       stand-alone premises *)
    remember_fwd_args_separately;

    (* Solve BEq and BLe cases by relying on montonicity of aexpFwd *)
    try solve_by_aexpFwd_monotonicity;
    (* Solve remaining BAnd and BLe cases *)
    solve_monotonicity_by_IH.

  (* We are now left with constructing pairs that act as arguments to inductive
     hypotheses.  We begin by unshelving the goals *)
  Unshelve.

  all: solve_pairR_by_rewrites_l2r.
Qed.

Theorem bexpBwd_monotone :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    monotone (prefixO boolPO (BoolP v))
             (pairR (prefixO bexpPO  (bexpPartialize b),
                     prefixO statePO (statePartialize st)))
             (bexpBwd ev).
Proof.
  intros st b v t ev v1 v2 H.
  (* proof proceeds by induction on the structure of evaluation derivation *)
  induction ev;
    (* v1 and v2 are two prefix nats in <= relation.  We destruct them to get
       the actual nats and evidence of <= relation out from a prefix set
       representation. *)
    destruct v1 as [v1 Hv1], v2 as [v2 Hv2];
    (* H contains evidence that v1 is smaller than v2 (both being prefix
       expression of common v).  We extract these evidence so that we can invert
       them later. *)
    inv H;
    (* simplify main goal.  This essentially unfolds the structure of aexpBwd,
       uncovering matches on v1 and v2 that we are going to destruct. *)
    simpl;
    (* 12 goals. destruct the first nat *)
    destruct v1;
    (* 24 goals. Further destroy any subnats being matched on in aexpBwd. *)
    destruct_goal_matches;
    (* Equalize Pfx proof terms so that they don't get in the way *)
    equalize_pfx_proofs;
    (* Find situations where two calls to slicing give different results and
       assert that these results are equal *)
    repeat match goal with
           | [ H1 : ?F ?EV ?ARG = _, H2 : ?F ?EV ?ARG = _ |- _ ] =>
             rewrite H1 in H2; clear H1
           end;
    (* Now invert resulting Pfx equalities *)
    invert_pfx_equalities;
    (* solves 38 out of 48 goals *)
    eauto;
    (* solves remaining 10 goals *)
    dismiss_impossible_value_inequalities.
Qed.

Ltac solve_by_bexpFwd_monotonicity :=
  match reverse goal with
  | [ H : bexpFwd ?EV _ = _ |- _ ] =>
    pose proof (bexpFwd_monotone EV); unfold monotone in *;
    (* We insist the the goal is solved, because there are two sets of evidence
       lying around (for first and second subexpression).  If chosing the first
       does not solve the goal then chosing the second one will *)
    solve [ solve_monotonicity_by_IH ]
  end.

Theorem comFwd_monotone :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    monotone (pairR (prefixO comPO   (comPartialize   c),
                     prefixO statePO (statePartialize st)))
             (prefixO statePO (statePartialize v)) (comFwd ev).
Proof.
  intros st c v t ev e1 e2 H.
  (* proof proceeds by induction on the structure of evaluation derivation *)
  induction ev;
    (* e1 and e2 are two prefix expression in <= relation.  We destruct them to
       get the actual expressions, their respective states and evidence of
       <= relation out from a prefix set representation. *)
    destruct e1 as [[e1 He1] [est1 Hest1]], e2 as [[e2 He2] [est2 Hest2]];
    (* H contains evidence that e1 is smaller than e2 (both being prefix
       expression of common (e, st)).  We extract these evidence so that we can
       invert them later. *)
    inversion H as [d e f g HExp HSt [eqA eqB] [eqC eqD]]; subst;
    (* we need to simplify calls to fst and snd in both evidence -
       cf. definition of pairR *)
    simpl in HExp, HSt;
    (* simplify main goal.  This essentially unfolds the structure of bexpFwd,
       uncovering matches on e1 and e2 that we are going to destruct. *)
    simpl;
    (* destruct the first command. Results in 42 goals *)
    destruct e1;
    (* now that we have learned something about e1 we can invert evidence HExp
       and HSt.  *)
    invert_prefixO;
    (* Further destroy any subexpression being matched on in bexpFwd.  This is
       crucial for recursive cases (BEq, BLe, BAnd, BNot) *)
    destruct_goal_matches;
    (* At this point we have 291 goals. Discharging impossible cases leaves us
       with 33. *)
    dismiss_impossible_com_inequalities;
    (* Extract inequalities between subexpressions *)
    invert_expression_inequalities;
    (* We can now solve most of the goals with eauto *)
    eauto;
    (* We are left with 4 contradiciting cases. In all cases we can derive
       contradiction from the inductive hypothesis and knowledge that aexp and
       bexp slicing is monotone. *)
    remember_fwd_args_separately;
    (* This solves two cases for if and one for while *)
    try solve_by_bexpFwd_monotonicity;
    (* This solves a case for assignment *)
    solve_by_aexpFwd_monotonicity.

  (* We are now left with constructing pairs that act as arguments to inductive
     hypotheses. *)
  Unshelve. all: solve_pairR_by_rewrites_l2r.
Qed.

Arguments updateP st k v : simpl never.

Theorem comBwd_monotone :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    monotone (prefixO statePO (statePartialize v))
             (pairR (prefixO comPO   (comPartialize   c),
                     prefixO statePO (statePartialize st)))
             (comBwd ev).
Proof.
  intros st c v t ev e1 e2 H.
  induction ev;
    (* e1 and e2 are two state prefix in <= relation.  We destruct them to get
       the actual partial states and evidence they are smalle than the
       lattice top. *)
    destruct e1 as [v1 Hv1], e2 as [v2 Hv2];
    (* Invert evidence that v1 <= v2  *)
    inv H; simpl;
    (* destruct the first slicing criterion. Results in 14 goals: 7 possible
    reductions in ev * two possible partial state values (nil, cons) *)
    destruct v1;
    (* Destruct all matches. Produces 403 goals. *)
    destruct_goal_matches;
    (* Some cases are impossible. Dismiss them. *)
    dismiss_impossible_state_inequalities;
    (* 327 goals left. *)
    unfold_expression_partialize; dismiss_impossible_com_inequalities;
    (* 255 goals left *)
    (* We now solve all goals, except for assignment, which requires some more
       work *)
    try solve [
      repeat match goal with
             | [ IH : forall _ _ _, _, H1 : _ _ (Pfx _ _ ?A1 ?R1) = _
               , H2 : _ _ (Pfx _ _ ?A2 ?R2) = _, EV : (?A1 <= ?A2)%s |- _ ] =>
               (* Repeatedly use inductive hypotheses.  In several cases
                  evidence EV is obtained from using one of the IH ans is then
                  used to trigger the second IH.  That's why we use repeat. *)
               specialize (IH (Pfx _ _ A1 R1) (Pfx _ _ A2 R2)
                              (PfxO _ _ _ _ _ _ EV));
               (* Rewrite calls to comBwd in the IH we just triggered *)
               do_pairR_rewrites;
               (* Invert rewriten IH to obtain further evidence *)
               invert_pairR;
               (* Substitute what we've learned from inversion *)
               subst
             end;
      (* WhileTrue cases crucially rely on properties of comLUB.  We need to
         manually trigger the relevant theorems.  But first equalize proofs and
         invert inequalities to make sure that all the evidence have the right
         form. *)
      equalize_pfx_proofs;
      invert_expression_inequalities; subst;
      try pose_bexpLUB_prefixO;
      try pose_comLUB_prefixO;
      try match goal with
          | [ H1  : comLUB (Pfx _ _ ?X1 _) (Pfx _ _ ?Y1 _) = _
            , H2  : comLUB (Pfx _ _ ?X2 _) (Pfx _ _ ?Y2 _) = _
            , EV1 : (?X1 <= ?X2)%c
            , EV2 : (?Y1 <= ?Y2)%c |- _ ] =>
            pose proof (comLUB_prefixO _ _ _ _ _ _ _ H1 H2)
          end;
      try match goal with
          | [ H1 : bexpLUB (Pfx _ _ ?X ?P) (Pfx _ _ ?Y1 _) = _
            , H2 : bexpLUB (Pfx _ _ ?X ?Q) (Pfx _ _ ?Y2 _) = _
            , EV : (?Y1 <= ?Y2)%b |- _ ] =>
            pose proof (bexpLUB_prefixO _ _ _ _ _ _ _ H1 H2
                                        (PfxO _ _ X X P Q (reflexive_bexpPO X))
                                        (PfxO _ _ _ _ _ _ EV))
          end;
      (* Specilize hypotheses obtained from above theorems *)
      repeat match goal with
             | [ IH : prefixO _ _ (Pfx _ _ ?A1 ?R1) (Pfx _ _ ?A2 ?R2) -> _
               , EV : (?A1 <= ?A2)%b |- _ ] =>
               specialize (IH (PfxO _ _ _ _ R1 R2 EV))
             | [ IH : prefixO _ _ (Pfx _ _ ?A1 ?R1) (Pfx _ _ ?A2 ?R2) -> _
               , EV : (?A1 <= ?A2)%c |- _ ] =>
               specialize (IH (PfxO _ _ _ _ R1 R2 EV))
             end;
      invert_prefixO;
      (* Some cases for WhileTrue are in fact contradiciting.  Not sure why --
         this is strange. *)
      dismiss_impossible_com_inequalities;
      (* Most of the cases solve by simplification and eauto *)
      simpl; eauto ].

  (* We are left with a single contradiciting case that solves by crucially
     relying on lookupP_contra lemma. *)
  exfalso; eauto.
Qed.

Ltac comFwd_monotonicity :=
  match goal with
  | [ H1 : Pfx _ _ ?A _ = comFwd ?EV ?P1, H2 : Pfx _ _ ?B _ = comFwd ?EV ?P2
    |- prefixO _ _ (Pfx _ _ ?A _) (Pfx _ _ ?B _) ] =>
    pose proof (comFwd_monotone EV P1 P2)
  | [ |- prefixO _ _ (comFwd ?EV ?A) (comFwd ?EV ?B) ] =>
    pose proof (comFwd_monotone EV A B)
  end.

(** ** Auxiliary lemmas *)

(* If forward slicing in state st1 yields a hole then slicing in a smaller state
   st2 will also yield a hole. *)
Lemma aexpFwd_hole_in_smaller_state :
   forall (a : aexp) (v : nat) (t : aexpT) (st : state)
          (st1 st2 : prefix statePO (statePartialize st))
          (ev : (t :: a, st \\ v)%ar)
          (arg arg2 : prefix aexpPO (aexpPartialize a)) P,
      (prefixO aexpPO  (aexpPartialize a) arg2 arg) ->
      (prefixO statePO (statePartialize st) st2 st1) ->
      aexpFwd ev (arg , st1) = Pfx natPO (natPartialize v) NHoleP P ->
      aexpFwd ev (arg2, st2) = Pfx natPO (natPartialize v) NHoleP P.
Proof.
  intros a v t st st1 st2 ev arg arg2 P S B A.
  pose proof (aexpFwd_monotone ev).
  unfold monotone in H.
  solve_monotonicity_by_IH.

  Unshelve.
  auto.
Qed.

Hint Resolve aexpFwd_hole_in_smaller_state.

Lemma bexpFwd_hole_in_smaller_state :
   forall (b : bexp) (v : bool) (t : bexpT) (st : state)
          (st1 st2 : prefix statePO (statePartialize st))
          (ev : (t :: b, st \\ v)%br)
          (arg arg2 : prefix bexpPO (bexpPartialize b)) P,
      (prefixO bexpPO  (bexpPartialize b) arg2 arg) ->
      (prefixO statePO (statePartialize st) st2 st1) ->
      bexpFwd ev (arg,  st1) = Pfx boolPO (boolPartialize v) BVHoleP P ->
      bexpFwd ev (arg2, st2) = Pfx boolPO (boolPartialize v) BVHoleP P.
Proof.
  intros a v t st st1 st2 ev arg arg2 P S B A.
  pose proof (bexpFwd_monotone ev).
  unfold monotone in H.
  solve_monotonicity_by_IH.

  Unshelve.
  auto.
Qed.

Hint Resolve bexpFwd_hole_in_smaller_state.

(* Helper for solving aexp consistency (inflation) proof.  Having a `stateLUB
   S1 S2` (LUB argument) we assert that either S1 or S2 (S argument) is smaller
   than the LUB (we use the helper to avoid repeating the same code twice).  We
   then substitite call to stateLUB into our hypotheses and using premise P we
   call the auxiliary lemma.  We should then be able to solve the goal.  If not
   it means we have chosen the wrong state S and need to backtrack in the
   calling branch. *)
Ltac progress_aexp_consistency_in_smaller_state Eq A1 A2 S LUB H :=
  let P1 := fresh "P" in
  assert (P1 : prefixO _ _ A1 A2) by auto;
  let P2 := fresh "P" in
  assert (P2 : prefixO _ _ S LUB) by auto;
  rewrite <- Eq in H; (* rewrite necessary for apply to work *)
  (* Use a smaller state so that we can use the IH *)
  apply (aexpFwd_hole_in_smaller_state _ _ _ _ _ _ _ _ _ _ P1 P2) in H.

Ltac progress_aexp_consistency :=
 (* To use the inductive hypothesis we need to switch from (stateLUB S1 S2) to
    either S1 (first contradicting case being solved) or S2 (second case).  But
    we don't really know which case we are solving so there is no other way than
    trying to solve the goal and backtracking if it fails. *)
  match goal with
  | [ S1 : prefix _ ?ST, S : stateLUB ?S1 ?S2 = ?R
    , H : aexpFwd ?EV (?A, ?R) = Pfx _ _ NHoleP _
    , H0 : aexpBwd ?EV _ = (_, ?S2) |- _ ] =>
    progress_aexp_consistency_in_smaller_state S A A S2 (stateLUB S1 S2) H
  | [ S1 : prefix _ ?ST, S : stateLUB ?S2 ?S1 = ?R
    , H : aexpFwd ?EV (?A, ?R) = Pfx _ _ NHoleP _
    , H0 : aexpBwd ?EV _ = (_, ?S2) |- _ ] =>
    progress_aexp_consistency_in_smaller_state S A A S2 (stateLUB S2 S1) H
  end.

Ltac solve_consistency_by_IH :=
  match reverse goal with
  | [ IH : pairR _ ?F ?S -> _, F : ?F = _, S: ?S = _ |- _ ] =>
    (* Get past silly Ltac scope checker that would otherwise complain that
       HFresh is not in scope *)
    let HFresh := fresh "HFresh" in
    (* Instantiate inductive hypothesis to derive contradiction.  The third
       argument is a pair, whose construction requires simplifying calls to fst
       and snd (cf. definition of pairR) as well as rewriting using equalities
       in the context.  We use refine to create a shelved goal corresponding to
       a pair - we will create it later with tactics.  Since we don't really
       care about the definition of HFresh, only its type, we use clearbody.
       Type of IH is a prefixO that represents inequality between calls to
       forward slicing *)
    refine (let HFresh := IH _ in _); clearbody HFresh;

    (* Remove a redundant Pfx equality *)
    clear_pfx_equalities;

    (* Again, we make use of equalities created with destruct.  We rewrite call
       to aexpFwd with its result, so that instead of inequality between calls
       to aexpFwd we now have inequality between resulting nats. *)
    do_prefixO_rewrites_r2l;

    (* We take the prefixO apart to extract contradicting inequalities.  Having
       contradicting inequalities allows us to finish the proof.  We insist that
       this solves the goal, since there are a couple of valid ways pattern
       matching above can instantiate IH, not all of them leading to solving the
       goal *)
    solve [ invert_prefixO; auto ]
  end.

(** ** Consistency proofs *)

Theorem aexpFwdBwd_inflating :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    inflation (prefixO natPO (NatP v)) (aexpBwd ev) (aexpFwd ev).
Proof.
  intros st a v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input nat *)
    destruct pfx as [v Hv]; destruct v;
    (* At the point we have 10 goals. Auto leaves us with four. *)
    try apply PfxO; auto;
    (* Case for E_Id solved easily with a lemma *)
    try solve [ rewrite lookupP_updateP; eauto ];
    (* We are now left with cases for plus, minus and mult.*)
    (* Simplify calls to slicing.  Uncovers matches. *)
    simpl;
    (* Destruct all matches *)
    destruct_matches;
    (* For each binary operator we arrive at four subcases.  First three
       are contradictory (NatP <= NHoleP as a goal), the 4th is true.
       We dismiss first and last case easily. *)
    try discriminate; auto;
    (* The middle two cases require some more work.  We begin by
       inverting all premisses containing Pfx equalities *)
    invert_pfx_equalities;
    (* Remember Pfx arguments of aexpBwd.  We need to abstract them away
       for further rewrites to work *)
    remember_bwd_args;
    (* Specialize inductive hypothesis.  Note the this specializes *both*
       hypotheses but each case only relies on one *)
    specialize_all;
    (* Equalize Pfx proof terms for rewrites to work*)
    equalize_pfx_proofs;
    (* Now the home stretch *)
    progress_aexp_consistency;
    do_prefixO_rewrites_l2r; invert_prefixO.
Qed.

Theorem aexpFwdBwd_deflating :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    deflation (pairR (prefixO aexpPO  (aexpPartialize a),
                           prefixO statePO (statePartialize st)))
                   (aexpBwd ev) (aexpFwd ev).
Proof.
  intros st a v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input nat *)
    destruct pfx as [v Hv]; destruct v;
    (* simplify calls to slicing functions to uncover the matches *)
    simpl;
    (* destruct all matches *)
    destruct_matches;
    (* The goals are actually pairs of goals: first component contains
        expressions, second contains state.  We unpack them and simplify calls
        to fst and snd. *)
    apply PairR; simpl;
    (* 144 goals at this point. auto goes down to 66 *)
    auto;
    (* We can apply PfxO to most of goals.  The exception are goals about
       stateLUB, where PfxO does not directly apply *)
    try apply PfxO;
    (* From now we follow a similar path to inflation proof.  First
       remember arguments to aexpFwd. *)
    remember_fwd_args;
    (* Specialie inudctive hypotheses (we actually need both this time) *)
    specialize_all;
    (* Invert equalities and inequalities to unify as many variables as
       possible *)
    invert_expression_inequalities;
    invert_value_inequalities;
    invert_pfx_equalities;
    (* Dismiss impossible cases *)
    dismiss_impossible_aexp_inequalities;
    (* And equalize proof terms as well so that we can rewrite with Pfx
       equalities *)
    equalize_pfx_proofs;
    (* Now rewrite specialized inudctive hypotheses with equalities in the
       context. *)
    do_pairR_rewrites;
    (* invert the resulting pairR expression *)
    invert_pairR;
    auto.
Qed.

(* Helper to avoid repetition *)
Ltac consistency_hlp H1 H2 P :=
  let H := fresh "H" in
  pose proof P as H;
  rewrite H1 in H; clear H1;
  rewrite H2 in H;
  inversion H.

(* Appeal to deflation or inflation (consistency).  Sometimes this derives
   contradiction and sometimes a useful fact about nested subexpressions *)
Ltac aexp_consistency :=
  repeat match goal with
         | [ H2 : aexpFwd ?EV _ = _
           , H1 : aexpBwd ?EV ?ARG = _ |- _ ] =>
           consistency_hlp H1 H2 (aexpFwdBwd_inflating EV ARG)
         | [ H1 : aexpFwd ?EV ?ARG = _
           , H2 : aexpBwd ?EV _ = _ |- _ ] =>
           consistency_hlp H1 H2 (aexpFwdBwd_deflating EV ARG)
         end.

(* Solve bexp inflation proof once we have premises from inductive hypothesis in
   context *)
Ltac solve_by_aexp_consistency :=
  solve [
      do_prefixO_rewrites_l2r;
      invert_prefixO;
      aexp_consistency;
      dismiss_impossible_value_inequalities
    ].

(* This helper is similar in spirit to an earlier one for solving aexp
   consistency.  The important differences are:

   * we refer to one of two *_smalle_state lemmas.  One for aexp solves cases
     where bexp have a nested aexp (BEq, BLe).  The other for bexps solves case
     for BAnd, where bexps are nested within bexps.  BNot is not covered here -
     since it has only on nested bexp there is no call to stateLUB during
     slicing.

   * the solving is a bit more involved.  We need to appeal to consistency and
     dismiss contradictions *)
Ltac progress_bexp_consistency_in_smaller_state S A1 A2 S1 S2 H :=
  let P := fresh "P" in
  assert (P : prefixO _ _ A1 A2) by auto;
  let P2 := fresh "P" in
  assert (P2 : prefixO _ _ S1 S2) by auto;
  rewrite <- S in H; (* rewrite necessary for apply to work *)
  (* Use a smaller state so that we can use the IH *)
  apply (bexpFwd_hole_in_smaller_state _ _ _ _ _ _ _ _ _ _ P P2) in H.

Ltac progress_bexp_consistency :=
  match goal with
  | [ S1 : prefix _ ?ST, S : stateLUB ?S1 ?S2 = ?R
    , H  : bexpFwd ?EV (?A, ?R) = Pfx _ _ BVHoleP _
    , H0 : bexpBwd ?EV _ = (?A2, ?S2) |- _ ] =>
    progress_bexp_consistency_in_smaller_state S A2 A S2 (stateLUB S1 S2) H
  | [ S1 : prefix _ ?ST, S : stateLUB ?S2 ?S1 = ?R
    , H  : bexpFwd ?EV (?A, ?R) = Pfx _ _ BVHoleP _
    , H0 : bexpBwd ?EV _ = (?A2, ?S2) |- _ ] =>
    progress_bexp_consistency_in_smaller_state S A2 A S2 (stateLUB S2 S1) H
  end.

Theorem bexpFwdBwd_inflating :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    inflation (prefixO boolPO (BoolP v)) (bexpBwd ev) (bexpFwd ev).
Proof.
  intros st a v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input nat *)
    destruct pfx as [v Hv]; destruct v;
    (* At the point we have 12 goals. Auto leaves us with four. *)
    try apply PfxO; auto;

    (* Simplify calls to slicing.  Uncovers matches. *)
    simpl;

    (* Destruct all matches *)
    destruct_matches;

    (* For each binary operator we arrive at four subcases.  First three
       are contradictory (BoolP <= VBHoleP as a goal), the last is true.
       We dismiss first case with discriminate and last with auto. *)
    try discriminate; auto;

    (* The middle cases require some more work.  We begin by inverting all
       premisses containing Pfx equalities *)
    invert_pfx_equalities;

    (* Remember Pfx arguments of aexpBwd.  We need to abstract them away
       for further rewrites to work *)
    remember_bwd_args;

    (* Equalize Pfx proof terms for rewrites to work *)
    equalize_pfx_proofs;

    (* Try specializing inductive hypotheses if we have them *)
    specialize_all;

    (* We are left with 7 contradicting cases.  First solve four cases for
       logical expressions nesting arithmetic expressions: BEq and BLe *)
    try solve [ try progress_aexp_consistency; solve_by_aexp_consistency ];
    (* Now the remaining three cases for BNot and BAnd *)
    progress_bexp_consistency; do_prefixO_rewrites_l2r; invert_prefixO.
Qed.

Theorem bexpFwdBwd_deflating :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    deflation (pairR (prefixO bexpPO  (bexpPartialize b),
                           prefixO statePO (statePartialize st)))
                   (bexpBwd ev) (bexpFwd ev).
Proof.
  intros st b v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input nat *)
    destruct pfx as [v Hv]; destruct v;
    (* simplify calls to slicing functions to uncover the matches *)
    simpl;
    (* destruct all matches *)
    destruct_matches;
    (* The goals are actually pairs of goals: first component contains
        expressions, second contains state.  We unpack them and simplify calls
        to fst and snd. *)
    apply PairR; simpl;
    (* 196 goals at this point. auto leaves 82 *)
    auto;
    (* We can apply PfxO to most of goals.  The exception are goals about
       stateLUB, where PfxO does not directly apply *)
    try apply PfxO;
    (* From now we follow a similar path to inflation proof.  First
       remember arguments to aexpFwd. *)
    remember_fwd_args;
    (* Specialie inudctive hypotheses (we actually need both this time) *)
    specialize_all;
    (* Invert inequalities to unify as many variables as possible *)
    invert_value_inequalities;
    invert_pfx_equalities;
    (* Dismiss impossible cases *)
    unfold_expression_partialize;
    dismiss_impossible_bexp_inequalities;
    (* Equalize Pfx proof so they don't get in the way when rewriting *)
    equalize_pfx_proofs;
    (* Rewrite paired facts *)
    do_pairR_rewrites;
    (* appeal to consistency of aexp slicing *)
    aexp_consistency;
    (* invert the resulting pairR expression *)
    invert_pairR;
    subst;
    auto.
Qed.

(* Appeal to deflation or inflation (consistency).  Sometimes this derives
   contradiction and sometimes a useful fact about nested subexpressions *)
Ltac bexp_consistency :=
  repeat match goal with
         | [ H2 : bexpFwd ?EV _ = _
           , H1 : bexpBwd ?EV ?ARG = _ |- _ ] =>
           consistency_hlp H1 H2 (bexpFwdBwd_inflating EV ARG)
         | [ H1 : bexpFwd ?EV ?ARG = _
           , H2 : bexpBwd ?EV _ = _ |- _ ] =>
           consistency_hlp H1 H2 (bexpFwdBwd_deflating EV ARG)
         end.

(* Solve bexp inflation proof once we have premises from inductive hypothesis in
   context *)
Ltac solve_by_bexp_consistency :=
  solve [
      do_prefixO_rewrites_l2r;
      invert_prefixO;
      bexp_consistency;
      dismiss_impossible_value_inequalities
    ].

Ltac progress_by_bexp_deflation :=
  match goal with
  | [ H1 : bexpFwd ?EV ?P = ?R, H2 : bexpBwd ?EV ?R = _ |- _ ] =>
    let p := fresh "P" in
    pose proof (bexpFwdBwd_deflating EV P)
  end.

Theorem comFwdBwd_inflating :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    inflation (prefixO statePO (statePartialize v))
                    (comBwd ev) (comFwd ev).
Proof.
  intros st c v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input state *)
    destruct pfx as [stC Hst]; destruct stC.
    (* At the point we have 14 goals.  *)

  - (* Skip, Nil *)
    induction st; simpl in *; auto; dismiss_impossible_state_inequalities.

  - (* Skip, Cons *)
    induction st; simpl in *; auto; dismiss_impossible_state_inequalities.

  - (* Ass, Nil *)
    simpl. apply PfxO.
    induction st; simpl in *. auto.
    destruct (beq_id x k);
      unfold_expression_partialize; dismiss_impossible_state_inequalities.

  - (* Ass, Cons *)
    simpl.
    destruct_matches; generalize_nested_pfx_proofs; invert_pfx_equalities;
    apply PfxO; reflect_id;
    (* We are now left with four goals.  The first two can be solved easily.
       The remaining two are more difficult but share a large portion of the
       proof script. *)
    [ subst; repeat rewrite updateP_head; trivial
    | rewrite updateP_shadow; rewrite updateP_tail; auto
    | idtac | idtac ];
      (* Prove the remianing two cases *)
      subst;
      equalize_pfx_proofs;
      remember_stateLUB_args;
      match goal with
      | [ S : stateLUB ?S1 ?S2 = ?R |- _ ] =>
        let H := fresh "H" in
        assert (H : prefixO _ _ S1 (stateLUB S1 S2)) by auto;
        rewrite S in H
      end;
      subst;
      invert_prefixO;
      (* We rely both on consistency and monotonicity of aexp slicing *)
      match goal with
      | [ H2 : aexpFwd ?EV ?ARG2 = _, H1 : aexpBwd ?EV ?ARG1 = ?R1 |- _ ] =>
        (* Pose consistency proof *)
        pose proof (aexpFwdBwd_inflating EV ARG1);
        (* Pose monotonicity proof.  We use refine to avoid constructing by hand
           pairR argument to monotonicity theorem - goal goes on the
           shelve and we later build the term with tactics *)
        let H := fresh "H" in
        pose proof (aexpFwd_monotone EV R1 ARG2) as H;
        let P := fresh "H" in
        refine (let P := H _ in _); clearbody P
      end;
      try rewrite updateP_head in *;
      try rewrite updateP_tail in *; auto;
      do_prefixO_rewrites_l2r;
      match goal with
      | [ H1 : prefixO natPO _ _ ?A, H2 : prefixO natPO _ ?A _ |- _ ] =>
        pose proof (transitive_prefixO _ _ transitive_natPO _ _ _ H1 H2)
      end;
      invert_prefixO;
      invert_value_inequalities;
      subst;
      (* First goal can now be solved by eauto.  Second goal requires more
         involved proof, so we use idtac to save time (eauto takes long time
         to fail). *)
      [ eauto | idtac ];
    (* Solving the remaining case is fairly involved.  One important fact that
       we need to derive is about keys K and K2 being in the domain of states we
       are operating on.  Without these some of the theorems we need would not
       hold because if key K is not in the domain of state S then
       (updateP S K V) degenerates to a no-op. *)
      match goal with
      | [ H  : lookupP _ _ = ?N0,
          H1 : (StatePCons ?K ?V _ <= statePartialize (update ?S ?K2 ?N))%s,
          H2 : (StatePCons ?K ?V (updateP ?STC ?K2 NHoleP) <= ?ST)%s
          |- (StatePCons ?K ?V ?STC <= updateP ?ST ?K2 (NatP ?N))%s ] =>
        let I := fresh "H" in
        (* Derive `In K (keysP STC)` *)
        pose proof (lookupP_NatP_In _ _ _ H) as I; (* (1) *)
        let J := fresh "H" in
        (* From (1) and H1 we derive In K2 (keysP (statePartialize S)).  from
           this we know The call to update in H1 is not a no-op *)
        apply (state_in_cons_tail _ K V) in I as J; (* 2 *)
        apply (in_keys_monotone _ _ _ H1) in J;
        rewrite <- (updateP_statePartialize_comm _ _ _ (NatP N)) in J; auto;
        rewrite <- updateP_keys_const in J;
        (* From (1), (2) and H1 we derive `lookup STC K2 <= NatP N` *)
        apply (lookupP_monotone _ _ K2) in H1;  (* 3 *)
        rewrite lookupP_tail in H1; auto;
        rewrite <- (updateP_statePartialize_comm _ _ _ (NatP N)) in H1; auto;
        rewrite lookupP_updateP in H1; auto;
        (* Derive equality between N0 and N, which allows us to conclude
           `lookup STC K2 = NatP N` *)
        rewrite H in H1; (* 4 *)
        inversion H1; subst;
        (* From monotonicity of updateP we transform H2 to:

          (updateP (StatePCons K V (updateP STC K2 NHoleP)) K2 (NatP N) <=
           updateP ST K2 (NatP N))%s*)
        apply (updateP_monotone _ _ K2 (NatP N)) in H2;
        (* Push updateP under StatePCons *)
        rewrite updateP_tail in H2; auto;
        (* Erase nested calls to updateP *)
        rewrite updateP_shadow in H2; try assumption;
        (* From (4) it follows that remianing updateP is a no-op, so we erase it
           and arrive exactly at the goal we need to prove *)
        rewrite updateP_of_existing in H2; auto
      end.

  - (* Seq, Nil *)
    erewrite comBwd_empty_state.
    erewrite comFwd_empty_state.
    eauto.

  - (* Seq, Cons *)
    (* Uncover matches in definitions of comBwd and comFwd *)
    simpl.
    (* Destruct matches and generalize enormous proof terms *)
    destruct_matches; generalize_nested_pfx_proofs.
    (* Most cases but one are impossible - dismiss *)
    all: dismiss_impossible_com_inequalities; invert_pfx_equalities;
    (* Now solve the possible case. Specialize first IH *)
    specialize_all;
    (* Create argument for second IH and specialize *)
    pose_state_and_specialize;
    (* Equalize proof terms so that we can do the rewrites *)
    equalize_pfx_proofs;
    (* Now do the rewrites in IHs *)
    do_prefixO_rewrites_l2r;
    (* Invert rewritten IHs *)
    invert_prefixO;
    (* Move calls to comFwd to hypotheses *)
    remember_comFwd;
    (* Remember arguments to those calls *)
    remember_fwd_args;
    (* Solve the goal by transitivity and monotonicity of comFwd *)
    transitivity; do_prefixO_rewrites_r2l; auto;
    comFwd_monotonicity;
    solve_consistency_by_IH.

  - (* IfTrue, Nil *)
    empty_state_reasoning.
    simpl.
    change (StatePNil) with (statePartialize StateNil).
    erewrite comFwd_empty_state; eauto.

  - (* IfTrue, Cons *)
    simpl.
    destruct_matches; generalize_nested_pfx_proofs;
    dismiss_impossible_com_inequalities; invert_pfx_equalities;
    (* Now solve two possible cases. Specialize first IH *)
    specialize_all;
    (* Create argument for second IH and specialize *)
    pose_state_and_specialize;
    (* Equalize proof terms so that we can do the rewrites *)
    equalize_pfx_proofs;
    (* Some contradicting cases solved from consistency of bwd slicing *)
    try solve [ progress_bexp_consistency; solve_by_bexp_consistency ];
    (* Rest follows from transitivity and monotonicity *)
    do_prefixO_rewrites_l2r; stateLUB_rewrite;
    progress_by_prefixO_transitivty;
    comFwd_monotonicity; auto.

  - (* IfFalse, Nil *)
    empty_state_reasoning.
    simpl.
    change (StatePNil) with (statePartialize StateNil).
    erewrite comFwd_empty_state; eauto.

  - (* IfFalse, Cons *)
    simpl.
    destruct_matches; generalize_nested_pfx_proofs;
    dismiss_impossible_com_inequalities; invert_pfx_equalities;
    (* Now solve two possible cases. Specialize first IH *)
    specialize_all;
    (* Create argument for second IH and specialize *)
    pose_state_and_specialize;
    (* Equalize proof terms so that we can do the rewrites *)
    equalize_pfx_proofs;
    (* Some contradicting cases solved from consistency of bwd slicing *)
    try solve [ progress_bexp_consistency; solve_by_bexp_consistency ];
    (* Rest follows from transitivity and monotonicity *)
    do_prefixO_rewrites_l2r; stateLUB_rewrite;
    progress_by_prefixO_transitivty;
    comFwd_monotonicity; auto.

  - (* WhileFalse, Nil *)
    empty_state_reasoning. simpl. eauto.

  - (* WhileFalse, Cons *)
    simpl.
    destruct_matches; generalize_nested_pfx_proofs;
    dismiss_impossible_com_inequalities; invert_pfx_equalities;
    (* Now solve two possible cases. Specialize first IH *)
    specialize_all;
    (* Create argument for second IH and specialize *)
    pose_state_and_specialize;
    (* Equalize proof terms so that we can do the rewrites *)
    equalize_pfx_proofs;
    (* Some contradicting cases solved from consistency of bwd slicing *)
    try solve [ progress_bexp_consistency; solve_by_bexp_consistency ];
    auto.

  - (* WhileTrue, Nil *)
    empty_state_reasoning.
    pose proof (cevalR_empty_final_state ev1 eq_refl). subst.
    simpl. generalize_nested_pfx_proofs.
    change (StatePNil) with (statePartialize StateNil).
    erewrite comFwd_empty_state; eauto.
    change (CWhileP (bexpPartialize b) (comPartialize c)) with
           (comPartialize (CWhile b c)).
    erewrite comFwd_empty_state; eauto.

  - (* WhileTrue, Cons *)
    Arguments comLUB c c1 c2 : simpl never.
    simpl.
    destruct_matches; generalize_nested_pfx_proofs;
    unfold_expression_partialize; dismiss_impossible_com_inequalities;
    invert_pfx_equalities; equalize_pfx_proofs;
    (* Some goals have comLUB fact paired with state.  We need to take it
       apart *)
    try match goal with
        | [ H : (_, _) = (_, _) |- _ ] => inversion H; clear H
        end;
    (* invert_expression_inequalities doesn't work here because H can't be
       cleared (it also appears as a proof term). *)
    try match reverse goal with
        | [ H : (CWhileP _ _ <= CWhileP _ _)%c |- _ ] => inversion H
        end;
    (* extract some facts from comLUB *)
    try match goal with
        | [ H : comLUB (Pfx _ (CWhileP ?b ?c) (CWhileP ?b1 ?c1) ?Q)
                       (Pfx _ (CWhileP ?b ?c) (CWhileP ?b2 ?c2) ?R) =
                Pfx _ (CWhileP ?b ?c) (CWhileP ?bLUB ?cLUB) ?S |- _ ] =>
          let P1 := fresh "H" in
          let P2 := fresh "H" in
          let P3 := fresh "H" in
          let P4 := fresh "H" in
          pose proof (comLUB_WhileP_leq b c b1 b2 c1 c2 bLUB cLUB Q R S H)
            as [P1 [P2 [P3 P4]]];
          try rewrite H
      end;
    (* Some goals solve by deriving incosistency of bwd slicing. For others we
       might just get some useful facts*)
    try solve [ progress_bexp_consistency; solve_by_bexp_consistency ];

    (* More goals about comLUB *)
    try match goal with
        | [ H : comLUB (Pfx _ (CWhileP ?b ?c) (CWhileP ?b1 ?c1) ?Q)
                       (Pfx _ (CWhileP ?b ?c) (CWhileP ?b2 ?c2) ?R) = _ |- _ ] =>
          let P := fresh "H" in
          pose proof (comLUB_comm_WhileP b c b1 b2 c1 c2 Q R) as P;
          let bLUB := fresh "b" in
          let cLUB := fresh "c" in
          let lubP := fresh "H" in
          let comEqn := fresh "eqnH" in
          destruct P as [bLUB [cLUB [lubP comEqn]]];
          rewrite comEqn in H
        end; invert_pfx_equalities;

      (* Now a fairly standard reasoning similar to earlier parts of the proof:
         trigger the inductive hypothesis, do the rewrites, and appeal to
         monotonicity *)
      match goal with
      | [ H  : comBwd ?EV ?ARG = ?R, IH : forall _, _ |- prefixO _ _ ?ARG _ ] =>
        specialize (IH ARG)
      end;
      specialize_all;
      do_prefixO_rewrites_l2r;
      repeat ( progress_by_prefixO_transitivty; comFwd_monotonicity; auto;
               match goal with
               | [ H : _ -> ?Q |- ?Q ] => apply H
               end; apply PairR; simpl; auto ).

    (* Solve 20 shelved goals *)
    Unshelve.
    all: try solve [ subst; try apply PairR; simpl; stateLUB_rewrite; auto ].
    (* 1 left *)
    + empty_state_reasoning;
      pose proof (cevalR_empty_final_state ev1 eq_refl); subst; auto.
Qed.

Theorem comFwdBwd_deflating :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    deflation (pairR (prefixO comPO   (comPartialize   c),
                           prefixO statePO (statePartialize st)))
                   (comBwd ev) (comFwd ev).
Proof.
  intros st c v t ev pfx.
  (* Proceed by induction on the structure of derivation *)
  induction ev;
    (* Destruct input state *)
    destruct pfx as [stC Hst]; destruct stC; simpl.
    (* At the point we have 7 goals.  *)

  - (* Skip *)
    destruct_matches; simpl; subst; auto.

  - (* Ass *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    try invert_value_inequalities; subst;
    dismiss_impossible_com_inequalities;
    apply PairR; simpl; auto;
    try apply PfxO;
    invert_pfx_equalities;
    (* 6 subgoals left *)
    try lookupP_updateP_reasoning; try discriminate; auto;
    (* 2 subgoals left *)
    invert_expression_inequalities; subst;
    invert_value_inequalities; subst;
    equalize_pfx_proofs;
    aexp_consistency;
    invert_pairR;
    auto.

  - (* Seq *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    dismiss_impossible_com_inequalities;
    apply PairR; simpl;
    (* specialize second IH using composition of comFwd calls *)
    match goal with
    | [ H  : comFwd ?EV ?ARG = ?R, IH : forall _, _ |- _ ] =>
      specialize (IH ARG)
    end;
    do_pairR_rewrites;
    invert_pairR;
    (* we use inequality derived from second IH as an argument to monotonicity
       of comBwd *)
    match goal with
    | [ H : prefixO _ _ ?P (comFwd ?EV ?ARG) |- _ ] =>
      pose proof (comBwd_monotone EV P (comFwd EV ARG) H)
    end;
    do_pairR_rewrites;
    (* specialize first IH and compose it using transitivity with inequality
       derived earlier from second IH and monotonicity.  This provides us with
       facts (often contradiciting) required to prove the goal. *)
    match goal with
    | [ H : pairR ?R ?A (comBwd _ (comFwd _ ?B)), IH : forall _, _ |- _ ] =>
      specialize (IH B);
      let P := fresh "H" in
      assert (P : pairR R A B) by (eapply transitive_pairPO; eauto)
    end;
    invert_pairR; subst;
    (* we have all required facts available - solve! *)
    dismiss_impossible_com_inequalities; auto.

  - (* IfTrue *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    dismiss_impossible_com_inequalities;
    apply PairR; simpl; auto;
    remember_fwd_args;
    specialize_all;
    try invert_value_inequalities; subst;
    equalize_pfx_proofs;
    try progress_by_bexp_deflation;
    do_pairR_rewrites;
    invert_pairR;
    dismiss_impossible_com_inequalities;
    auto.

  - (* IfFalse *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    dismiss_impossible_com_inequalities;
    apply PairR; simpl; auto;
    remember_fwd_args;
    specialize_all;
    try invert_value_inequalities; subst;
    equalize_pfx_proofs;
    try progress_by_bexp_deflation;
    do_pairR_rewrites;
    invert_pairR;
    dismiss_impossible_com_inequalities;
    auto.

  - (* WhileFalse *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    dismiss_impossible_com_inequalities;
    apply PairR; simpl; subst; auto.

  - (* WhileTrue *)
    destruct_matches;
    generalize_nested_pfx_proofs;
    unfold_expression_partialize;
    subst;
    dismiss_impossible_com_inequalities;
    (* 24 goals left *)
    apply PairR; simpl; auto;
    (* 39 goals left *)
    match goal with
    | [ H  : comFwd ?EV ?ARG = ?R, IH : forall _, _ |- _ ] =>
      specialize (IH ARG)
    end;
    do_pairR_rewrites;
    invert_pairR;
    (* we use inequality derived from second IH as an argument to monotonicity
       of comBwd *)
    match goal with
    | [ H : prefixO _ _ ?P (comFwd ?EV ?ARG) |- _ ] =>
      pose proof (comBwd_monotone EV P (comFwd EV ARG) H)
    end;
    do_pairR_rewrites;
    (* specialize first IH and compose it using transitivity with inequality
       derived earlier from second IH and monotonicity.  This provides us with
       facts (often contradiciting) required to prove the goal. *)
    match goal with
    | [ H : pairR ?R ?A (comBwd _ (comFwd _ ?B)), IH : forall _, _ |- _ ] =>
      specialize (IH B);
      let P := fresh "H" in
      assert (P : pairR R A B) by (eapply transitive_pairPO; eauto)
    end;
    invert_pairR; subst;
    (* we have more facts available - solve! *)
    dismiss_impossible_com_inequalities;
    invert_expression_inequalities;
    (* Some cases require additional facts from consistency of bexp slicing *)
    try ( invert_value_inequalities;
          subst;
          equalize_pfx_proofs;
          bexp_consistency;
          simpl in *;
          invert_prefixO
        );
    auto.
Qed.

(** ** Galois connection proofs *)

Theorem aexpFwdBwd_gc : forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
  galoisConnection (aexpBwd ev) (aexpFwd ev)
                   (order_prefixO natPO (NatP v) order_natPO)
                   (order_pair (prefixO aexpPO  (aexpPartialize a))
                               (prefixO statePO (statePartialize st))
                               (order_prefixO aexpPO (aexpPartialize a)
                                              order_aexpPO)
                               (order_prefixO statePO (statePartialize st)
                                              order_statePO)).
Proof.
  intros st a v t ev.
  apply (cons_mono__gc (aexpFwdBwd_deflating ev)
                       (aexpFwdBwd_inflating ev)
                       (aexpFwd_monotone ev)
                       (aexpBwd_monotone ev)).
Qed.

Theorem bexpFwdBwd_gc : forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
  galoisConnection (bexpBwd ev) (bexpFwd ev)
                   (order_prefixO boolPO (BoolP v) order_boolPO)
                   (order_pair (prefixO bexpPO  (bexpPartialize b))
                               (prefixO statePO (statePartialize st))
                               (order_prefixO bexpPO (bexpPartialize b)
                                              order_bexpPO)
                               (order_prefixO statePO (statePartialize st)
                                              order_statePO)).
Proof.
  intros st b v t ev.
  apply (cons_mono__gc (bexpFwdBwd_deflating ev)
                       (bexpFwdBwd_inflating ev)
                       (bexpFwd_monotone ev)
                       (bexpBwd_monotone ev)).
Qed.

Theorem comFwdBwd_gc : forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
  galoisConnection (comBwd ev) (comFwd ev)
                   (order_prefixO statePO (statePartialize v) order_statePO)
                   (order_pair (prefixO comPO   (comPartialize   c ))
                               (prefixO statePO (statePartialize st))
                               (order_prefixO comPO (comPartialize c)
                                              order_comPO)
                               (order_prefixO statePO (statePartialize st)
                                              order_statePO)).
Proof.
  intros st c v t ev.
  apply (cons_mono__gc (comFwdBwd_deflating ev)
                       (comFwdBwd_inflating ev)
                       (comFwd_monotone ev)
                       (comBwd_monotone ev)).
Qed.

Hint Resolve aexpFwdBwd_gc bexpFwdBwd_gc comFwdBwd_gc.

(** Consistency proofs *)

Theorem aexpFwdBwd_consistent :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    consistency (prefixO natPO (NatP v))
                (aexpBwd ev) (aexpFwd ev).
Proof.
  intros.
  unfold consistency.
  eapply gc_implies_consistency.
  eauto.
Qed.

Theorem bexpFwdBwd_consistent :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    consistency (prefixO boolPO (BoolP v))
                (bexpBwd ev) (bexpFwd ev).
Proof.
  intros.
  unfold consistency.
  eapply gc_implies_consistency.
  eauto.
Qed.

Theorem comFwdBwd_consistent :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    consistency (prefixO statePO (statePartialize v))
                (comBwd ev) (comFwd ev).
Proof.
  intros.
  unfold consistency.
  eapply gc_implies_consistency.
  eauto.
Qed.

(** Minimality proofs *)

(* Prove that our definitions of forward and backward slicing have the
   minimality property. *)

Theorem aexpFwdBwd_minimal :
  forall {st : state} {a : aexp} {v : nat} {t : aexpT}
         (ev : (t :: a, st \\ v)%ar),
    minimality (prefixO natPO (NatP v))
               (pairR (prefixO aexpPO  (aexpPartialize a),
                       prefixO statePO (statePartialize st)))
               (aexpBwd ev) (aexpFwd ev).
Proof.
  intros.
  unfold minimality.
  eapply gc_implies_minimality.
  eauto.
Qed.

Theorem bexpFwdBwd_minimal :
  forall {st : state} {b : bexp} {v : bool} {t : bexpT}
         (ev : (t :: b, st \\ v)%br),
    minimality (prefixO boolPO (BoolP v))
               (pairR (prefixO bexpPO  (bexpPartialize b),
                       prefixO statePO (statePartialize st)))
               (bexpBwd ev) (bexpFwd ev).
Proof.
  intros.
  unfold minimality.
  eapply gc_implies_minimality.
  eauto.
Qed.

Theorem comFwdBwd_minimal :
  forall {st : state} {c : com} {v : state} {t : comT}
         (ev : (t :: c, st \\ v)%cr),
    minimality (prefixO statePO (statePartialize v))
               (pairR (prefixO comPO  (comPartialize c),
                       prefixO statePO (statePartialize st)))
               (comBwd ev) (comFwd ev).
Proof.
  intros.
  unfold minimality.
  eapply gc_implies_minimality.
  eauto.
Qed.
