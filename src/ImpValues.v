Require Import Lattice.
Require Import PrefixSets.
Require Import LSTactics.

(** ** Partial values *)

(* Partial nats *)
Inductive natP : Type :=
| NHoleP :          natP
| NatP  : nat -> natP.

(* Partial booleans *)
Inductive boolP : Type :=
| BVHoleP :         boolP
| BoolP   : bool -> boolP.

(** ** Partial value ordering *)

(* Ordering on partial nats *)
Inductive natPO : relation natP :=
| NHolePO : forall v,
    NHoleP <= v
| NatPO : forall n1 n2,
    n1 = n2 ->
    NatP n1 <= NatP n2
  where "v1 '<=' v2" := (natPO v1 v2) : natPO_scope.

Delimit Scope natPO_scope with v.

Hint Constructors natPO.

(* Ordering on partial bools *)
Inductive boolPO : relation boolP :=
| BVHolePO : forall v,
    BVHoleP <= v
| BoolPO : forall b1 b2,
    b1 = b2 ->
    BoolP b1 <= BoolP b2
  where "v1 '<=' v2" := (boolPO v1 v2) : boolPO_scope.

Delimit Scope boolPO_scope with vb.

Hint Constructors boolPO.

(* Dismiss cases containing impossible premises about inequalities *)
Ltac dismiss_impossible_value_inequalities :=
  repeat match reverse goal with
         | [ H : (_ <= NHoleP)%v   |- _ ] => inversion H; clear H
         | [ H : (_ <= BVHoleP)%vb |- _ ] => inversion H; clear H
         end.

(* Invert a prefixO expression.  Calling dismiss_impossible_value_inequalities
   solves impossible cases early (in fact this call is mandatory for the tactic
   to work as intended in later proofs). *)
Ltac invert_prefixO :=
  repeat match goal with
         | [ IH : prefixO _ _ _ _ |- _ ] =>
           subst; inv IH; dismiss_impossible_value_inequalities
         end.

(** ** Partialize functions *)

Definition natPartialize (v : nat) : natP := NatP v.

Definition boolPartialize (v : bool) : boolP := BoolP v.

Ltac unfold_value_partialize :=
  (* Partialization of nats and bools is trivial, we can always unfold *)
  unfold boolPartialize in *;
  unfold natPartialize  in *.

(* Inverts value in the context. *)
Ltac invert_value_inequalities :=
  unfold_value_partialize;
  (* Can't use `repeat match `because in many cases inequality appears as a
     proof term and it is not possible to clear it - withoit clearing we would
     get into infinite loop.  Thus we specialize for up to six inequalities *)
  match goal with
  | [ H1 : (NatP _ <= _)%v, H2 : (NatP _ <= _)%v
    , H3 : (NatP _ <= _)%v, H4 : (NatP _ <= _)%v
    , H5 : (NatP _ <= _)%v, H6 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4;
    inversion H5; try clear H5;
    inversion H6; try clear H6
  | [ H1 : (NatP _ <= _)%v, H2 : (NatP _ <= _)%v
    , H3 : (NatP _ <= _)%v, H4 : (NatP _ <= _)%v
    , H5 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4;
    inversion H5; try clear H5
  | [ H1 : (NatP _ <= _)%v, H2 : (NatP _ <= _)%v
    , H3 : (NatP _ <= _)%v, H4 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4
  | [ H1 : (NatP _ <= _)%v, H2 : (NatP _ <= _)%v
    , H3 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3
  | [ H1 : (NatP _ <= _)%v, H2 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2
  | [ H1 : (NatP _ <= _)%v |- _ ] =>
    inversion H1; try clear H1

  | [ H1 : (BoolP _ <= _)%vb, H2 : (BoolP _ <= _)%vb
    , H3 : (BoolP _ <= _)%vb, H4 : (BoolP _ <= _)%vb
    , H5 : (BoolP _ <= _)%vb, H6 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4;
    inversion H5; try clear H5;
    inversion H6; try clear H6
  | [ H1 : (BoolP _ <= _)%vb, H2 : (BoolP _ <= _)%vb
    , H3 : (BoolP _ <= _)%vb, H4 : (BoolP _ <= _)%vb
    , H5 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4;
    inversion H5; try clear H5
  | [ H1 : (BoolP _ <= _)%vb, H2 : (BoolP _ <= _)%vb
    , H3 : (BoolP _ <= _)%vb, H4 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3;
    inversion H4; try clear H4
  | [ H1 : (BoolP _ <= _)%vb, H2 : (BoolP _ <= _)%vb
    , H3 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2;
    inversion H3; try clear H3
  | [ H1 : (BoolP _ <= _)%vb, H2 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1;
    inversion H2; try clear H2
  | [ H1 : (BoolP _ <= _)%vb |- _ ] =>
    inversion H1; try clear H1
  end.

(** ** LUB functions *)

(* This function is somewhat underspecified.  It does not do anything useful
   with evidences stored in prefix expressions.  For example, n1 and n2 are
   equal but this fact is not observed in any way. *)
Definition natLUB {v : natP} (v1 v2 : prefix natPO v) :
    prefix natPO v :=
  match v1, v2 with
  | Pfx _ _ v1' R1, Pfx _ _ v2' R2 =>
    match v1', v2' with
    | NHoleP, _ => Pfx _ _ v2' R2
    | _, NHoleP => Pfx _ _ v1' R1
    | NatP n1, NatP n2 => Pfx _ _ v1' R1
    end
  end.

(** ** Proofs *)

Theorem reflexive_natPO : reflexive natPO.
Proof. solve_reflexive. Qed.

Theorem transitive_natPO : transitive natPO.
Proof. solve_transitive. Qed.

Theorem antisymmetric_natPO : antisymmetric natPO.
Proof. solve_antisymmetric. Qed.

Hint Resolve reflexive_natPO transitive_natPO antisymmetric_natPO.

Theorem reflexive_boolPO : reflexive boolPO.
Proof. solve_reflexive. Qed.

Theorem transitive_boolPO : transitive boolPO.
Proof. solve_transitive. Qed.

Theorem antisymmetric_boolPO : antisymmetric boolPO.
Proof. solve_antisymmetric. Qed.

Hint Resolve reflexive_boolPO transitive_boolPO antisymmetric_boolPO.

Theorem order_natPO : order natPO.
Proof. eauto. Qed.

Theorem order_boolPO : order boolPO.
Proof. eauto. Qed.

Hint Resolve order_natPO order_boolPO.
