Verified Self-Explaining Computation
====================================

This repository contains Coq development for paper "Verified Self-Explaining
Computation" by Jan Stolarek and James Cheney.

Building
--------

To build the project execute the following commands in project's root directory:

```bash
coq_makefile -f _CoqProject -o Makefile
make
```

This code was tested with Coq 8.9.0/Ocaml 4.07.1.  It might not work with other
Coq versions.
